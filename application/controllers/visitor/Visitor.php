<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitor extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('plan_m');
        $this->load->model('visitor_m');
        $this->load->helper('email_helper');
    } 

    public function check_status()
    {
        $url = $this->input->post('url');
        $unique_code = strrev($this->input->post('unique_code'));
        $relation = array(               
            "fields" => "*",                          
            'conditions' => "unique_code ='" .$unique_code . "'"                      
        );    
        $result = $this->user_m->get_relation("",$relation); 
        
        if (count($result) > 0)
        {
            if ($result[0]['domain'] != '')
            {
                if ($result[0]['domain'] == $url)
                {
                    return $this->output->set_content_type("application/json")->
                    set_output(json_encode(array("user_id" => $result[0]['id'], "status" => "yes")));
                }
                else{
                    return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
                }
            }
            else
            {
                $array['domain'] = $url;
                $this->user_m->save($array, $result[0]['id']);
                return $this->output->set_content_type("application/json")->
                    set_output(json_encode(array("user_id" => $result[0]['id'], "status" => "yes")));
            }
        }
        else
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
    }

    public function add()
    {
        $array['cookie'] = $this->input->post('cookie');
        $array['email'] = $this->input->post('email');
        $relation = array(
            "fields" => "*",
            'conditions' => "email_id ='" .$array['email'] . "'"
        );
        $count_users = $this->user_m->get_relation("",$relation, true);
        if ($count_users > 0)
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no","message"=>"Same Email-Id is used as Client. Would you please use another Email ?")));
            exit;
        }
        $relation = array(
            "fields" => "*",
            'conditions' => "email ='" .$array['email'] . "'"
        );
        $visitor_info = $this->visitor_m->get_relation("",$relation);
        $url = $this->input->post('url');
        $relation = array(
            "fields" => "*",
            'conditions' => "domain ='" .$url . "'"
        );
        $result = $this->user_m->get_relation("",$relation);
        if (count($visitor_info) > 0)
        {
            $data = array('status'=>1,'cookie'=>$array['cookie']);
            $this->db->where('unique_id', $visitor_info[0]['unique_id']);
            $this->db->update('webchat_visitors', $data);
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"repeat","visitor_info"=>$visitor_info, "email"=>$result[0]['email_id'])));
            exit;
        }
        $array['full_name'] = $this->input->post('full_name');
        $array['unique_id'] = strtotime("now");
        $array['client_id'] = isset($result) ? $result[0]['id'] : '';
        $id = $this->visitor_m->save($array);
        if ( $id )
        {
            $data = array('status'=>1);
            $this->db->where('email', $array['email']);
            $this->db->update('webchat_visitors', $data);
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes","visitor_id"=> $array['unique_id'],"client_id"=>$array['client_id'],"email"=>$result[0]['email_id'])));
        }
        else
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no", "message"=>"Not able to perform the action")));
        }
    }

    public function fetch_record()
    {
        if ($this->input->post('type') == "new")
        {
            $condition =  "chat_id != '' AND created > DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND client_id =" .$this->session->userdata('user_id');
        }
        else if ($this->input->post('type') == "online")
        {
            $condition =  "chat_id != '' AND status = 1 AND client_id =" .$this->session->userdata('user_id');
        }
        else if ($this->input->post('type') == "all")
        {
            $page_no = $this->input->post("page_no");
            $per_page = 10;
            $start = ($page_no - 1) * $per_page;
            $end = $per_page;
            $condition =  "chat_id != ''  AND client_id =" .$this->session->userdata('user_id');
        }
        $relation = array(
            "fields" => "*",
            'conditions' => $condition
        );
        if ($this->input->post('type') == "all") {
            $relation['LIMIT']['start'] =  $end;
            $relation['LIMIT']['end'] = $start;
        }
        $relation['ORDER_BY']['field']='notifications_count';
        $relation['ORDER_BY']['order']='DESC';
        $array = $this->visitor_m->get_relation("",$relation);
        if (!empty($array))
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes","data"=>$array)));
        }
        else{
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no", "message"=>"Not able to perform the action")));
        }   
    }

    public function update_last_seen()
    {
        $data = array('chat_id'=> $this->input->post('chat_id'), 'status'=>1);
        $this->db->where('unique_id', $this->input->post('visitor_id'));
        $result  = $this->db->update('webchat_visitors', $data);
        if ($result == true)
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
        }
        else{
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
    }

    public function disconnect()
    {
        $data = array('status'=>0);
        $this->db->where('unique_id', $this->input->post('email'));
        $result = $this->db->update('webchat_visitors', $data);
        if ($result == true)
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
        }
        else{
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
    }

    public function check_cookie()
    {
        $relation = array(
            "fields" => "*",
            'conditions' => "cookie = '".$this->input->post('cookie')."'"
        );
        $visitor_info = $this->visitor_m->get_relation('', $relation);
        if (!empty($visitor_info))
        {
            $relation = array(
                "fields" => "email_id",
                'conditions' => "id ='" .$visitor_info[0]['client_id'] . "'"
            );
            $result = $this->user_m->get_relation("",$relation);
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes", "unique_id"=>$visitor_info[0]['unique_id'],"client_email"=>$result[0]['email_id'], "client_id"=>$visitor_info[0]['client_id'])));
        }
        else
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
    }

    public function check_client_status()
    {
        $url = $this->input->post('url');
        $relation = array(
            "fields" => "*",
            'conditions' => "domain ='" .$url. "'"
        );
        $result = $this->user_m->get_relation("",$relation, true);
        if ($result > 0)
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
        }
        else{
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
    }

    public function get_questions()
    {
        $relation = array(
            "fields" => "primary.plan_id, secondaryy.widget_color, secondaryy.widget_image ,secondaryy.name_ques, secondaryy.email_ques, secondaryy.number_ques, secondaryy.client_id, secondaryy.contact_option ",
            'JOIN' => array(
                array(
                    'table' => 'webchat_users_secondary as secondaryy',
                    'condition' => 'secondaryy.client_id = webchat_users.id',
                    'type' => 'LEFT'
                ),
                array(
                    'table' => 'webchat_users_primary as primary',
                    'condition' => 'primary.client_id = webchat_users.id',
                    'type' => 'LEFT'
                ),
            ),
            'conditions' => "webchat_users.domain = '" .$this->input->post('url')."'"
        );
        $result = $this->user_m->get_relation("",$relation);
        $relation = array(
            "fields" => "*",
            'conditions' => "unique_id ='" .base64_decode($this->input->post('visitor_id')). "'"
        );
        $visitor_information = $this->visitor_m->get_relation("",$relation);
        if (!empty($result))
        {
            $question_html = "";
            if (($result[0]['contact_option'] == 0 || $result[0]['contact_option'] == 1) && empty($visitor_information[0]['email']))
            {
                $email_ques = $result[0]['email_ques'] != '' ?  $result[0]['email_ques'] : 'هل تسمح لنا بالحصول على بريدك الالكتروني في حالة احتجا التواصل' ;
                // $question_html .= '<div class="receiver">                    
                //     <div class="msg-bubble user-basic-info">                       
                //         <div class="form-group">
                //             <label for="email">'.$email_ques .'</label>
                //             <input type="email" class="form-control" id="email" name="email">
                //             <span style="display:none" id="email_error">Please enter valid email address</span>
                //         </div>
                //     </div>                         
                //     <div class="user-avtar">                        
                //         <img src="http://clientapp.narola.online/SD/web_live_chat/assets/img/chat-user-avtar.png" class="img-responsive" alt="img">
                //     </div>                
                // </div>';
                     $question_html .= '<div class="sender">                    
                     <div class="user-avtar">                        
                         <img src="http://clientapp.narola.online/hde/276431/Web_live/assets/img/chat-user-avtar.png" class="img-responsive" alt="img">
                     </div>                
                    <div class="msg-bubble user-basic-info">                       
                        <div class="form-group">
                            <label for="email">'.$email_ques .'</label>
                            <input type="email" class="form-control" id="email" name="email">
                            <span style="display:none" class="chatbot-error" id="email_error">Please enter valid email address</span>
                        </div>
                    </div>                         
                </div>';
            }
            if (($result[0]['contact_option'] == 2 || $result[0]['contact_option'] == 1) && empty($visitor_information[0]['full_name']))
            {
                $name_ques = $result[0]['name_ques'] != '' ?  $result[0]['name_ques'] : 'الاسم الكريم';
                $question_html .= ' <div class="sender">                    
                    <div class="user-avtar">                        
                        <img src="http://clientapp.narola.online/hde/276431/Web_live/assets/img/chat-user-avtar.png" class="img-responsive" alt="img">
                    </div>                
                    <div class="msg-bubble user-basic-info">                       
                        <div class="form-group">
                            <label for="name">'.$name_ques.'</label>
                            <input type="name" class="form-control" id="name" name="name">
                            <span style="display:none" class="chatbot-error" id="name_error">Please enter valid name</span>
                        </div>
                    </div>                         
                </div>';
            }
            if ($result[0]['contact_option'] == 2 && empty($visitor_information[0]['phone_number']))
            {
                $number_ques = $result[0]['number_ques'] != '' ?  $result[0]['number_ques'] : 'هل تسمح لنا بالحصول على رقم هاتفك ان احتجنا نتواصل معك';
                $question_html .= '<div class="sender">                    
                    <div class="user-avtar">                        
                        <img src="http://clientapp.narola.online/hde/276431/Web_live/assets/img/chat-user-avtar.png" class="img-responsive" alt="img">
                    </div>                
                    <div class="msg-bubble user-basic-info">                       
                        <div class="form-group">
                            <label for="phone">'.$number_ques.'</label>
                            <input type="number" class="form-control" id="phone_number" name="phone_number">
                            <span style="display:none" class="chatbot-error" id="phone_number_error">Please enter valid phone number</span>
                        </div>
                    </div>                         
                </div>';
            }
            if (!empty($result))
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes", "question_html"=> $question_html,"widget_color" => $result[0]['widget_color'], "widget_image" => WIDGET_LOGO.$result[0]['widget_image'],"plan_id"=> $result[0]['plan_id'])));
            else
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
    }

    public function add_visitor()
    {
        $relation = array(
            "fields" => "*",
            'conditions' => "domain ='" .$this->input->post('url'). "'"
        );
        $result = $this->user_m->get_relation("",$relation);
        $array['cookie'] = $this->input->post('cookie');
        $array['client_id'] = $result[0]['id'];
        $array['unique_id'] = strtotime("now");
        $array['ip'] = $this->input->post('ip');
        $resultt = $this->visitor_m->save($array);
        if (!empty($resultt))
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes", "unique_id" => $array['unique_id'], "client_id" => $result[0]['id'], "client_email" => $result[0]['email_id'])));
        }else{
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
    }

    public function set_visitor_info()
    {
        $relation = array(
            "fields" => "*",
            'conditions' => "email ='" .$this->input->post('f_value'). "'"
        );
        $count = $this->visitor_m->get_relation("",$relation, true);
        if ($count > 0)
        {
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
        else
        {
            $this->db->where("unique_id", $this->input->post('unique_id'));
            $result = $this->db->update("webchat_visitors", array(
                $this->input->post("f_name") => $this->input->post("f_value")
            ));
            if ($result)
            {
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
            }else{
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
            }
        }

    }

}