<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('plan_m');
        $this->load->model('visitor_m');
        $this->load->model('teammate_m');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }

    public function index()
    {
        $this->data['title'] = 'Web Live chat - Conversations';
        $this->data['subview'] = 'teammate/profile/index';
        $this->data['script'] = 'teammate/profile/script';
        $relation = array(
            "fields" => "*",
            'conditions' => "teammate_id =" .$this->session->userdata('user_id')
        );
        $this->data['teammate_information'] = $this->teammate_m->get_relation('', $relation);
        $this->load->view("teammate_layout_main", $this->data);
    }

    public function set_teammate_info()
    {
        $relation = array(
            "fields" => "*",
            'conditions' => "teammate_id =".$this->session->userdata('user_id')
        );
        $count = $this->teammate_m->get_relation("",$relation, true);
        if ($count > 0)     
        {
            $this->db->where("teammate_id", $this->session->userdata('user_id'));
            $result = $this->db->update('webchat_teammates', array("chatbot_email" => $this->input->post('chatbot_email'),"name" => $this->input->post('name') ));
            $this->session->set_flashdata("success","Profile updated successfully");
        }
        else{
            $result = $this->db->insert('webchat_teammates', array("chatbot_email" => $this->input->post('chatbot_email'), "teammate_id"=>$this->session->userdata('user_id'),"name" => $this->input->post('name') ));
            $this->session->set_flashdata("success","Profile updated successfully");
        }
        redirect("t_profile");
    }

    public function set_chatbot_image()
    {
        if (!empty($_FILES['chatbot_profile']['name']))
        {
           
            $config['upload_path']          = './uploads/profile';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000000;
            $config['file_name']           =  time();
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('chatbot_profile'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata("error", "Image is not uplodaded. Please try again");
                redirect('profile');
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                $array['chatbot_profile'] = $data['upload_data']['file_name'] ;
                $array['teammate_id'] = $this->session->userdata("user_id");
                $relation = array(
                    "fields" => "*",
                    'conditions' => "teammate_id =" .$this->session->userdata('user_id')
                );
                $setting_info = $this->teammate_m->get_relation('',$relation);  
                if (count($setting_info) > 0)
                {
                    $id = $this->teammate_m->save($array,$setting_info[0]['id']);
                }
                else{
                    $id = $this->teammate_m->save($array);
                }
                if ($id)
                {
                    $this->session->set_flashdata("success", "Profile picture updated successfully");
                }
                else{
                    $this->session->set_flashdata("error", "Something happens wrong");
                }
                redirect('t_profile');
            }
        }
    }

}