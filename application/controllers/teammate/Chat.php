<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('plan_m');
        $this->load->model('visitor_m');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }

    public function index()
    {
        $this->data['title'] = 'Web Live chat - Conversations';
        $this->data['subview'] = 'teammate/chat/index';
        $this->data['script'] = 'teammate/chat/script';
        $this->load->view("teammate_layout_main", $this->data);
    }

}