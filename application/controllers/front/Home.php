<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title'] = 'Index';
        // $this->data['script'] = 'front/script';
        $this->load->view('front/index', $this->data);
    }

    public function terms()
    {
        $this->data['title'] = 'Terms & Services';
        $this->load->view('front/terms', $this->data);
    }

    public function privacy()
    {
        $this->data['title'] = 'Privacy Policy';
        $this->load->view('front/privacy', $this->data);
    }

    public function pricing()
    {
        $this->data['title'] = 'Our Prices';
        $this->load->view('front/pricing', $this->data);
    }

    public function about()
    {
        $this->data['title'] = 'About Sideburns';
        $this->load->view('front/about', $this->data);
    }

    public function chatbot()
    {
        $this->data['title'] = 'About chatbot';
        $this->load->view('front/chatbot', $this->data);
    }
}