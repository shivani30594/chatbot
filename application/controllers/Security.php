<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Security extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('plan_m');
        $this->load->model('visitor_m');
        require_once APPPATH.'third_party/src/Google_Client.php';
        require_once APPPATH.'third_party/src/contrib/Google_Oauth2Service.php';
        $this->load->helper('email_helper');
    }

    public function index()
    {
        $this->data['title'] = 'Index';
        $this->load->view('front/index', $this->data);
    }

    public function login()
    {
        if ($this->session->userdata('u_loggedin') == TRUE) {
            redirect('chats');
            exit;
        }
        $this->data['title'] = 'Login';
        $this->data['script'] = "front/script";
        $this->load->view('front/login', $this->data);
    }

    public function check_login()
    {
        $email_id = $this->input->post('email_id');
        $password = md5($this->input->post('password'));
        if ($this->user_m->login($email_id, $password) == FALSE)
        {
            $this->session->set_flashdata('error',"Username and password combination doesn't exists");
            redirect('login');
            exit;
        }
        else
        {
            if ($this->session->userdata('role') == strtolower('teammate'))
            {
                redirect('t_profile');
            }
            else 
            {
                $relation = array(
                    "fields" => "*",
                    'conditions' => "client_id =".$this->session->userdata('user_id')
                );
                $count = $this->visitor_m->get_relation('', $relation, true);
                if ($count > 0)
                {
                    redirect('chats');
                }
                else
                {
                    redirect('install_script');
                }
            }
            exit;
        }
    }

    public function google_login()
    {
                                                                                                                                                                 
        $clientId = GOOGLE_CLIENT_ID; //Google client ID
        $clientSecret = GOOGLE_CLIENT_SECRET ; //Google client secret
        $redirectURL = GOOGLE_REDIRECT_URL;

        //Call Google                                                                                 
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectURL);
        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if(isset($_GET['code']))
        {
            $gClient->authenticate($_GET['code']);
            $this->session->set_userdata('token' , $gClient->getAccessToken());
            header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
        }

        if (isset($_SESSION['token'])) 
        {
            $gClient->setAccessToken($this->session->userdata('token'));
        }

        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();
            $relation = array(
                "fields" => "*",
                'conditions' => "token ='" .$userProfile['id'] . "'"
            );
            $data  = array();
			$userInfo = $this->user_m->get_relation('', $relation, false);
            if (count($userInfo) == 0)
            {
                $relation = array(
                    "fields" => "email_id",
                    'conditions' => "email_id ='" .$userProfile['email'] . "'"
                );
                $count = $this->user_m->get_relation('',$relation,true);   
                if ($count > 0)
                {
                    $this->session->set_flashdata("error","Email-id already exists. Try with another Email-id!");
                    redirect("login");
                    exit;
                } 
                $userArray['token'] = $userProfile['id'];
                $userArray['first_name'] = $userProfile['given_name'];
                $userArray['last_name'] = $userProfile['family_name'];
                $userArray['email_id'] = $userProfile['email'];
                $userArray['profile_image'] = $userProfile['picture'];
                $userArray['is_active'] = 'Yes';
                $result = $this->user_m->save($userArray);
            }
            $data = array(
                'user_id' => count($userInfo) == 0 ? $this->db->insert_id() : $userInfo[0]['id'],
                'u_loggedin' => TRUE,
            );
            $this->session->set_userdata($data);
            redirect('u_dashboard');
        } 
        else 
        {
            $url = $gClient->createAuthUrl();
            header("Location: $url");
            exit;
		}
    }


    public function check_email()
    {
        $email_id = $this->input->post('email_id');
        $relation = array(
            "fields" => "email_id",
            'conditions' => "email_id ='" .$email_id . "'"
        );
        $count = $this->user_m->get_relation('',$relation,true);
        if ($count > 0)
        {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => 'found')));
        }
        else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => 'not_found')));
        }
    }

    public function forgot()
    {
        $this->data['title'] = 'Web live chat - Forgot password';
        $this->load->view('front/forgot_password', $this->data);
    }

    public function forgot_password()
    {
        $this->load->helper('email_helper');  
        if ($this->input->post())
        {
            $email = $this->input->post('email');
            $user = $this->user_m->get_by("email = '$email'")[0];  
            if (!empty($user))
            {
                $token = md5($user->email . rand());
                $data = array(
                    'verification_token' => $token,
                    'updated' => date('Y-m-d H:i:s')
                );
                $this->user_m->save($data, $user->id);
                $to = $email;
                $subject = "Web live chat Password Reset Request";
                $body = "<p><strong>Hello</strong><strong>" . $user->first_name . "</strong>,</p>Looks like you'd like to change your                 password. Please click the following link to do so:<br>
                        <a href=" . base_url('security/changePassword') . "?token=" . $token . ">Click Here</a>
                        <P>Please ignore this e-mail if you did not request a password rest.</p>
                        <p><strong>Cheers,</strong></p>
                        <p><b>Web live chat<b></p>
                        <img src='". IMG. "global/img/login-logo.png' height='100px' width='100px'>";
                $temp = send_mail($to, $subject, $body);
                if ($temp)
                {
                    $this->session->set_flashdata("success", "Reset password mail is successfully sent to your emailId " . $email);
                    redirect('login');
                }
                else
                {
                    $this->session->set_flashdata("error", "Opps Something went wrong!!");
                    redirect('login');
                }
            }
            else
            {
                $this->session->set_flashdata("error", "Email you have entered is not exists!!");
                redirect('login');
            }
        }
        else
        {

            $this->session->set_flashdata("error", "Sorry Something went wrong!!!!!");
            redirect('login');
        }
    }

    public function change_password()
    {
        if (!isset($_GET['token']))
        {
            show_error('Invalid Url');
        }
        $token = $_GET['token'];

        $user = $this->user_m->get_by("verification_token = '$token'")[0];
        if ($this->input->post())
        {
            $data = array(
                'password' => md5($this->input->post('password')),
                'verification_code' => NULL,
            );
            $this->user_m->save($data, $user->user_id);
            $this->session->set_flashdata("success", "Your password is reset successfully!!");
            redirect('login');
        }

        if (!empty($user))
        {
            if ($user->verification_code == NULL)
            {
                show_error("Your reset password link is expired");
            }
        }
        else
        {
            show_error("Your reset password link is expired");
        }
        $this->data['title'] = 'Register';
        $this->load->view('front/change_password');
    }

    public function set_email()
    {
        $this->session->set_userdata("set_email", $this->input->post('email')) ;
        redirect('register');
    }

    public function check_set_email()
    {
        $email_id = $this->input->post('email_id');
        $relation = array(
            "fields" => "email_id",
            'conditions' => "email_id ='" .$email_id . "'"
        );
        $count = $this->user_m->get_relation('',$relation,true);
        if ($count > 0)
        {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => 'found')));
        }
        else{
            $this->session->set_userdata("set_email", $this->input->post('email_id'));
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => 'not_found')));
        }
    }
}