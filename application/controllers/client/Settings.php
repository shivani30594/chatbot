<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('plan_m');
        $this->load->model('secondary_m');
        $this->load->model('primary_m');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }

    /*
    * initial page
    */
    public function index()
    {
        $relation = array(
            "fields" => "*",
            'conditions' => "client_id ='" .$this->session->userdata('user_id') . "'"
        );
        $this->data['style_data'] = $this->secondary_m->get_relation('',$relation);  
        $relation = array(
            "fields" => "plan_id",
            'conditions' => "client_id ='" .$this->session->userdata('user_id') . "'"
        );
        $this->data['plan_id'] = $this->primary_m->get_relation('',$relation)[0]['plan_id'];  
        $this->data['title'] = 'Web Live chat - settings';
        $this->data['subview'] = 'client/settings/index';
        $this->data['script'] = 'client/settings/script';
        $this->load->view("client_layout_main", $this->data);
    }

    /*
    * logout from the website
    */
    public function logout()
    {
        $this->session->sess_destroy();
        $this->user_m->logout();
        redirect("/");
    }

    /*
    * set the color for the chatbot
    */
    public function set_color()
    {
        if (!empty($this->input->post('widget_color')))
        {
            $this->db->where('client_id',$this->session->userdata('user_id'));
            $result = $this->db->update('webchat_users_secondary', array("widget_color"=> base64_decode($this->input->post("widget_color"))));
            if ($result)
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
            else
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
        }
    }

    /*
    * To set the chatbot-image
    */
    public function set_image()
    {
        if (!empty($_FILES['file']['name']))
        {
            $config['upload_path']          = './uploads/logo';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2048;
            $config['file_name']           =  time();
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('file'))
            {
                $error = array('error' => $this->upload->display_errors());
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
            }
            else
            {
                if (!empty($this->input->post('widget_logo')))
                {
                    if (file_exists("./uploads/logo/".base64_decode($this->input->post('widget_logo'))))
                    {
                        unlink('./uploads/logo/'.base64_decode($this->input->post('widget_logo')));
                    }
                }
                $data = array('upload_data' => $this->upload->data());
                $this->db->where('client_id',$this->session->userdata('user_id'));
                $result = $this->db->update('webchat_users_secondary', array("widget_image" => $data['upload_data']['file_name']));
                if ($result)
                    return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
                else
                    return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
            }
        }
    }

    /*
    * To set the status "online" and "offline" we used below function
    */
    public function change_client_status()
    {
        $result = $this->user_m->save(array("login_status"=> base64_decode($this->input->post("status"))), $this->session->userdata('user_id'));
        if ($result)
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
        else
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
    }

    /*
    * To set the status "online" and "offline" we used below function
    */
    public function change_contact_option()
    {
        $this->db->where('client_id',$this->session->userdata('user_id'));
        if (!empty($this->input->post()))
        {
            $result = $this->db->update('webchat_users_secondary', array("contact_option"=> base64_decode($this->input->post("contact_option"))));
        }
        if ($result)
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
        else
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
    }

    /*
    * To set the question text
    */
    public function set_question_text()
    {
        $this->db->where('client_id',$this->session->userdata('user_id'));
        if (!empty($this->input->post()))
        {
            $result = $this->db->update('webchat_users_secondary', array($this->input->post("q_name") => $this->input->post("q_value")));
        }
        if ($result)
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
        else
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
    }
}
