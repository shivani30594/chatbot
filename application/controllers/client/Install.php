<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('plan_m');
        $this->load->model('user_m');
        $this->load->model('visitor_m');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }
    
    public function index() 
    {
        $this->data['title'] = 'Web Live chat - Install';
        // fetch the unique code
        $user_info = $this->user_m->get($this->session->userdata('user_id'));  
        if (isset($user_info))
        {
            $unique_code = $user_info->unique_code;
            $unique_code = strrev($unique_code);
        } 
        else{
            $unique_code = '';
        }
        // genretae the uniqeu code
        // $code_snippet = "
        //                 <script type='text/javascript'>		
        //                         $(document).ready(function()
        //                         {
        //                                 var z = window, A = document;
        //                                 s = A.createElement('span');
        //                                 s.setAttribute('id','token');
        //                                 $('body').append(s);
        //                                 $('#token').text('".$unique_code."');
        //                                 $('#token').css('display','none');
        //                                 $.ajax({
        //                                         url: '".BASE_URL."check_status',   
        //                                         type: 'POST', 
        //                                         data: { url:window.location.hostname, unique_code:$('#token').text() },
        //                                         success: function(response)
        //                                         {
        //                                                 if(response.status === 'yes')
        //                                                 {
        //                                                         var e = A.createElement('div');
        //                                                         e.setAttribute('id','chat-widget-container');
        //                                                         var d = A.body && A.body.lastChild || A.body || A.head;
        //                                                         $('body').append(e);
        //                                                         $('#chat-widget-container').css({
        //                                                             'position': 'fixed',
        //                                                             'bottom': '15px',
        //                                                             'width': '360px',
        //                                                             'height': '560px',
        //                                                             'max-width': '100%',
        //                                                             'max-height': 'calc(100% - 0px)',
        //                                                             'overflow': 'hidden',
        //                                                             'right': '15px',
        //                                                             'transition': 'none 0s ease 0s !important'
        //                                                         });
        //                                                         $('#token').remove('');
        //                                                         var c = A.createElement('iframe');
        //                                                         c.setAttribute('src','".NODE_URL."');
        //                                                         c.setAttribute('id','chat-widget');
        //                                                         c.setAttribute('allowtransparency',true);
        //                                                         $('#chat-widget-container').append(c);
        //                                                         $('#chat-widget').css({
        //                                                             'width': '100%',
        //                                                             'height': '100%',
        //                                                             'min-height': '0px',
        //                                                             'min-width': '0px',
        //                                                             'margin': '0px',
        //                                                             'padding': '0px',
        //                                                             'background-image': 'none',
        //                                                             'background-position': '0% 0%',
        //                                                             'background-size': 'initial',
        //                                                             'background-attachment': 'scroll',
        //                                                             'background-origin': 'initial',
        //                                                             'background-clip': 'initial',
        //                                                             'background-color': 'rgba(0, 0, 0, 0)',
        //                                                             'border-width': '0px','float': 'none',
        //                                                             'transition': 'none 0s ease 0s !important',
        //                                                             'visibility': 'visible'
        //                                                         });
        //                                                 }
        //                                                 else
        //                                                 {
        //                                                         alert('Your website is not enrolled into our system. Would you please check it ?')
        //                                                 }
        //                                         }    
        //                                 });
        //                         });
        //                 </script>";

        $code_snippet = "
                        <script type='text/javascript'>		
                                $(document).ready(function() {
                                    $('body').append( '<span id=\"token\" style=\"display:none\">".$unique_code."</span>');
                                });
                        </script>
                        <script src='".SCRIPTS."install_snippet.js'></script>";
        $this->data['code_snippet'] = $code_snippet;
        $this->data['subview'] = 'client/install/index';
        $this->data['script'] = 'client/install/script';
        $this->load->view("client_layout_main", $this->data);
    }

}