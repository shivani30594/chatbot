<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class General_setting extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('plan_m');
        $this->load->model('secondary_m');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }

    public function index()
    {
        $this->data['title'] = 'Web Live chat - General Setting';
        $this->data['subview'] = 'client/general_setting/index';
        $this->data['script'] = 'client/general_setting/script';
        $this->load->view("client_layout_main", $this->data);
    }

}