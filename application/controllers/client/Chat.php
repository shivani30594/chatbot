<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('plan_m');
        $this->load->model('visitor_m');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }

    public function index()
    {
        $this->data['title'] = 'Web Live chat - Conversations';
        $this->data['subview'] = 'client/chat/index';
        $this->data['script'] = 'client/chat/script';
        $client_id = $this->session->userdata("user_id");
        // Retrive the list of vistiors for individual client
        $relation = array(
            "fields" => "*",
            'conditions' => " client_id = ".$client_id
        );
        $this->data['visitors'] = $this->visitor_m->get_relation("",$relation);
        $this->load->view("client_layout_main", $this->data);
    }

    public function find_visitor()
    {
        $relation = array(
            "fields" => "distinct(unique_id)",
            'conditions' => " client_id = ".$this->session->userdata('user_id')
        );
        $visitors= $this->visitor_m->get_relation("",$relation);
        if (!empty($visitors))
        {
            return $this->output->set_content_type('application/json')->set_output(json_encode(array("data"=> array_column($visitors,"unique_id"), "status"=> "yes")));
        }
        else{
            return $this->output->set_content_type('application/json')->set_output(json_encode(array("status"=>"no")));
        }
    }

   

}