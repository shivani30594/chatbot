<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('plan_m');
        $this->load->model('secondary_m');
        $this->load->model('user_m');
        $this->load->model('primary_m');
    }

    public function index()
    {
        $this->data['title'] = 'Register';
        $this->load->view('front/register', $this->data);
    }

    public function create($plan_id = '')
    {
        $relation = array(
            'fields' => '*',
            'conditions' => 'id = '.$plan_id
        );
        $found = $this->plan_m->get_relation('',$relation, true);
        if ($found == 0)
        {
            $this->session->set_flashdata('error', "Please select appropriate plan");
            redirect('register');
            exit;
        }
        $this->data['title'] = 'Register';
        $this->data['plan_id'] = $plan_id;
        $this->data['script'] = "front/script";
        $this->load->view('front/register_2', $this->data);
    }

    public function register_2()
    {
        $this->session->set_flashdata('error', "Please select one of the subscription plan");
        redirect("register");
    }

    public function register_3()
    {
        $this->session->set_flashdata('error', "Please select one of the subscription plan");
        redirect("register");
    }

    public function save_details()
    {
        $this->data['company_name'] = $this->input->post('company_name');
        $this->data['company_size'] = $this->input->post('company_size');
        $this->data['username'] = $this->input->post('email_id');
        $this->data['password'] = md5($this->input->post('password'));
        $this->data['plan_id'] = $this->input->post('plan_id');
        $this->data['title'] = 'Register';
        $this->data['script'] = "front/script";
        $this->load->view('front/register_3', $this->data);
    }

    public function final_save() 
    {
        if (!empty($this->input->post('email_id')))
        {
            $this->session->set_flashdata("error", "Please enter valid email-id!!!!");
            redirect('login');
        }
        $user_array['role'] = 'client';
        $user_array['email_id'] = $this->session->userdata('set_email');
        $user_array['password'] = $this->input->post('password');
        $user_array['is_active'] = 'Yes';
        $user_array['login_status'] = 'online';
        $user_array['unique_code'] = generate_refferal_code();
        $user_id = $this->user_m->save($user_array);
        $data = array(
            'user_id' =>$user_id,
            'u_loggedin' => TRUE
        );
        $this->session->set_userdata($data);
        if ($user_id)
        {
            $info['client_id'] = $user_id;
            $info['company_name'] = $this->input->post('company_name');
            $info['company_size'] = $this->input->post('company_size');
            $info['plan_id'] = $this->input->post('plan_id');
            $info['card_number'] = $this->input->post('card_number');
            $info['cvv_number'] = $this->input->post('cvv_number');
            $info['expiry_date'] = $this->input->post('expiry_month').'/'.$this->input->post('expiry_year');
            $info['website'] = $this->input->post('website');
            $result = $this->primary_m->save($info);
            if ($result)
            {
                $sec_array['client_id'] = $this->session->userdata('user_id');
                $this->secondary_m->save($sec_array);
                redirect("install");
            }
            else
            {
                $this->user_m->delete($user_id);
                $this->db->query('DELETE from webchat_users_primary WHERE client_id '.$user_id);
                $this->session->set_flashdata("error", "Opps Something went wrong!!");
                redirect('login');
            }
        }
        else
        {
            $this->user_m->delete($user_id);
            $this->db->query('DELETE from webchat_users_primary WHERE client_id '.$user_id);
            $this->session->set_flashdata("error", "Opps Something went wrong!!");
            redirect('login');
        }
    }
}
