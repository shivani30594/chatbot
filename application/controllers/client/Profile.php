<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('plan_m');
        $this->load->model('user_m');
        $this->load->model('secondary_m');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }

    public function index()
    {
        $relation = array(
            "fields" => "*",
            'conditions' => "client_id =" .$this->session->userdata('user_id')
        );
        $this->data['profile_info'] = $this->secondary_m->get_relation('',$relation);  
        $this->data['title'] = 'Profile';
        $this->data['subview'] = 'client/profile/index';
        $this->data['script'] = 'client/profile/script';
        $this->load->view("client_layout_main", $this->data);
    } 

    public function set_chatbot_image()
    {
        if (!empty($_FILES['chatbot_profile']['name']))
        {
           
            $config['upload_path']          = './uploads/profile';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000000;
            $config['file_name']           =  time();
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('chatbot_profile'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata("error", "Image is not uplodaded. Please try again");
                redirect('profile');
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                $array['chatbot_profile'] = $data['upload_data']['file_name'] ;
                $array['client_id'] = $this->session->userdata("user_id");
                $relation = array(
                    "fields" => "*",
                    'conditions' => "client_id =" .$this->session->userdata('user_id')
                );
                $setting_info = $this->secondary_m->get_relation('',$relation);  
                if (count($setting_info) > 0)
                {
                    $id = $this->secondary_m->save($array,$setting_info[0]['id']);
                }
                else{
                    $id = $this->secondary_m->save($array);
                }
                if ($id)
                {
                    $this->session->set_flashdata("success", "Profile updated successfully");
                }
                else{
                    $this->session->set_flashdata("error", "Something happens wrong");
                }
                redirect('profile');
            }
        }
    }

    public function set_chatbot_email()
    {
        $relation = array(
            "fields" => "*",
            'conditions' => "client_id =".$this->session->userdata('user_id')
        );
        $count = $this->secondary_m->get_relation("",$relation, true);
        if ($count > 0)
        {
            $this->db->where("client_id", $this->session->userdata('user_id'));
            $result = $this->db->update('webchat_users_secondary', array("chatbot_email" => $this->input->post('chatbot_email'),"name" => $this->input->post('name') ));
            $this->session->set_flashdata("success","Profile updated successfully");
        }
        else{
            $result = $this->db->insert('webchat_users_secondary', array("chatbot_email" => $this->input->post('chatbot_email'), "client_id"=>$this->session->userdata('user_id'),"name" => $this->input->post('name') ));
            $this->session->set_flashdata("success","Profile updated successfully");
        }
        redirect("profile");
    }
}                         