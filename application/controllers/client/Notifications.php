<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('visitor_m');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }
 
    public function set_sound_status()
    {
        if (!empty($this->input->post('sound_status')))
        {
            $array['sound_status'] =  $this->input->post('sound_status');
            $result = $this->user_m->save($array, $this->data['user_details']->id);
            if ($result)
            {
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes")));
            }
            else{
                return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"no")));
            }
        }
    }

    
    public function add_notification()
    {
        $relation = array(
            "fields" => "id, notifications_count",
            'conditions' => "unique_id ='" .$this->input->post('visitor_id')."'"
        );
        $result = $this->visitor_m->get_relation("",$relation);
        $data = array();
        if (!empty($result[0]['notifications_count']) || $result[0]['notifications_count'] != 0)
        {
            $data = array('notifications_count'=> $result[0]['notifications_count'] + 1 );
        }
        else{
            $data = array('notifications_count'=> 1);
        }
        $this->db->where('unique_id', $this->input->post('visitor_id'));
        $result = $this->db->update('webchat_visitors', $data);
        return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>$result)));
    }

    public function clear_notification()
    {
        $data = array('notifications_count'=>0);
        $this->db->where('client_id', $this->session->userdata('user_id') );
        $result = $this->db->update('webchat_visitors', $data);
        if ($result)
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>true)));
        else
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>false)));
    }

    public function notification_details()
    {
        $relation = array(
            "fields" => "*",
            'conditions' => "client_id ='".$this->session->userdata('user_id')."' AND chat_id != ''"
        );
        $notification_details = $this->visitor_m->get_relation("",$relation);
        if (count($notification_details) > 0)
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>true, "data"=>$notification_details)));
        else
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>false)));
    }

    public function reset_notification()
    {
        $data = array('notifications_count'=>0);
        $this->db->where('unique_id', $this->input->post('visitor_id') );
        $result = $this->db->update('webchat_visitors', $data);
        if (!$result)
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>true)));
        else
            return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>false)));
    }
}