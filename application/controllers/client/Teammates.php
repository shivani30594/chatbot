<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teammates extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('secondary_m');
        $this->load->model('teammateInvition_m');
        $this->load->model('teammate_m');
        $this->load->helper('email_helper');
        if ($this->user_m->u_loggedin() == FALSE) {
            redirect('login');
            exit;
        }
    }

    public function index()
    {
        $this->data['title'] = 'Web Live chat - Team-mates';
        $this->data['subview'] = 'client/teammates/index';
        $this->data['script'] = 'client/teammates/script';
        $relation = array(
            "fields" => "teammates.chatbot_profile as chatbot_profile ,webchat_teammates_invitation.teammate_email as chatbot_email,teammates.name as name, teammates.teammate_id as teammate_id, webchat_teammates_invitation.id as id  ",
            'JOIN' => array(
                array(
                    'table' => 'webchat_teammates as teammates',
                    'condition' => 'teammates.teammate_id = webchat_teammates_invitation.teammate_id',
                    'type' => 'LEFT'
                ),
            ),
            'conditions' => "webchat_teammates_invitation.is_confirm = 1 AND webchat_teammates_invitation.client_id = ".$this->session->userdata('user_id')
        );
        $this->data['teammates'] = $this->teammateInvition_m->get_relation("",$relation);
        $relation = array(
            "fields" => "*",
            "conditions" => "client_id = ".$this->session->userdata('user_id')
        );
        $this->data['client_info'] = $this->secondary_m->get_relation('', $relation);
        // Invited members
        $relation = array(
            "fields" => "teammates.chatbot_profile as chatbot_profile ,webchat_teammates_invitation.teammate_email as chatbot_email,teammates.name as name, webchat_teammates_invitation.id as id ",
            'JOIN' => array(
                array(
                    'table' => 'webchat_teammates as teammates',
                    'condition' => 'teammates.teammate_id = webchat_teammates_invitation.teammate_id',
                    'type' => 'LEFT'
                ),
            ),
            'conditions' => "webchat_teammates_invitation.created > DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND webchat_teammates_invitation.is_confirm = '0' AND webchat_teammates_invitation.client_id = ".$this->session->userdata('user_id')
        );
        $this->data['invited_teammates'] = $this->teammateInvition_m->get_relation("",$relation);
        $this->load->view("client_layout_main", $this->data);
    }

    public function invite_member()
    {
        $array = array();
        $array['teammate_email']  = $this->input->post('teammate_email');
        $array['client_id']  = $this->session->userdata('user_id');
        $array['verification_code'] = generate_refferal_code(20);
        $result = $this->teammateInvition_m->save($array);
        if ($result)
        {
            // save the details into usertable with role the teammate 
            $userArray['role'] = 'teammate';
            $userArray['email_id'] = $this->input->post('teammate_email');
            $userArray['unique_code'] = generate_refferal_code(12); 
            $userArray['password'] = md5($userArray['unique_code']);
            $userArray['login_status'] = 'offline';
            $userArray['is_active'] = 'No';
            $result_user = $this->user_m->save($userArray);
            if ($result_user)
            {
                $body = 'Click on "Activate Account" to confirm your activation.<a href='.BASE_URL.'verification/'.utf8_encode($array['verification_code']).'/'.$this->db->insert_id().'>'.BASE_URL.'verification/'.utf8_encode($array['verification_code']).'/'.$this->db->insert_id().'</a><br> ';
                $body .= 'Username :   '.$array['teammate_email'].'<br>';
                $body .= 'Password :   '.$userArray['unique_code'].'<br>';
                $this->load->library('email');
                $this->email->set_newline("\r\n");
                $this->email->from(EMAILID, 'myname');
                $this->email->to($array['teammate_email'] ); 
                $this->email->subject('Invite Teammate');
                $this->email->message($body);  
                $resultt = $this->email->send(); 
                if ($resultt)
                {
                    $this->session->set_flashdata('success','Your confirmation email will be set successfully');
                    redirect('teammates');
                }
                else {
                    $this->session->set_flashdata('error','Something happens wrong!');
                    redirect('teammates');
                }
            }
            else {
                $this->session->set_flashdata('error','Something happens wrong!!');
                redirect('teammates');
            }
        } else {
            $this->session->set_flashdata('error','Something happens wrong!!!');
            redirect('teammates');
        }  
    }

    public function verification($code = '', $user_id = '')
    {
        if (!empty($code))
        {
            $verification_code = utf8_decode($code);
            $relation = array(
                "fields" => "*",
                "conditions" => "verification_code = '".$verification_code."'"
            );
            $result = $this->teammateInvition_m->get_relation('', $relation);
            if (count($result) > 0)
            {
                $array['verification_code'] = generate_refferal_code(20);
                $array['is_confirm'] = 1;
                $array['teammate_id'] = $user_id;
                $res = $this->teammateInvition_m->save($array, $result[0]['id']);
                if ($res)
                {
                    $user_array['is_active'] = 'Yes';
                    $res = $this->user_m->save($user_array, $user_id);
                    // at the same time, we trigger PLSQL trigger which will insert the new row with null details and teammate_id [From the database]
                    $this->session->set_flashdata('success','Your confirmation will be set successfully');
                    redirect('login');
                }
                else {
                    $this->session->set_flashdata('error','Token is not valid');
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('error','Token is not valid');
                redirect('/');
            }
        }
    }
    
    public function check_email()
    {
        $email_id = $this->input->post('email_id');
        $relation = array(
            "fields" => "email_id",
            'conditions' => "email_id ='" .$email_id . "'"
        );
        $count = $this->user_m->get_relation('',$relation,true);
        if ($count > 0)
        {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => 'found')));
        }
        else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => 'not_found')));
        }
    }

    public function delete_teammate()
    {
        $id = $this->input->post('teammate_id');
        $teammate_invite['is_confirm'] = '0';
        $result = $this->teammateInvition_m->save($teammate_invite, $id);
        echo $this->db->last_query();
        if ($result)
        {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => 'success')));
        }
        else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => 'fail')));
        }
    }

}