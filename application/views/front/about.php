<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('components/header');?>
<body>
	<div class="top-border"></div>
	<nav class="navbar ">
		<div class="container">
			<div class="navbar-flex">
                <div class="navbar-header navbar-right">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand">
                        <img src="<?php echo IMG?>logo.png" class="img-responsive" alt="image">
                    </a>
                </div>
                <div class="input-group nav-input-group col-md-4">
                    <input type="text" class="form-control" placeholder="بريدك الالكتروني" aria-label="" aria-describedby="basic-addon1">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" type="button">إبدأ</button>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="<?php echo BASE_URL?>login">تسجيل دخول</a></li>
                        <li><a href="<?php echo BASE_URL?>pricing">أسعارنا</a></li>
                        <!-- <li class=""><a href="#">منتجاتنا</a></li> -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                منتجاتنا
                            </a>
                            <div class="dd-menu" aria-labelledby="navbarDropdown">
                                <div class="dd-inner-wrap">
                                    <div class="dd-content-icon">
                                        <img src="img/messenger-icon.png" class="img-responsive" alt="image">
                                    </div>
                                    <div class="dd-content">
                                        <h2>سوالفنا</h2>
                                        <P>إدارة الحديث المباشر، والبريد، والتواصل الاجتماعي </P>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="about">
			<div class="input-group header-input-group col-md-4">
                <input type="text" class="form-control" placeholder="بريدك الالكتروني" aria-label="" aria-describedby="basic-addon1">
                <div class="input-group-prepend">
                    <button class="btn btn-outline-secondary" type="button">إبدأ</button>
                </div>
            </div>
            <section class="header">
                <div class="header-content text-center">
                    <h2 class="">المنصة الاولى في الخليج التي تساعد</h2>
                    <h2>الشركات على تسريع النموّ </h2>
                </div>
            </section>
			<section class="about-inner">
                <div class="col-md-offset-5 col-md-7 col-sm-offset-4 col-sm-8 col-xs-12">
                    <div class="about-inner-wrap">
                        <h2>عنا</h2>
                        <div class="inner-content">
                            <p>
                                سوالف توفر للخليج طرق للحصول على عملاء جدد، وتوفير
                                .دعم لهم و الحفاظ عليهم
                            </p>
                            <p>
                                التحول لعصر الانترنت كان سبب سقوط الكثير
                                    من الشركات التي لم تستطيع الحفاظ على الأستمرارية فيمن
                                    الشركات التي لم تستطيع الحفاظ على الأستمرارية في
                                    عصر التكنلوجيا
                            </p>
                            <p>
                                سوالف تأسست لتساعد الشركات و الافراد الموتأسسون
                                بِتخاذ ارقى المنتجات لشركاتهم
                            </p>
                            <p>
                                سوالف توفر للخليج طرق للحصول على عملاء جدد، وتوفير
                                .دعم لهم و الحفاظ عليهم
                            </p>
                            <p>
                                سوالف تأسست لتساعد الشركات و الافراد الموتأسسون
                                بِتخاذ ارقى المنتجات لشركاتهم بِتخاذ ارقى المنتجات لشركاتهم
                            </p>
                        </div>	
                    </div>
                </div>
			</section>
			<section class="our-investor text-center">
                <h2>المستثمرين</h2>
                <div class="investor-logo">
                    <img src="<?php echo IMG?>trusted-company-logo.png" class="img-responsive" alt="image">
                </div>
            </section>
		</div>
	</div>
    <div class="grey-footer">
        <div class="container">
            <div class="grey-footer-inner">
                <a href="" class="s-link">س و ا ل ف</a>
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <h2>إبدأ الاصدار التجريبي مجانا ًلمدة ١١ يوم</h2>
                        <h4>أسعار خاصة للمشاريع الجديدة، فقط </h4>
                        <p class="price-text">
                            <span>فقط       د.أ للشهر</span>
                            <a href="">أعرف أ كثر .</a>
                        </p>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="email" class="form-control" id="email" placeholder="بريدك الالكتروني">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('components/footer')?>
</body>
<script>
    (function($){
        $(window).on("load",function(){
            $(".meta-content").mCustomScrollbar();
        });
    })(jQuery);
</script>
</html>