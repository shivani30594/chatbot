<!DOCTYPE html>
<html lang="en">
<?php  $this->load->view('components/header') ?>
<body>
    <div class="top-border"></div>
    <nav class="navbar ">
        <div class="container">
            <div class="navbar-flex">
                <div class="navbar-header navbar-right">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">
                        <img src="<?php echo IMG?>logo.png" class="img-responsive" alt="image">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="<?php echo BASE_URL?>login">تسجيل دخول</a></li>
                        <li><a href="<?php echo BASE_URL?>pricing">أسعارنا</a></li>
                        <li class=""><a href="#">منتجاتنا</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <section class="header">
            <div class="header-content">
                <h2>طريقة أمثل لتحويل المهتمين الى عملاء</h2>
                <p>أتصل مع عملائك وأ كبر اسرع</p>
                <form autocomplete="off">
                    <input type="email" class="form-control email" id="email" name="email"  placeholder="بريدك الالكتروني">
                    <label id="email_id_error" class="error" style="display:none">معرف البريد الإلكتروني موجود بالفعل</label>
                </form>
            </div>
        </section>
        <div class="intro">
            <h1 class="text-center">موثوق به من قبل أكثر الشركات ابتكاراً</h1>
                <div class="company-logo">
                    <img src="<?php echo IMG?>trusted-company-logo.png" class="img-responsive" alt="image">
                </div>
            <h1 class="text-center">مثالي للمبيعات والتسويق والدعم</h1>
        </div>
    </div>
    <div class="grey-footer">
        <div class="container">
            <div class="grey-footer-inner">
                <a href="" class="s-link">س و ا ل ف</a>

                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <h2>إبدأ الاصدار التجريبي مجانا ًلمدة ١١ يوم</h2>
                        <h4>أسعار خاصة للمشاريع الجديدة، فقط </h4>
                        <p class="price-text">
                            <span>فقط       د.أ للشهر</span>
                            <a href="">أعرف أ كثر .</a>
                        </p>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <form autocomplete="off">
                            <input type="email" class="form-control email" id="email_id" name="email_id" placeholder="بريدك الالكتروني">
                            <label id="email_id_found_error" class="error" style="display:none">معرف البريد الإلكتروني موجود بالفعل</label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('components/footer')?>
</body>
<?php $this->load->view('components/script')?>
<!-- <?php if (isset($script)) :?>
    <?php $this->load->view($script);?>
<?php endif;?> -->
</html>
<script>
    $(document).ready(function()
    {
        $(document).on('change', ".email", function(e)
        {
            jQuery.ajax({
                url : '<?= BASE_URL?>security/check_set_email',
                method: 'post',
                dataType: 'json',
                data: {email_id: e.target.value},
                success: function(response){
                    if (response.status == 'found')
                    {
                        document.getElementById("email_id_error").style.display = 'block';
                        $("#email").val('');
                    }
                    else
                    {
                        window.location.href= "<?= BASE_URL?>register";
                    }
                }
            });
        })

        $(document).on('change', "#email_id", function(e)
        {
            jQuery.ajax({
                url : '<?= BASE_URL?>security/check_set_email',
                method: 'post',
                dataType: 'json',
                data: {email_id: e.target.value},
                success: function(response){
                    if (response.status == 'found')
                    {
                        document.getElementById("email_id_found_error").style.display = 'block';                        
                        document.getElementById("email_id_error").style.display = 'none';
                        $("#email_id").val('');
                    }
                    else
                    {
                        window.location.href= "<?= BASE_URL?>register";
                    }
                }
            });
        })
    });
</script>

