<?php
header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('components/header');?>
<body>
    <div class="navbar-flex" id="navi">
        <div class="navbar-header navbar-right">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand navbar-brand-register-acc" href="#">
                <p> ابدأ مع سوالف </p>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="myNavbar-ul">
                <li><a href="<?php echo BASE_URL ?>register">١- اختر باقتك</a></li>
                <li><a href="<?php echo BASE_URL ?>register_2">٢- أنشأ حسابك</a></li>
                <li><a href="<?php echo BASE_URL ?>register_3" class="current">٣- أبدأ النسخة التجرديبية</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <section class="step-outer">
            <div id="step-3" class= "step-wrap step-three">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="login-form">
                            <div class="step-title">
                                <h2>ابدأ الإصدار التجريبي المجاني</h2>
                            </div>
                            <form id="register_3" action="<?=BASE_URL?>final_save" method="post" autocomplete="off" >
                                <fieldset class="">
                                    <input type="hidden" name="password" id="password" value="<?php echo isset($password) ? $password : '' ?>">
                                    <input type="hidden" name="plan_id" id="plan_id" value="<?php echo isset($plan_id) ? $plan_id : '' ?>">
                                    <input type="hidden" name="company_size" id="company_size" value="<?php echo isset($company_size) ? $company_size : '' ?>">
                                    <input type="hidden" name="company_name" id="company_name" value="<?php echo isset($company_name) ? $company_name : '' ?>">
                                    <div class="">
                                        <label class="label"> معلومات بطاقة الائتمان <i class="fa fa-lock" aria-hidden="true"></i></label>
                                        <div class="small-inputs custom-paymemt-inputs form-group">
                                            <div class='icon'></div>
                                            <input type="text" class="form-control input-card card-nm" placeholder='Card Number' name="card_number" id="card_number" onkeypress="return isNumber(event)">
                                            <input type="text" name="expiry_month" class="form-control month" placeholder="MM" id="month" onkeypress="return isNumber(event)">
                                            <span>/</span>
                                            <input type="text" name="expiry_year" class="form-control year" placeholder="YY" id="year" onkeypress="return isNumber(event)">
                                            <input type="text"  name="cvv_number" class="form-control cvv" placeholder="CVV" id="verification" onkeypress="return isNumber(event)">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="label"> الموقع الالكتروني للشركة</label>
                                        <div class="custome-card-input company-website">
                                            <div class='icon'></div>
                                            <input type="text" class="form-control input-card" name="website" id="website">
                                        </div>
                                        <label class="error" id="website-error-label" style="display:none;">Please enter valid URL</label>
                                    </div>
                                </fieldset>
                                <div class="celarfix">
                                    <button type="submit" class="btn btn-primary">ابدأ الإصدار التجريبي المجاني الذي تبلغ مدته 14 يومًا <i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                                </div>
                                <p class="notes-big"><i class="fa fa-check" aria-hidden="true"></i> لن تتم محاسبتك اليوم </p>
                                <p class="notes-big"><i class="fa fa-check" aria-hidden="true"></i> إلغاء في أي وقت قبل 17 ديسمبر / كانون الأول </p>
                                <p class="notes-big"><i class="fa fa-check" aria-hidden="true"></i> أدخل تاريخ الإلغاء قبل << تاريخ العرض بعد 14 يومًا >> </p>
                                <p class="notes">
                                        بالنقر على "بدء الإصدار التجريبي المجاني لمدة 14 يومًا" ، فإنك توافق
                                        على<a href="">  شروط الخدمة </a>و<a href=""> سياسة الخصوصية في </a>
                                        Swaflna. سيتم فرض رسوم على بطاقتك الائتمانية شهريًا وقد تختلف الرسوم وفقًا لكيفية دفع الفاتورة.
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- <div class="chat-icon">
        <a href="">سِ</a>
    </div> -->
</body>

<?php $this->load->view('components/script');?>
<script>
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    $(function () {
        // $('.cc-expires').formatCardExpiry();
        // $('.cc-cvc').formatCardCVC();

        // $(":input").inputmask();
    });

    $(document).on('change', '#month', function()
    {
        var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
        
    });


</script>
<?php $this->load->view($script);?>
</html>
