<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('components/header');?>
<body>
	<div class="top-border"></div>
	<nav class="navbar ">
        <div class="container">
            <div class="navbar-flex">
                <div class="navbar-header navbar-right">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">
                        <img src="<?php echo IMG?>logo.png" class="img-responsive" alt="image">
                    </a>
                </div>
                <div class="input-group nav-input-group col-md-4">
                    <input type="text" class="form-control" placeholder="بريدك الالكتروني" aria-label="" aria-describedby="basic-addon1">
                    
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" type="button">إبدأ</button>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="<?php echo BASE_URL?>login">تسجيل دخول</a></li>
                        <li><a href="<?php echo BASE_URL?>pricing">أسعارنا</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                منتجاتنا
                            </a>
                            <div class="dd-menu" aria-labelledby="navbarDropdown">
                                <div class="dd-inner-wrap">
                                    <div class="dd-content-icon">
                                        <img src="<?php echo IMG;?>messenger-icon.png" class="img-responsive" alt="image">
                                    </div>
                                    <div class="dd-content">
                                        <h2>سوالفنا</h2>
                                        <P>إدارة الحديث المباشر، والبريد، والتواصل الاجتماعي </P>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
			</div>
		</div>
    </nav>
	<div class="container">
		<div class="pricing">
            <div class="input-group header-input-group col-md-4">
                <input type="text" class="form-control" placeholder="بريدك الالكتروني" aria-label="" aria-describedby="basic-addon1">
                <div class="input-group-prepend">
                    <button class="btn btn-outline-secondary" type="button">إبدأ</button>
                </div>
            </div>
            <section class="header">
                <div class="header-content text-center">
                    <div class="logo-intro">
                        <p>سوالفنا</p>
                        <img src="<?php echo IMG?>messenger-icon.png" class="img-responsive" alt="image">
                    </div>
                    <h3>من ٩٩د.أ. فقط</h3>
                    <h4>إدارة الحديث المباشر</h4>
                    <h4> والبريد، والتواصل الاجتماعي</h4> <br>
                    <h4>تجربة  اي من كل الباقات لمدة ١٤ يوم مجاناً</h4>
                    <!-- <a  id="button_scroll_down"><p> أعرف الزيد </p> <i class="fa fa-arrow-down" aria-hidden="true"></i> </a> -->
                </div>
            </section>
			<!-- <section class="pricing-intro text-center">
				<h1 class="">ادفع لما تحتاج فقط</h1>
                <div class="pricing-intro-inner">
                    <p>ابدأ الاصدار المجاني </p>
                    <div class="input-group nav-input-group col-md-4">
                 
                        <input type="text" class="form-control" placeholder="بريدك الالكتروني" aria-label="" aria-describedby="basic-addon1">

                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary" type="button">إبدأ</button>
                        </div>
                    </div>
                    <ul>
                        <li><a href="">اصدار مجاني لمدة ١٤ يوم</a></li>
                        <li><a href="">إلغاء الاشتراك في اي وقت</a></li>
                    </ul>
                </div>
			</section> -->
			<!-- <section class="pricing-inner">
				<div class="col-md-7 col-sm-7 col-xs-12">
					<div class="left">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#menu1">الاسعار</a></li>
                            <li><a data-toggle="tab" href="#menu2">مميزاتنا</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="menu1" class="tab-pane fade in active">
                                <div class="price-tbl">
                                    <div class="list">
                                        <p class="title">سعر المستخدم</p>
                                        <p>للمستخدم الواحد</p>
                                        <p>لمستخدمين</p>
                                    </div>
                                    <div class="price text-center">
                                        <p class="title">الرئيسي</p>
                                        <p>د.أ</p>
                                        <p>د.أ</p>
                                    </div>

                                </div>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <div class="feature-tbl">
                                        <div class="list">
                                            <p class="title">مميزاتنا</p>
                                            <p>محدث سوالفنا</p>
                                            <p>صفحة المعلمومات</p>
                                            <p>اوقات العمل</p>
                                            <p>التقاط البيانات</p>
                                        </div>
                                        <div class="feature-exist text-center">
                                            <p class="title">الرئيسية</p>
                                            <p> <i class="fa fa-check" aria-hidden="true"></i> </p>
                                            <p> <i class="fa fa-check" aria-hidden="true"></i> </p>
                                            <p> <i class="fa fa-check" aria-hidden="true"></i> </p>
                                            <p> <i class="fa fa-check" aria-hidden="true"></i> </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="right text-center">
                            <div class="logo-intro">
                                <img src="<?php echo IMG?>messenger-icon.png" class="img-responsive" alt="image">
                                <p>سوالفنا</p>
                            </div>
                            <h1>إدارة الحديث المباشر<br>
                            والبريد</h1>
                            <h1>ترقبونا للأسعار<br>
                                الشهر /</h1>
                        </div>
                    </div>
                </div>
            </section> -->
        </div>
    </div>

    <div class="container-fluid plan-block-bg">
        <div class="container">
            <div class="plan-block">
                <h2 class="plan-block-title">ساعد عملائك على محادثات مباشرة</h2>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="plan-wrap">
                            <div class="paln-intro-wrap">
                                <!-- <div class="paln-intro">
                                    <img src="http://18.217.216.88/Chatbot//assets/img/logo-symbol.png" class="img-responsive" alt="image">
                                </div>     -->
                                <div class="plan-name"> 
                                    <div class="name">الرئيسي</div>
                                </div>  
                                <div class="price">
                                    <h4>من ٩٩ د. أ. /شهر</h4>
                                </div> 
                                <h5 class="meta-infor">أدعم عملائك من خلال قنوات متعددة </h5> 
                                <a href="http://18.217.216.88/Chatbot/create/3" class="plan-wrap-a">جرب مجاناً</a>  
                            </div>
                            <div class="bottom-wrap">
                                <h4>كيف نسعر خدماتنا</h4>
                                <ul>
                                    <li class="title">&nbsp;</li>
                                    <li>إدارة المحادثات من البريد الإلكتروني والدردشة</li>
                                    <li>تعاون كفريق واحد لحل أسئلة الدعم</li>
                                    <li>ادمج سوالف مع</li>
                                </ul>  
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="plan-wrap pro-plan">
                            <h3 class="most-used"> 68 ٪ من الشركات تختار الموالية </h3>
                            <div class="paln-intro-wrap">
                                <!-- <div class="paln-intro ">
                                    <img src="http://18.217.216.88/Chatbot//assets/img/logo-symbol.png" class="img-responsive" alt="image">
                                </div>     -->
                                <div class="plan-name"> 
                                    <div class="name">محترف</div>
                                    
                                </div>   
                                <div class="price">
                                        <h4>من ١٧٩ د. أ. /شهر</h4>
                                    </div>
                                <h5 class="meta-infor">أحصل على مميزات ذكية لتبسيط سير العمل</h5>
                                <a href="http://18.217.216.88/Chatbot/create/2" class="plan-wrap-a"> جرب مجاناً </a>  
                            </div>
                            <div class="bottom-wrap">
                                <h4>كيف نسعر خدماتنا</h4>
                                <ul>
                                    <li class="title">كل مميزات الرئيسي مع</li>
                                    <li>خصص المحادثات و سير المحادثات بالذكاء الاصتناعي </li>
                                    <li>دمج المحادثات من القنوات الاجتماعية</li>
                                    <li>السيطرة على من يرى حالة المحدث ومتى</li>
                                    
                                </ul>  
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="plan-wrap">
                            <div class="paln-intro-wrap">
                                <!-- <div class="paln-intro">
                                    <img src="http://18.217.216.88/Chatbot//assets/img/logo-symbol.png" class="img-responsive" alt="image">
                                </div>     -->
                                <div class="plan-name"> 
                                    <div class="name">مؤسسة</div>
                                </div> 
                                <div class="price">
                                        <h4>تسعير حسب الطلب</h4>
                                </div>  
                                <h5 class="meta-infor">خصص على حاجة مؤسستك بخدمات خاصة للمؤسسات</h5> 
                                <a href="http://18.217.216.88/Chatbot/create/1" class="plan-wrap-a">تحدث معنا </a>  
                            </div>
                            <div class="bottom-wrap">
                                <h4>كيف نسعر خدماتنا</h4>
                                <ul>
                                    <li class="title">كل مميزات باقة المحترف مع</li>
                                    <li>حذف العلامة التجارية من محدث سوالف</li>
                                    <li>خصص المحادثات بأيقاف استقبال المرفقات</li>
                                    <li>إدارة الكميات العالية من المحادثات مع وضع حدود وتحويل متحدث  </li>
                                </ul>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hide-show-feature">
                    <a href="javascript:void(0)" class="hide-show-feature-a" id="close_button">  إخفاء المميزات الاضافية  <i id="down" class="fa fa-caret-down" aria-hidden="true" ></i>  <i style="display:none" id="cross" class="fa fa-times" aria-hidden="true"></i><span id="btn_status" style="display:none">on</span> </a>
                </div>
            </div>
            <div class="features-wrap">
                <div class="features">
                    <div class="f-title"><h2 class=""> إدارة المحادثة الواردة </h2></div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="feature-inner-wrap">
                            <div class="f-mobile-title"><h2 class=""> إدارة المحادثة الواردة </h2></div>
                            <ul>
                                <li> دردشة مباشرة </li>
                                <li> تطبيقات الموبايل </li>
                                <li> البريد </li>
                                <li class="disable" > حالة التواجد </li>
                                <li class="disable" > Twitter دمج  </li>
                                <li class="disable" > Facebook دمج  </li>
                                <li class="disable" > إزالة علامة سوالف التجارية </li>
                                <li class="disable" > تخصيص عالي على محدث سوالف </li>
                                <li class="disable" > قم بتعيين حدود للمحادثة  مع إدارة اكتفاء العمل </li>
                                <li class="disable" > تحويل المحادثات الى افراد الفريق </li>
                            </ul>
                        </div> 
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                        <div class="feature-inner-wrap">
                        <div class="f-mobile-title"><h2 class=""> إدارة المحادثة الواردة </h2></div>
                            <ul>
                                <li> دردشة مباشرة </li>
                                <li> تطبيقات الموبايل </li>
                                <li> البريد </li>
                                <li > حالة التواجد </li>
                                <li > Twitter دمج  </li>
                                <li > Facebook دمج  </li>
                                <li class="disable" > إزالة علامة سوالف التجارية </li>
                                <li class="disable" > تخصيص عالي على محدث سوالف </li>
                                <li class="disable" > قم بتعيين حدود للمحادثة  مع إدارة اكتفاء العمل </li>
                                <li class="disable" > تحويل المحادثات الى افراد الفريق </li>
                            </ul> 
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="feature-inner-wrap">
                        <div class="f-mobile-title"><h2 class=""> إدارة المحادثة الواردة </h2></div>
                            <ul>
                                <li> دردشة مباشرة </li>
                                <li> تطبيقات الموبايل </li>
                                <li> البريد </li>
                                <li > حالة التواجد </li>
                                <li > Twitter دمج  </li>
                                <li > Facebook دمج  </li>
                                <li > إزالة علامة سوالف التجارية </li>
                                <li > تخصيص عالي على محدث سوالف </li>
                                <li > قم بتعيين حدود للمحادثة  مع إدارة اكتفاء العمل </li>
                                <li > تحويل المحادثات الى افراد الفريق </li>
                            </ul> 
                        </div>
                    </div>
                </div>
                <div class="features features-2">
                        <div class="f-title"><h2 class=""> سير العمل  </h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  سير العمل </h2></div>
                                <ul>
                                    <li> مجمع البريد الالكتروني </li>
                                    <li> ملفات العملاء و العملاء المحتملين </li>
                                    <li> حالة تواجد فريق العمل وحالة الغياب  </li>
                                    <li class="disable" > جدول الاجتماعات الالي </li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  سير العمل </h2></div>
                                <ul>
                                    <li> مجمع البريد الالكتروني </li>
                                    <li> ملفات العملاء و العملاء المحتملين </li>
                                    <li> حالة تواجد فريق العمل وحالة الغياب  </li>
                                    <li > جدول الاجتماعات الالي </li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  سير العمل </h2></div>
                                <ul>
                                    <li> مجمع البريد الالكتروني </li>
                                    <li> ملفات العملاء و العملاء المحتملين </li>
                                    <li> حالة تواجد فريق العمل وحالة الغياب  </li>
                                    <li > جدول الاجتماعات الالي </li>
                                </ul> 
							</div>
                        </div>
                </div>
                <div class="features features-3">
                    <div class="f-title"><h2 class=""> تعاون كفريق </h2></div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="feature-inner-wrap">
                            <div class="f-mobile-title"><h2 class=""> تعاون كفريق  </h2></div>
                            <ul>
                                <li> البريد الوارد للفريق  </li>
                                <li> أوقات العمل  </li>
                                <li> الردود المحفوظة  </li>
                                <li class="disable" > قوانين للمهام </li>
                                <li class="disable" > ادن للمحادثات </li>
                                <li class="disable" > سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                        <div class="feature-inner-wrap">
                            <div class="f-mobile-title"><h2 class=""> تعاون كفريق  </h2></div>
                            <ul>
                                <li> البريد الوارد للفريق  </li>
                                <li> أوقات العمل  </li>
                                <li> الردود المحفوظة  </li>
                                <li> قوانين للمهام </li>
                                <li> ادن للمحادثات </li>
                                <li class="disable" > سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="feature-inner-wrap">
                            <div class="f-mobile-title"><h2 class=""> تعاون كفريق  </h2></div>
                            <ul>
                                <li> البريد الوارد للفريق  </li>
                                <li> أوقات العمل  </li>
                                <li> الردود المحفوظة  </li>
                                <li> قوانين للمهام </li>
                                <li> ادن للمحادثات </li>
                                <li> سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                    </div>
                </div>
                <div class="features features-4">
                        <div class="f-title"><h2 class=""> تقارير الاداء </h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  تقارير الاداء </h2></div>
                                <ul>
                                    <li>ملخص الاداء</li>
                                    <li>ملخص العملاء المحتملون</li>
                                    <li>ملخص اداء الفريق</li>
                                    <li>تصدير</li>
                                    <li>تصدير المحادثات كاملة</li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  تقارير الاداء </h2></div>
                                <ul>
                                    <li>ملخص الاداء</li>
                                    <li>ملخص العملاء المحتملون</li>
                                    <li>ملخص اداء الفريق</li>
                                    <li>تصدير</li>
                                    <li>تصدير المحادثات كاملة</li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  تقارير الاداء </h2></div>
                                <ul>
                                    <li>ملخص الاداء</li>
                                    <li>ملخص العملاء المحتملون</li>
                                    <li>ملخص اداء الفريق</li>
                                    <li>تصدير</li>
                                    <li>تصدير المحادثات كاملة</li>
                                </ul> 
							</div>
                        </div>
                </div>
                <div class="features features-5">
                        <div class="f-title"><h2 class=""> الدعم والخدمات </h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  الدعم والخدمات  </h2></div>
                                <ul>
                                    <li> مركز معرفة </li>
                                    <li> دعم البريد الالكتروني </li>
                                    <li class="disable"> دعم دردشة مباشر </li>
                                    <li class="disable" > دعم على مدار الساعة طول  ابام الاسبوع </li>
                                    <li class="disable" > الحصول على اجدد المميزات قبل  توافرهن للعامة </li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  الدعم والخدمات  </h2></div>
                                <ul>
                                    <li> مركز معرفة </li>
                                    <li> دعم البريد الالكتروني </li>
                                    <li> دعم دردشة مباشر </li>
                                    <li class="disable" > دعم على مدار الساعة طول  ابام الاسبوع </li>
                                    <li class="disable" > الحصول على اجدد المميزات قبل  توافرهن للعامة </li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  الدعم والخدمات  </h2></div>
                                <ul>
                                    <li> مركز معرفة </li>
                                    <li> دعم البريد الالكتروني </li>
                                    <li> دعم دردشة مباشر </li>
                                    <li> دعم على مدار الساعة طول  ابام الاسبوع </li>
                                    <li> الحصول على اجدد المميزات قبل  توافرهن للعامة </li>
                                </ul> 
							</div>
                        </div>
                </div>
                <!-- <div class="features features-6">
                        <div class="f-title"><h2 class="">التطبيقات والتكاملات</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="disable" > Zendesk </li>
                                <li class="disable" > جيثب </li>
                                <li class="disable" > قوة المبيعات الأساسية </li>
                                <li class="disable" > Marketo </li>
                                <li class="disable" > Clearbit </li>
                            </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="" > Zendesk </li>
                                <li class="" > جيثب </li>
                                <li class="" > قوة المبيعات الأساسية </li>
                                <li class="disable" > Marketo </li>
                                <li class="disable" > Clearbit </li>
                            </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="" > Zendesk </li>
                                <li class="" > جيثب </li>
                                <li class="" > قوة المبيعات الأساسية </li>
                                <li class="" > Marketo </li>
                                <li class="" > Clearbit </li>
                            </ul> 
							</div>
                        </div>
                </div>
                <div class="features features-7">
                        <div class="f-title"><h2 class="">الدعم والخدمات</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="disable" > دعم الدردشة</li>
                                <li class="disable" > فريق نجاح شخصي </li>
                                <li class="disable" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 
							</div>
                            <a href="<?php echo BASE_URL.'create/3'?>" class="plan-wrap-a">دردش معنا</a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border pro-plan">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="" > دعم الدردشة</li>
                                <li class="disable" > فريق نجاح شخصي </li>
                                <li class="disable" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 
							</div>

                            <a href="<?php echo BASE_URL.'create/2'?>" class="plan-wrap-a">  دردش معنا <i class="fa fa-arrow-left" aria-hidden="true"></i> </a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="" > دعم الدردشة</li>
                                <li class="" > فريق نجاح شخصي </li>
                                <li class="" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 
							</div>

                            <a href="<?php echo BASE_URL.'create/1'?>" class="plan-wrap-a">دردش معنا <i class="fa fa-arrow-left" aria-hidden="true"></i> </a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
		<!-- <div class="grey-footer">
			<div class="container">
				<div class="grey-footer-inner">
					<a href="" class="s-link">س و ا ل ف</a>
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<h2>إبدأ الاصدار التجريبي مجانا ًلمدة ١١ يوم</h2>
							<h4>أسعار خاصة للمشاريع الجديدة، فقط </h4>
							<p class="price-text">
								<span>فقط       د.أ للشهر</span>
								<a href="">أعرف أ كثر .</a>
							</p>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
							 <input type="email" class="form-control" id="email" placeholder="بريدك الالكتروني">
						</div>
					</div>
				</div>
			</div>
        </div> -->
    </div>
    <!-- <div class="pricing-new-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-4 col-sm-4 col-xs-12">
                    <div class="pricing-new-footer-inner text-center">
                        <h3>Early stage startup?</h3>
                        <p>Eligible appliactions get all of Intercom for $49/mo.</p>
                        <a href=""> Apply now <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div>   
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="pricing-new-footer-inner text-center">
                        <h3>Annual plans available</h3>
                        <p>Get in touch with our sales team for discounted annual pricing.</p>
                        <a href="">Contact Us <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div>  
            </div>
        </div>
    </div> -->
</div>
<footer class="grey-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer-content">
                    <h2>الشركة</h2>
                    <ul>
                        <li><a href="http://18.217.216.88/Chatbot/about">عن سوالف</a></li>
                        <li><a href="javascript:void(0)">عملائنا</a></li>
                        <li><a href="javascript:void(0)">وظائف</a></li>
                        <li><a href="http://18.217.216.88/Chatbot/terms">شروط</a></li>
                        <li><a href="http://18.217.216.88/Chatbot/privacy">سرية</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer-content">
                    <h2>مميزات</h2>
                    <ul>
                        <li><a href="javascript:void(0)">إشعارات مباشرة</a></li>
                        <li><a href="javascript:void(0)">برمجية محادة</a></li>
                        <li><a href="javascript:void(0)">برامج أبل و أندرويد</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer-content">
                    <h2>منتجات</h2>
                    <ul>
                        <li><a href="javascript:void(0)">سوالفنا</a></li>
                    </ul>
                    <ul class="footer-bootm-link">
                        <li><a href="http://18.217.216.88/Chatbot/pricing">أسعارنا</a></li>
                        <li><a href="javascript:void(0)">سوالف للشركات البتدأة</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
<?php $this->load->view('components/script')?>
<script>
     $( document ).ready(function() {
        $(document).on('click', '#button_scroll_down',function(event) {
            $('html, body').animate({
                'scrollTop' : $(".pricing-intro").position().top
            });
        });
    });

    $(document).on('click',"#close_button", function()
    {
        if ($("#btn_status").text() == 'off')
        {
            $("#cross").css("display","none"); 
            $("#down").css("display","inline-block");
        } else
        {
            $("#cross").css("display","inline-block"); 
            $("#down").css("display","none");  
        }
        $("#btn_status").text() == "off" ? $("#btn_status").text("on") : $("#btn_status").text("off");
        $(".features-wrap").slideToggle("1000");
    });
</script>
</html>