<script type="text/javascript">
    jQuery(document).ready(function()
    {
        $('#login_form').validate({
            rules: {
                email_id : { 
                    required :true,
                },
                password : { 
                    required :true,
                },
            },
            messages: {
                email_id : {
                    required : 'Enter Email-Id',
                },
                password : {
                    required : 'Enter Password',
                },
            }
        });
        
        $('#register_2').validate({
            rules: {
                company_name : { 
                    required :true,
                },
                company_size : { 
                    required :true,
                },
                email_id : { 
                    required :true,
                },
                password : { 
                    required :true,
                },
            },
            messages: {
                company_name : {
                    required : '',
                },
                company_size : {
                    required : ' ',
                },
                email_id : {
                    required : '',
                },
                password : {
                    required : '',
                },
            }
        });
            jQuery.validator.addMethod('CCExpYear', function(value, element, params) {
                var minYear = new Date().getFullYear().toString().substr(-2);
                var year = parseInt($(params.year).val(), 10);
                return (!month || !year || year > minYear || (year === minYear));
            }, '');

            jQuery.validator.addMethod('CCExpMonth', function(value, element, params) {
                var minMonth = new Date().getMonth() + 1;
                var month = parseInt($(params.month).val(), 10);
                
                return (!month || !year || (month >= minMonth && month <= 12));
            }, '');

            jQuery.validator.addMethod("urlCheck", function(value, element) {
                return this.optional( element ) || /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/.test( value );
            }, "");
          $('#register_3').validate({
            rules: {
                card_number : { 
                    required :true,
                    minlength : 16,
                    maxlength : 16
                },
                website : { 
                    required :true,
                    urlCheck : true
                },
                cvv_number : { 
                    required :true,
                    minlength : 3,
                    maxlength : 3
                },
                expiry_year : { 
                    CCExpYear: {
                        year: '#year'
                    },
                    required :true,
                    minlength : 2,
                    maxlength : 2
                },
                expiry_month : { 
                    CCExpMonth: {
                        month: '#month'
                    },
                    required :true,
                    minlength : 2,
                    maxlength : 2
                },
            },
            messages: {
                card_number : {
                    required : '',
                    minlength : '',
                    maxlength : ''
                },
                website : {
                    required : ' ',
                    urlCheck : ' ',
                },
                cvv_number : {
                    required : '',
                    minlength : '',
                    maxlength : ''
                },
                expiry_year : {
                    required : '',
                    minlength : '',
                    maxlength : ''
                },
                expiry_month : {
                    required : '',
                    minlength : '',
                    maxlength : ''
                },
            }
        });

        $(document).on('change', '#email_id', function()
        {
            jQuery.ajax({
                url : '<?php echo BASE_URL?>security/check_email',
                method: 'post',
                dataType: 'json',
                data: {email_id: $("#email_id").val()},
                success: function(response){
                    if (response.status == 'found')
                    {
                        document.getElementById("email_id_error").style.display = 'block';
                        $("#email_id").val('');
                    }
                    else
                    {
                        document.getElementById("email_id_error").style.display = 'none';
                        $("#email_id-error").hide();
                    }
                }
            });
        });

        // $(document).on('change', '#email', function()
        // {
        //     jQuery.ajax({
        //         url : '<?php echo BASE_URL?>security/check_email',
        //         method: 'post',
        //         dataType: 'json',
        //         data: {email_id: $("#email").val()},
        //         success: function(response){
        //             if (response.status == 'found')
        //             {
        //                 document.getElementById("email_error").style.display = 'block';
        //                 $("#email").val('');
        //             }
        //             else
        //             {
        //                 document.getElementById("email_error").style.display = 'none';
        //                 $("#email-error").hide();
        //             }
        //         }
        //     });
        // });
        

        // $(document).on('change', '#year', function()
        // {
        //     if ($(this).val() < 30 && $(this).val() > 94)
        //     {
        //         $("#year-error").css("display","none");
        //     }
        //     else{
        //         console.log("elser")
        //         $("#year-error").css("display","inline-block");
        //     }
        // });
      
        // $(document).on('change', '#month', function()
        // {
        //     if ($(this).val() > 12)
        //     {
        //         $("#month-error").css("display","none");
        //     }
        //     else{
        //         console.log("elser")
        //         $("#month-error").css("display","inline-block");
        //     }
        // });


      
    });
</script>