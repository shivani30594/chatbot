<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('components/header');?>
<link rel="stylesheet" href="<?php echo CSS?>jquery.mCustomScrollbar.css" />
<body>
    <div class="top-border">
    </div>
    <nav class="navbar ">
        <div class="container">
            <div class="navbar-flex">
                <div class="navbar-header navbar-right">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="<?php echo IMG?>logo.png" class="img-responsive" alt="image">
                    </a>
                </div>
                <div class="input-group nav-input-group col-md-4">
                    <input type="text" class="form-control" placeholder="بريدك الالكتروني" aria-label="" aria-describedby="basic-addon1">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" type="button">إبدأ</button>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="#">تسجيل دخول</a></li>
                        <li><a href="#">أسعارنا</a></li>
                        <li class=""><a href="#">منتجاتنا</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
	<div class="container">
		<div class="terms">
			<div class="input-group header-input-group col-md-4">
                <input type="text" class="form-control" placeholder="بريدك الالكتروني" aria-label="" aria-describedby="basic-addon1">
                <div class="input-group-prepend">
                    <button class="btn btn-outline-secondary" type="button">إبدأ</button>
                </div>
            </div>
				<section class="header">
					<div class="header-content">
						<h2 class="green">سوالف للشركات الجدد</h2>
					</div>
				</section>


				<section class="terms-section">
						
						 <div  class="terms-inner-wrap">
					        <div class="col-md-8 col-sm-8 col-xs-12">
					          <!-- Tab panes -->
					          <div class="tab-content">
					            <div class="tab-pane active" id="tab1-vr">
					            	<h2>شروط الخدمة</h2>
									<div class="meta-content">
										<P>يتم تحرير فاتورة لك شهريًا لكل تطبيق استنادًا إلى:
											المنتجات في اشتراكك ،
											الخطة التي تختارها ، و
											عدد الأشخاص (المستخدمين والناشطون) الذين يتتبعهم
											 تطبيقك.
											هذه هي "مقاييس الأسعار" التي يتم استخدامها لحساب
											 الفاتورة. نرسل إليك فاتورة لك كل شهر مسبقًا استنادًا إلى
											 ما كانت عليه مقاييس الأسعار في اليوم الأخير من الشهر
											 السابق.</P>
											 <p>
											 	يتم تحرير فاتورة لك شهريًا لكل تطبيق استنادًا إلى:
											المنتجات في اشتراكك ،
											الخطة التي تختارها ، و
											عدد الأشخاص (المستخدمين والناشطون) الذين يتتبعهم
											 تطبيقك.
											هذه هي "مقاييس الأسعار" التي يتم استخدامها لحساب
											 الفاتورة. نرسل إليك فاتورة لك كل شهر مسبقًا استنادًا إلى
											 ما كانت عليه مقاييس الأسعار في اليوم الأخير من الشهر
											 السابق.
											 </p>
									</div>
					            </div>

					            <div class="tab-pane" id="tab2-vr">
					            	<h2>سياسة الخصوصية</h2>
									<div class="meta-content">
										<P>يتم تحرير فاتورة لك شهريًا لكل تطبيق استنادًا إلى:
											المنتجات في اشتراكك ،
											الخطة التي تختارها ، و
											عدد الأشخاص (المستخدمين والناشطون) الذين يتتبعهم
											 تطبيقك.
											هذه هي "مقاييس الأسعار" التي يتم استخدامها لحساب
											 الفاتورة. نرسل إليك فاتورة لك كل شهر مسبقًا استنادًا إلى
											 ما كانت عليه مقاييس الأسعار في اليوم الأخير من الشهر
											 السابق.</P>
											 <p>
											 	يتم تحرير فاتورة لك شهريًا لكل تطبيق استنادًا إلى:
											المنتجات في اشتراكك ،
											الخطة التي تختارها ، و
											عدد الأشخاص (المستخدمين والناشطون) الذين يتتبعهم
											 تطبيقك.
											هذه هي "مقاييس الأسعار" التي يتم استخدامها لحساب
											 الفاتورة. نرسل إليك فاتورة لك كل شهر مسبقًا استنادًا إلى
											 ما كانت عليه مقاييس الأسعار في اليوم الأخير من الشهر
											 السابق.
											 </p>
									</div>
					            </div>

					            <div class="tab-pane " id="tab3-vr">
					            	<h2>كيفية يتم الدفع</h2>
									<div class="meta-content">
										<P>يتم تحرير فاتورة لك شهريًا لكل تطبيق استنادًا إلى:
											المنتجات في اشتراكك ،
											الخطة التي تختارها ، و
											عدد الأشخاص (المستخدمين والناشطون) الذين يتتبعهم
											 تطبيقك.
											هذه هي "مقاييس الأسعار" التي يتم استخدامها لحساب
											 الفاتورة. نرسل إليك فاتورة لك كل شهر مسبقًا استنادًا إلى
											 ما كانت عليه مقاييس الأسعار في اليوم الأخير من الشهر
											 السابق.</P>
											 <p>
											 	يتم تحرير فاتورة لك شهريًا لكل تطبيق استنادًا إلى:
											المنتجات في اشتراكك ،
											الخطة التي تختارها ، و
											عدد الأشخاص (المستخدمين والناشطون) الذين يتتبعهم
											 تطبيقك.
											هذه هي "مقاييس الأسعار" التي يتم استخدامها لحساب
											 الفاتورة. نرسل إليك فاتورة لك كل شهر مسبقًا استنادًا إلى
											 ما كانت عليه مقاييس الأسعار في اليوم الأخير من الشهر
											 السابق.
											 </p>
									</div>		
					            </div>

					          </div>
					        </div>

					        <div class="col-md-4 col-sm-4 col-xs-12"> <!-- required for floating -->
					          <!-- Nav tabs -->
					          <ul class="nav nav-tabs tabs-right sideways">
					            <li class="active"><a href="#tab1-vr" data-toggle="tab">شروط الخدمة</a></li>
					            <li><a href="#tab2-vr" data-toggle="tab">سياسة الخصوصية</a></li>
					            <li><a href="#tab3-vr" data-toggle="tab">كيفية يتم الدفع</a></li>
					          </ul>
					        </div>

				</section>
				
				

			</div>

		</div>

		<div class="grey-footer">
			<div class="container">
				<div class="grey-footer-inner">
					<a href="" class="s-link">س و ا ل ف</a>
					
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<h2>إبدأ الاصدار التجريبي مجانا ًلمدة ١١ يوم</h2>
							<h4>أسعار خاصة للمشاريع الجديدة، فقط </h4>
							<p class="price-text">
								<span>فقط       د.أ للشهر</span>
								<a href="">أعرف أ كثر .</a>
							</p>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
							 <input type="email" class="form-control" id="email" placeholder="بريدك الالكتروني">
						</div>
					</div>
				</div>
			</div>
		</div>
		 <?php $this->load->view('components/footer')?>
</body>
<?php $this->load->view('components/script')?>
<script src="<?php echo SCRIPTS?>jquery.mCustomScrollbar.concat.min.js"></script>
<script>
    (function($){
        $(window).on("load",function(){
            $(".meta-content").mCustomScrollbar();
        });
    })(jQuery);
</script>
</html>