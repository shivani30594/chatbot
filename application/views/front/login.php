<!DOCTYPE html>
<html lang="en">
<?php  $this->load->view('components/header') ?>
<body>
    <nav class="navbar ">
        <div class="container">
            <div class="navbar-flex">
                <div class="navbar-header login-navbar-header-logo navbar-right">
                    <a class="navbar-brand" href="#">
                    <img src="<?= IMG?>logo.png" class="img-responsive" alt="image">
                    </a>
                </div>
                <form action="<?= BASE_URL?>security/set_email" method="post">
                    <div class="intro login-nav-intro">
                        <h3 class="intro-title"> هل لديك حساب ؟ انشاء حسابك الان</h3>
                        <div class="input-group">
                            <input type="email" required name="email" id="email_id" class="form-control" placeholder="بريدك الالكتروني" aria-label="" aria-describedby="basic-addon1">
                            <label id="email_id_error" class="error" style="display:none">معرف البريد الإلكتروني موجود بالفعل</label>
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-secondary" type="submit">إبدأ</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </nav>
    <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true"></i></button>
            <p><strong>Error!</strong> <?php echo $this->session->flashdata('error') ?></p>
        </div>
    <?php endif;?>
    <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true"></i></button>
            <p><strong>Error!</strong> <?php echo $this->session->flashdata('success') ?></p>
        </div>
    <?php endif;?>
    <section class="login">	
		<div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-4">
                    <div class="login-form">
                        <h2 class="text-center">تسجيل الدخول إلى حسابك</h2>
                        <form action="<?= BASE_URL?>google_login" >
                            <div class="google-btn">
                                <button type="submit" class="btn btn-primary"><img src="<?php echo IMG?>google-logo.png"> <span> الدخول مع جوجل </span>
                                </button>
                            </div>
                        </form>
                        <p class="text-center">أو</p>
                       
                        <form action="<?= BASE_URL?>check_login" id="login_form" method="post">
                            <fieldset class="">	
                                <div class="form-group">
                                    <label class="label">بريد العمل</label>
                                    <input type="text" class="form-control" name="email_id" id="email_id" placeholder="بريد العمل">
                                </div>
                                <div class="form-group">
                                    <label class="label">كلمه السر</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="كلمه السر">
                                </div>
                                <div class="login-forgot-password pull-left">
                                    <a href="<?= BASE_URL?>forgot_password">نسيت كلمة المرور ؟ </a>
                                </div>
                                <div class="checkbox pull-right">
                                    <label><input type="checkbox" value=""> <span> ابقني مسجل </span></label>
                                </div>
                            </fieldset>
                            <div class="celarfix">
                                <button type="submit" class="btn btn-primary">تسجيل الدخول</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
    </section>
    <!-- <div class="chat-icon">
        <a href="">سِ</a>
    </div> -->
</body>
<?php $this->load->view('components/script')?>
<?php if($script):?>
    <?php $this->load->view($script);?>
<?php endif;?>
</html>