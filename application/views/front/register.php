<?php
header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('components/header');?>
<body>
    <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert"> <i class="fa fa-times" aria-hidden="true"></i> </button>
            <p><strong>Warning!</strong> <?php echo $this->session->flashdata('error') ?></p>
        </div>
    <?php endif;?>
    <div class="navbar-flex" id="navi">
        <div class="navbar-header navbar-right">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand navbar-brand-register-acc" href="#">
                <p> ابدأ مع سوالف </p>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="myNavbar-ul">
                <li><a href="<?php echo BASE_URL?>register" class="current">١- اختر باقتك</a></li>
                <li><a href="<?php echo BASE_URL?>register_2">٢- أنشأ حسابك</a></li>
                <li><a href="<?php echo BASE_URL?>register_3">٣- أبدأ النسخة التجرديبية</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <section class="step-outer">
            <div id="step-1" class= "step-wrap step-one">
                <div class="step-title">
                    <h2>أختر باقة. أكبر اسرع مع سوالف</h2>
                    <p>نسخة مجانية لمدة ١٤ يوم</p>
                </div>
                <div class="step-title">
                    <h2>طريقة أمثل لتحويل المهتمين الى عملاء</h2>
                </div>
            </div>
        </section>
    </div>
    <div class="container-fluid plan-block-bg">
        <div class="container">
            <div class="plan-block">
                <h2 class="plan-block-title">ساعد عملائك على محادثات مباشرة</h2>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="plan-wrap">
                            <div class="paln-intro-wrap">
                                <!-- <div class="paln-intro">
                                    <img src="http://18.217.216.88/Chatbot//assets/img/logo-symbol.png" class="img-responsive" alt="image">
                                </div>     -->
                                <div class="plan-name"> 
                                    <div class="name">الرئيسي</div>
                                </div>  
                                <div class="price">
                                    <h4>من ٩٩ د. أ. /شهر</h4>
                                </div> 
                                <h5 class="meta-infor">أدعم عملائك من خلال قنوات متعددة </h5> 
                                <a href="http://18.217.216.88/Chatbot/create/3" class="plan-wrap-a">جرب مجاناً</a>  
                            </div>
                            <div class="bottom-wrap">
                                <h4>كيف نسعر خدماتنا</h4>
                                <ul>
                                    <li class="title">&nbsp;</li>
                                    <li>إدارة المحادثات من البريد الإلكتروني والدردشة</li>
                                    <li>تعاون كفريق واحد لحل أسئلة الدعم</li>
                                    <li>ادمج سوالف مع</li>
                                </ul>  
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="plan-wrap pro-plan">
                            <h3 class="most-used"> 68 ٪ من الشركات تختار الموالية </h3>
                            <div class="paln-intro-wrap">
                                <!-- <div class="paln-intro ">
                                    <img src="http://18.217.216.88/Chatbot//assets/img/logo-symbol.png" class="img-responsive" alt="image">
                                </div>     -->
                                <div class="plan-name"> 
                                    <div class="name">محترف</div>
                                    
                                </div>   
                                <div class="price">
                                        <h4>من ١٧٩ د. أ. /شهر</h4>
                                    </div>
                                <h5 class="meta-infor">أحصل على مميزات ذكية لتبسيط سير العمل</h5>
                                <a href="http://18.217.216.88/Chatbot/create/2" class="plan-wrap-a"> جرب مجاناً </a>  
                            </div>
                            <div class="bottom-wrap">
                                <h4>كيف نسعر خدماتنا</h4>
                                <ul>
                                    <li class="title">كل مميزات الرئيسي مع</li>
                                    <li>خصص المحادثات و سير المحادثات بالذكاء الاصتناعي </li>
                                    <li>دمج المحادثات من القنوات الاجتماعية</li>
                                    <li>السيطرة على من يرى حالة المحدث ومتى</li>
                                    
                                </ul>  
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="plan-wrap">
                            <div class="paln-intro-wrap">
                                <!-- <div class="paln-intro">
                                    <img src="http://18.217.216.88/Chatbot//assets/img/logo-symbol.png" class="img-responsive" alt="image">
                                </div>     -->
                                <div class="plan-name"> 
                                    <div class="name">مؤسسة</div>
                                </div> 
                                <div class="price">
                                        <h4>تسعير حسب الطلب</h4>
                                </div>  
                                <h5 class="meta-infor">خصص على حاجة مؤسستك بخدمات خاصة للمؤسسات</h5> 
                                <a href="http://18.217.216.88/Chatbot/create/1" class="plan-wrap-a">تحدث معنا </a>  
                            </div>
                            <div class="bottom-wrap">
                                <h4>كيف نسعر خدماتنا</h4>
                                <ul>
                                    <li class="title">كل مميزات باقة المحترف مع</li>
                                    <li>حذف العلامة التجارية من محدث سوالف</li>
                                    <li>خصص المحادثات بأيقاف استقبال المرفقات</li>
                                    <li>إدارة الكميات العالية من المحادثات مع وضع حدود وتحويل متحدث  </li>
                                </ul>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hide-show-feature">
                    <a href="javascript:void(0)" class="hide-show-feature-a" id="close_button">  إخفاء المميزات الاضافية  <i id="down" class="fa fa-caret-down" aria-hidden="true" ></i>  <i style="display:none" id="cross" class="fa fa-times" aria-hidden="true"></i><span id="btn_status" style="display:none">on</span> </a>
                </div>
            </div>
            <div class="features-wrap">
                <div class="features">
                    <div class="f-title"><h2 class=""> إدارة المحادثة الواردة </h2></div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="feature-inner-wrap">
                            <div class="f-mobile-title"><h2 class=""> إدارة المحادثة الواردة </h2></div>
                            <ul>
                                <li> دردشة مباشرة </li>
                                <li> تطبيقات الموبايل </li>
                                <li> البريد </li>
                                <li class="disable" > حالة التواجد </li>
                                <li class="disable" > Twitter دمج  </li>
                                <li class="disable" > Facebook دمج  </li>
                                <li class="disable" > إزالة علامة سوالف التجارية </li>
                                <li class="disable" > تخصيص عالي على محدث سوالف </li>
                                <li class="disable" > قم بتعيين حدود للمحادثة  مع إدارة اكتفاء العمل </li>
                                <li class="disable" > تحويل المحادثات الى افراد الفريق </li>
                            </ul>
                        </div> 
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                        <div class="feature-inner-wrap">
                        <div class="f-mobile-title"><h2 class=""> إدارة المحادثة الواردة </h2></div>
                            <ul>
                                <li> دردشة مباشرة </li>
                                <li> تطبيقات الموبايل </li>
                                <li> البريد </li>
                                <li > حالة التواجد </li>
                                <li > Twitter دمج  </li>
                                <li > Facebook دمج  </li>
                                <li class="disable" > إزالة علامة سوالف التجارية </li>
                                <li class="disable" > تخصيص عالي على محدث سوالف </li>
                                <li class="disable" > قم بتعيين حدود للمحادثة  مع إدارة اكتفاء العمل </li>
                                <li class="disable" > تحويل المحادثات الى افراد الفريق </li>
                            </ul> 
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="feature-inner-wrap">
                        <div class="f-mobile-title"><h2 class=""> إدارة المحادثة الواردة </h2></div>
                            <ul>
                                <li> دردشة مباشرة </li>
                                <li> تطبيقات الموبايل </li>
                                <li> البريد </li>
                                <li > حالة التواجد </li>
                                <li > Twitter دمج  </li>
                                <li > Facebook دمج  </li>
                                <li > إزالة علامة سوالف التجارية </li>
                                <li > تخصيص عالي على محدث سوالف </li>
                                <li > قم بتعيين حدود للمحادثة  مع إدارة اكتفاء العمل </li>
                                <li > تحويل المحادثات الى افراد الفريق </li>
                            </ul> 
                        </div>
                    </div>
                </div>
                <div class="features features-2">
                        <div class="f-title"><h2 class=""> سير العمل  </h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  سير العمل </h2></div>
                                <ul>
                                    <li> مجمع البريد الالكتروني </li>
                                    <li> ملفات العملاء و العملاء المحتملين </li>
                                    <li> حالة تواجد فريق العمل وحالة الغياب  </li>
                                    <li class="disable" > جدول الاجتماعات الالي </li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  سير العمل </h2></div>
                                <ul>
                                    <li> مجمع البريد الالكتروني </li>
                                    <li> ملفات العملاء و العملاء المحتملين </li>
                                    <li> حالة تواجد فريق العمل وحالة الغياب  </li>
                                    <li > جدول الاجتماعات الالي </li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  سير العمل </h2></div>
                                <ul>
                                    <li> مجمع البريد الالكتروني </li>
                                    <li> ملفات العملاء و العملاء المحتملين </li>
                                    <li> حالة تواجد فريق العمل وحالة الغياب  </li>
                                    <li > جدول الاجتماعات الالي </li>
                                </ul> 
							</div>
                        </div>
                </div>
                <div class="features features-3">
                    <div class="f-title"><h2 class=""> تعاون كفريق </h2></div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="feature-inner-wrap">
                            <div class="f-mobile-title"><h2 class=""> تعاون كفريق  </h2></div>
                            <ul>
                                <li> البريد الوارد للفريق  </li>
                                <li> أوقات العمل  </li>
                                <li> الردود المحفوظة  </li>
                                <li class="disable" > قوانين للمهام </li>
                                <li class="disable" > ادن للمحادثات </li>
                                <li class="disable" > سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                        <div class="feature-inner-wrap">
                            <div class="f-mobile-title"><h2 class=""> تعاون كفريق  </h2></div>
                            <ul>
                                <li> البريد الوارد للفريق  </li>
                                <li> أوقات العمل  </li>
                                <li> الردود المحفوظة  </li>
                                <li> قوانين للمهام </li>
                                <li> ادن للمحادثات </li>
                                <li class="disable" > سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="feature-inner-wrap">
                            <div class="f-mobile-title"><h2 class=""> تعاون كفريق  </h2></div>
                            <ul>
                                <li> البريد الوارد للفريق  </li>
                                <li> أوقات العمل  </li>
                                <li> الردود المحفوظة  </li>
                                <li> قوانين للمهام </li>
                                <li> ادن للمحادثات </li>
                                <li> سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                    </div>
                </div>
                <div class="features features-4">
                        <div class="f-title"><h2 class=""> تقارير الاداء </h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  تقارير الاداء </h2></div>
                                <ul>
                                    <li>ملخص الاداء</li>
                                    <li>ملخص العملاء المحتملون</li>
                                    <li>ملخص اداء الفريق</li>
                                    <li>تصدير</li>
                                    <li>تصدير المحادثات كاملة</li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  تقارير الاداء </h2></div>
                                <ul>
                                    <li>ملخص الاداء</li>
                                    <li>ملخص العملاء المحتملون</li>
                                    <li>ملخص اداء الفريق</li>
                                    <li>تصدير</li>
                                    <li>تصدير المحادثات كاملة</li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  تقارير الاداء </h2></div>
                                <ul>
                                    <li>ملخص الاداء</li>
                                    <li>ملخص العملاء المحتملون</li>
                                    <li>ملخص اداء الفريق</li>
                                    <li>تصدير</li>
                                    <li>تصدير المحادثات كاملة</li>
                                </ul> 
							</div>
                        </div>
                </div>
                <div class="features features-5">
                        <div class="f-title"><h2 class=""> الدعم والخدمات </h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  الدعم والخدمات  </h2></div>
                                <ul>
                                    <li> مركز معرفة </li>
                                    <li> دعم البريد الالكتروني </li>
                                    <li class="disable"> دعم دردشة مباشر </li>
                                    <li class="disable" > دعم على مدار الساعة طول  ابام الاسبوع </li>
                                    <li class="disable" > الحصول على اجدد المميزات قبل  توافرهن للعامة </li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  الدعم والخدمات  </h2></div>
                                <ul>
                                    <li> مركز معرفة </li>
                                    <li> دعم البريد الالكتروني </li>
                                    <li> دعم دردشة مباشر </li>
                                    <li class="disable" > دعم على مدار الساعة طول  ابام الاسبوع </li>
                                    <li class="disable" > الحصول على اجدد المميزات قبل  توافرهن للعامة </li>
                                </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                                <div class="f-mobile-title"><h2 class="">  الدعم والخدمات  </h2></div>
                                <ul>
                                    <li> مركز معرفة </li>
                                    <li> دعم البريد الالكتروني </li>
                                    <li> دعم دردشة مباشر </li>
                                    <li> دعم على مدار الساعة طول  ابام الاسبوع </li>
                                    <li> الحصول على اجدد المميزات قبل  توافرهن للعامة </li>
                                </ul> 
							</div>
                        </div>
                </div>
                <!-- <div class="features features-6">
                        <div class="f-title"><h2 class="">التطبيقات والتكاملات</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="disable" > Zendesk </li>
                                <li class="disable" > جيثب </li>
                                <li class="disable" > قوة المبيعات الأساسية </li>
                                <li class="disable" > Marketo </li>
                                <li class="disable" > Clearbit </li>
                            </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="" > Zendesk </li>
                                <li class="" > جيثب </li>
                                <li class="" > قوة المبيعات الأساسية </li>
                                <li class="disable" > Marketo </li>
                                <li class="disable" > Clearbit </li>
                            </ul> 
							</div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="" > Zendesk </li>
                                <li class="" > جيثب </li>
                                <li class="" > قوة المبيعات الأساسية </li>
                                <li class="" > Marketo </li>
                                <li class="" > Clearbit </li>
                            </ul> 
							</div>
                        </div>
                </div>
                <div class="features features-7">
                        <div class="f-title"><h2 class="">الدعم والخدمات</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="disable" > دعم الدردشة</li>
                                <li class="disable" > فريق نجاح شخصي </li>
                                <li class="disable" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 
							</div>
                            <a href="<?php echo BASE_URL.'create/3'?>" class="plan-wrap-a">دردش معنا</a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border pro-plan">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="" > دعم الدردشة</li>
                                <li class="disable" > فريق نجاح شخصي </li>
                                <li class="disable" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 
							</div>

                            <a href="<?php echo BASE_URL.'create/2'?>" class="plan-wrap-a">  دردش معنا <i class="fa fa-arrow-left" aria-hidden="true"></i> </a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feature-inner-wrap">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="" > دعم الدردشة</li>
                                <li class="" > فريق نجاح شخصي </li>
                                <li class="" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 
							</div>

                            <a href="<?php echo BASE_URL.'create/1'?>" class="plan-wrap-a">دردش معنا <i class="fa fa-arrow-left" aria-hidden="true"></i> </a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
		<!-- <div class="grey-footer">
			<div class="container">
				<div class="grey-footer-inner">
					<a href="" class="s-link">س و ا ل ف</a>
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<h2>إبدأ الاصدار التجريبي مجانا ًلمدة ١١ يوم</h2>
							<h4>أسعار خاصة للمشاريع الجديدة، فقط </h4>
							<p class="price-text">
								<span>فقط       د.أ للشهر</span>
								<a href="">أعرف أ كثر .</a>
							</p>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
							 <input type="email" class="form-control" id="email" placeholder="بريدك الالكتروني">
						</div>
					</div>
				</div>
			</div>
        </div> -->
    </div>
    <?php $this->load->view('components/footer');?>
</body>
<?php $this->load->view('components/script')?>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click',"#close_button", function()
        {
            if ($("#btn_status").text() == 'off')
            {
                $("#cross").css("display","none"); 
                $("#down").css("display","inline-block");
            } else
            {
                $("#cross").css("display","inline-block"); 
                $("#down").css("display","none");  
            }
            $("#btn_status").text() == "off" ? $("#btn_status").text("on") : $("#btn_status").text("off");
            $(".features-wrap").slideToggle("1000");
        });
});
// habe gebraucht - werde brauchen
// 
</script> 
</html>