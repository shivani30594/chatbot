<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('components/teammate_header')?>
<body>
    <?php if($this->session->flashdata('success')):?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert"> <i class="fa fa-times" aria-hidden="true"></i> </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success') ?>
        </div>
    <?php elseif($this->session->flashdata('error')):?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"> <i class="fa fa-times" aria-hidden="true"></i> </button>
            <strong>Error!</strong> <?php echo $this->session->flashdata('error') ?>
        </div>
    <?php endif;?>
    <section>
        <header class="header fixed-top clearfix">
            <div class="brand">
                <a href="#" class="logo">
                    سوالف
                </a>
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
                </div>
            </div>
        </header>
        <!-- Side bar -->
        <?php $this->load->view('components/teammate_sidebar')?>
        <!-- Display main content -->
        <?php $this->load->view($subview)?>
    </section>
</body>
<!-- Load javascaript -->
<?php $this->load->view('components/teammate_footer')?>
<!--  Javascript for the single page-->
</html>