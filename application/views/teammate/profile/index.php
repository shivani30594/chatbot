<section id="main-content" class="pt80">
    <section class="wrapper">
        <div class="">
            <div class="user-personal-profile">
                <div class="admin-header">
                    <h1 class="user-h1">إعدادات الملف الشخصي<i class="fa fa-user" aria-hidden="true"></i> </h1>
                </div>
                <div class="inbox-msg">
                    <div class="setting-left-side">
                        <!-- content -->
                        <div class="peronal-profile-left">
                        <form action="<?= BASE_URL?>t_set_teammate_info" class="email-form" method="POST">
                            <div class="form-group">
                                <label for="email">بريدك الالكتروني</label>
                                <input type="email" class="form-control" id="chatbot_email" name="chatbot_email" required value="<?= (isset($teammate_information) AND !empty($teammate_information[0]['chatbot_email'])) ? $teammate_information[0]['chatbot_email'] : ''?>">
                            </div>
                            <div class="form-group">
                                <label for="name">الاسم </label>
                                <input type="text" class="form-control" id="name" name="name" required value="<?= (isset($teammate_information) && !empty($teammate_information) && $teammate_information[0]['name'] != '') ? $teammate_information[0]['name'] :''?>">
                            </div>
                            <button class="btn" type="submit">حفظ</button>
                        </form>                    
                         <form action="<?= BASE_URL?>t_set_chatbot_image" class="profile-picture-form" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="email"> الصورة الشخصية </label>
                                <div class="images-pro">
                                      <div class="go-top">
                                        <input class="my-set_3" type="file" name="chatbot_profile" id="chatbot_profile" required>
                                        <a href="#"></a> </div>
                                      <img src="<?= (isset($teammate_information) && !empty($teammate_information) && $teammate_information[0]['chatbot_profile'] != '') ? PROFILE_IMG.$teammate_information[0]['chatbot_profile'] :'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg'?>" alt="User profile picture" id="imgprvw" style="height: 70px; width: 70px"> 
                                  </div>
                            </div>
                            <button class="btn upld-btn" type="submit">تحميل زر</button>
                        </form>
                        </div>
                    </div>
                    <div class="inbox-people">       
                        <div class="user-btn-mobile">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <div class="main-chat-list">
                            <div class="setting-right-side">
                                <a href="#" class="<?= $this->uri->segment(1) == 't_profile' ? "profile-menu-active" : '' ?>"> <h3> حسابك </h3> </a>
                                <a href="#"> <h3> اشعارات </h3> </a>    
                            </div>                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>