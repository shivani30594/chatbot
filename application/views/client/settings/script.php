<script>
$(document).ready(function()
{
    // click on accordiabn menu, close the other accordian menu and open the cuirrent one
    $(".acc-inner").click(function (element) {
        // body...
        var id = $(this).parent('div').parent('li').attr("id");
        var class_name = "accordiabn-meta-"+id;
        if($("."+class_name).css('display') == 'none')
        {
            $('.accordiabn-meta').each(function() {
            if(!$(this).hasClass('"."+class_name')){
                if($(this).css('display')=='block') {
                    $(this).slideToggle(200);
            }
            }
            });
        }
        $("."+class_name).slideToggle(200);
        $("."+class_name).parent('li').find('.arrow-dwn').toggleClass("main");

    })

    // change the color and set into the database so that we can get it for the next time
    $(document).on('change',"#widget_color", function()
    {
        $.ajax({
			url: '<?php echo BASE_URL?>client/settings/set_color',           
			type: 'POST',          
			data: { widget_color : window.btoa( $("#widget_color").val() )},          
			success: function(response)
			{       
				 if (response.status == 'yes')
                 {
                     $("#chat_logo").css("background-color",$("#widget_color").val());
                     $(".send-btn").css("color",$("#widget_color").val());
                 }         
	  		}                     
		});
    });

    // change the image and set into the widget_logo. Also set into the database for the net time
    $(document).on('change',"#widget_image", function()
    {
        var formData = new FormData();
        formData.append('file', document.getElementById("widget_image").files[0]);
        formData.append('widget_logo', window.btoa($("#widget_logo").val()));
        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL?>client/settings/set_image",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.status == "no")
                {
                    $("#upload_error").css("display","inline-block");
                }
                else
                {
                    readURL($("#widget_image")[0]);
                    $("#upload_error").css("display","none");
                }
            },
            error: function(errResponse) {
                console.log(errResponse);
            }
        })
    });

    // read the image and display using javascript
    function readURL(input) 
    {
        if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#logo_image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }

    // click on the cancle button under the accordian menu will close the menu
    $(document).on('click',"#cancel", function()
    {
        var i = $(this).data("value");
        var class_name = "accordiabn-meta-ac-"+i;
        $("."+class_name).slideToggle(200);
        $("."+class_name).parent('li').find('.arrow-dwn').toggleClass("main");
    });
       
    // click on active button make it in-active 
    $(document).on('click',".active-btn", function()
    {
        $(".active-btn").hide();
        $(".in-active-btn").show();
        $("#in-active-badge").css("display","none");
        $("#active-badge").css("display","block");
        change_client_status("online");
    })

    // in-active button click make it active button
    $(document).on('click',".in-active-btn", function()
    {
        $(".in-active-btn").hide();
        $(".active-btn").show();
        $("#active-badge").css("display","none");
        $("#in-active-badge").css("display","block");
        change_client_status("offline");
    })

    // click on active anf in=active button set the status into database also
    function change_client_status(status)
    {
        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL?>change_client_status",
            data : { status : window.btoa(status)},
            success: function(response) {
                console.log(response)
            },
            error: function(errResponse) {
                console.log(errResponse);
            }
        })
    }

    // set the automated question setting from the settings page. based on the requirement 
    // to display the question at visitor end, client have to set it from here. defauklt is asking thr email
    $(document).on('change','input[name=contact_option]', function()
    {
        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL?>change_contact_option",
            data : { contact_option : window.btoa( $(this).val() )},
            success: function(response) {
                // response 
                // console.log("response", response);
            },
            error: function(errResponse) {
                // alert(errResponse)
                // console.log(errResponse)
            }
        })
    });

    // on change of name question field called below function is called
    $(document).on('blur',"#name", function(){
        set_question_text('name_ques',$(this).val());
    });

    // on change of email question field called below function is called
    $(document).on('blur',"#email", function(){
        set_question_text('email_ques',$(this).val());
    });

    // on change of phone number question field called below function is called
    $(document).on('blur',"#phone_number", function(){
        set_question_text('number_ques',$(this).val());
    });

    // Set question text in database using the ajax call.
    function set_question_text(q_name, q_value)
    {
        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL?>set_question_text",
            data : { q_name : q_name, q_value : q_value},
            success: function(response) {
                // response 
                // console.log(response,"response");
            },
            error: function(errResponse) {
                // alert(errResponse)
                // console.log("errResponse", errResponse);
            }
        })
    }
    
});



</script>