<section id="main-content" class="pt80">
	<section class="wrapper">
        <div class="">
            <div class="messenger-wrap">
                <div class="admin-header">
                    <h1 class="user-h1"> رسول </h1>
                    <div class="meta">
                        <p>يمكنك الدردشة ومساعدة عملائك في إنجاز المهام بسرعة وبتطبيقات سهلة الاستخدام</p>
                        <p>ومنزل رسول شخصية.</p>
                    </div>
                </div>
                <div class="messenger-inner">
                    <h1>سوالفنا ماسنجر</h1>
                    <div class="row">
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="msngr-features">
                                <ul>
                                    <li id="ac-1" class="accordian-li">
                                        <i class="arrow-dwn"></i>
                                        <div class="">
                                            <a href="javascript:void(0)" class="acc-inner">
                                            <img src="<?= IMG?>accordian-1.png" alt="icon">
                                                <div class="text">
                                                    <h2>قل مرحبًا بلغة (تقريبًا) أي لغة </h2>
                                                    <h5>قدم فريقك بما يصل إلى 38 لغة</h5>
                                                </div>  
                                            </a>
                                        </div>
                                        <div class="accordiabn-meta accordiabn-meta-ac-1" >
                                            <div class="acc-inner-wrap">
                                                <h2>تحية</h2>
                                                <p>قل مرحبًا لعملائك عند فتح رسول.</p>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="pwd">الإنجليزية</label>
                                                <div class="">          
                                                    <input type="" class="form-control" id="" placeholder="مرحبا" name="">
                                                </div>
                                            </div>
                                            <div class="acc-inner-wrap">
                                                <h2>مقدمة الفريق</h2>
                                                <p>قدم فريقك واذكر كيف يمكنك مساعدة عميلك.</p>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="pwd">الإنجليزية</label>
                                                <div class="">          
                                                    <textarea class="form-control" rows="4" cols="50" placeholder="اسألنا أي شيء ، أو شارك ملاحظاتك."></textarea>
                                                    <p class="word-cnt"><span>120</span> الشخصيات المتبقية</p>
                                                </div>
                                            </div>
                                            <div class="btn-grp">
                                                <a class="btn btn-default btn-lg save" href="#">حفظ وتعيين العيش</a>
                                                <a class="btn btn-default btn-lg cancle" data-value="2" id="cancel">إلغاء</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="ac-2" class="accordian-li">
                                        <i class="arrow-dwn"></i>
                                        <div class="">
                                            <a href="javascript:void(0)" class="acc-inner">
                                                <img src="<?= IMG?>accordian-2.png" alt="icon">
                                                <div class="text">
                                                    <h2>حدد مدى تواجدك</h2>
                                                    <h5>تعيين ساعات العمل في الفريق وأوقات الرد</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="accordiabn-meta accordiabn-meta-ac-2">
                                            <div class="set-office-hrs">  
                                              <div class="acc-inner-wrap">
                                                  <h2>تعيين ساعات العمل</h2>
                                                  <p>دع العملاء يعرفون متى يعمل فريقك. خارج هذه الساعات ،</p>
                                                  <p>سيرون متى ستعود ، بالنسبة إلى منطقتهم الزمنية.</p>
                                              </div>
                                              <div class="form-group">
                                                <label class="control-label" for="">المنطقة الزمنية لتطبيقك هي لوس أنجلوس</label>
                                                     <div class="fields">  
                                                        <div class="">          
                                                            <select name="days">
                                                                <option value="weekdays">weekdays</option>
                                                                <option value="weekends">weekends</option>
                                                            </select>
                                                        </div>
                                                        <div class="">          
                                                            <select name="in-time">
                                                                <option value="time1">9:00 am</option>
                                                                <option value="time2">10:00 am</option>
                                                            </select>
                                                        </div>
                                                        <div class="">to</div>
                                                        <div class="">          
                                                            <select name="out-time">
                                                                <option value="time1">5:00 am</option>
                                                                <option value="time2">6:00 am</option>
                                                            </select>
                                                        </div>
                                                        <div class="">
                                                            <a href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                    <a href="" class="add-hrs"><i class="fa fa-plus" aria-hidden="true"></i> أضف ساعات العمل <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                              </div>
                                            </div>
                                            <hr>
                                            <div class="set-office-time">  
                                                <div class="acc-inner-wrap">
                                                    <h2>تعيين وقت الرد</h2>
                                                    <p>خلال ساعات العمل ، اسمح لعملائك بمعرفة الوقت الذي يمكنهم فيه توقع الرد.</p>
                                                </div>
                                                <div class="form-group custome-rdo">
                                                    <label class="container">وقت الرد التلقائي. حاليًا "سوف أقوم بالرد بأسرع ما يمكن"
                                                        <input type="radio" checked="checked" name="radio">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container">"عادة ما يرد الفريق في غضون دقائق قليلة."
                                                        <input type="radio" name="radio">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container">"عادة ما يرد الفريق في غضون بضع ساعات."
                                                        <input type="radio" name="radio">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container">"عادة ما يرد الفريق في يوم واحد."
                                                        <input type="radio" name="radio">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <p class="note">
                                                ملاحظة: سيقوم المشغل التلقائي بالرد مع توفر فريقك خلال ساعات العمل خارج المكتب أو أوقات الرد الطويلة.
                                                </p>
                                                <div class="btn-grp">
                                                    <a class="btn btn-default btn-lg cancle" data-value="2" id="cancel"> أغلق</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="ac-7" class="accordian-li">
                                        <i class="arrow-dwn"></i>
                                        <div class="">
                                            <a href="javascript:void(0)" class="acc-inner">
                                                <img src="<?= IMG?>automated-msg.png" alt="icon">
                                                <div class="text">
                                                <div class="d-flex">
                                                    <h2> اطلب جهة اتصال العميل</h2> 
                                                    <div class="badge-wrap">
                                                        <span class="active badge" id='active-badge' style="display:none">مفعل</span>
                                                        <span class="in-active badge" id='in-active-badge'>غير مفعل</span>
                                                    </div>
                                                </div>
                                                    <h5>اذا لم يكن لدينا تفاصيل الاتصال الخاصة بالعملاء، فسيقترح المشغل على العملاء ترك عنوان بريدهم الالكتروني
                                                    </h5>
                                                </div>
                                            </a>                                        
                                        </div>
                                        <div class="accordiabn-meta accordiabn-meta-ac-7">
                                            <div class="set-office-time">  
                                                <div class="acc-inner-wrap">
                                                    <p>او رقم هاتفهم وستحصل على اشعار حين الحصول على رد</p>
                                                </div>
                                                <div class="form-group custome-rdo">
                                                    <label class="container">اطلب البريد الالكتروني
                                                        <input type="radio" <?= (isset($style_data) AND $style_data[0]['contact_option'] == 0) ? 'checked' : '' ?> name="contact_option" value="0">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container">اطلب البريد الالكتروني و الاسم
                                                        <input type="radio" <?= (isset($style_data) AND $style_data[0]['contact_option'] == 1) ? 'checked' : '' ?> name="contact_option" value="1">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container"> اطلب الاسم و رقم الهاتف
                                                        <input type="radio" <?= (isset($style_data) AND $style_data[0]['contact_option'] == 2) ? 'checked' : '' ?> name="contact_option" value="2">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>                                                                                                       
                                                <div class="btn-grp active-in-active-btn-grp">
                                                        <button class="btn btn-default btn-lg save active-btn">مفعل</button>
                                                        <button class="btn btn-default btn-lg cancle in-active-btn" style="display:none">غير مفعل</button>
                                                </div>
                                                <div class="btn-grp">
                                                    <a class="btn btn-default btn-lg cancle" id="cancel" data-value="7">أغلق</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="ac-8" class="accordian-li">
                                        <i class="arrow-dwn"></i>
                                        <div class="">
                                            <a href="javascript:void(0)" class="acc-inner">
                                                <img src="<?= IMG?>form-edit.png" alt="icon">
                                                <div class="text">
                                                    <div class="d-flex">
                                                        <h2>اسلوب رسائل تلقائية خاص بك</h2>
                                                    </div>
                                                   <h5>ارسل رسائل تلقائية مفصلة على محدث سوالف الخاص بك للحص،ل على جهة</h5>
                                                </div>
                                            </a>                                        
                                        </div>
                                        <div class="accordiabn-meta accordiabn-meta-ac-8">
                                            <div class="set-office-time set-intro-msg-field">  
                                                <!-- <div class="acc-inner-wrap">
                                                    <p>او رقم هاتفهم وستحصل على اشعار حين الحصول على رد</p>
                                                </div> -->
                                                <div class="form-group">
                                                   <label for="">١. سؤال للحصول على اسم الزائر</label>
                                                   <input type="text"  class="form-control" name="name" id="name" value="<?php echo (!empty($style_data) AND $style_data[0]['name_ques'] != NULL ) ? $style_data[0]['name_ques']   : '' ?>" required>
                                                </div> 
                                                <div class="form-group">
                                                   <label for="">٢. سؤال للحصول على رقم الزائر</label>
                                                   <input type="text"  class="form-control" required name="phone_number" id="phone_number" value="<?php echo (!empty($style_data) AND $style_data[0]['number_ques'] != NULL ) ? $style_data[0]['number_ques']   : '' ?>" >
                                                </div> 
                                                <div class="form-group">
                                                   <label for="">٣. سؤال للحصول على بريد الزائر</label>
                                                   <input type="text"  class="form-control" required name="email" id="email"  value="<?php echo (!empty($style_data) AND $style_data[0]['email_ques'] != NULL ) ? $style_data[0]['email_ques']   : '' ?>">
                                                </div>                                                                                                       
                                                <div class="btn-grp">
                                                    <a class="btn btn-default btn-lg cancle" id="cancel" data-value="8">أغلق</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="ac-3" class="accordian-li">
                                        <i class="arrow-dwn"></i>                                 
                                        <div class="">
                                            <a href="javascript:void(0)" class="acc-inner">
                                            <img src="<?= IMG?>accordian-3.png" alt="icon">
                                                <div class="text">
                                                    <h2>رسول إضافة التطبيقات المنزلية </h2>
                                                    <h5>ساعد عملاءك قبل بدء محادثة</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="accordiabn-meta accordiabn-meta-ac-3">
                                            <p>
                                            هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط. هذا هو مجرد نص زائف
                                            هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط. هذا هو مجرد نص زائف
                                            هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط.
                                            </p>
                                        </div>
                                    </li>
                                    <?php if(isset($plan_id) && $plan_id != '1'):?>
                                    <li id="ac-4" class="accordian-li">
                                        <i class="arrow-dwn"></i>
                                        <div class="">
                                            <a href="javascript:void(0)" class="acc-inner">
                                                <img src="<?= IMG?>accordian-4.png" alt="icon">
                                                <div class="text">
                                                    <h2>اسلوب رسول الخاص بك </h2>
                                                    <h5>أضف شعارك ، واختر الألوان والخلفيات ، وقم بإيقاف تشغيل الأصوات أو تشغيلها</h5>
                                                </div>
                                            </a> <!-- -->
                                        </div>
                                        <div class="accordiabn-meta accordiabn-meta-ac-4">
                                            <form action="<?= BASE_URL?>style_messenger" method="POST" id="style_form" accept-charset="utf-8" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <div class="logo-uploadarea">
                                                    <p> شعار  <i class="fa fa-question-circle" aria-hidden="true"></i> <i class="fa fa-plus-circle" aria-hidden="true"></i></p>
                                                    <input type="file" class="hide inputfile" id="widget_image" data-multiple-caption="{count} files selected" name="widget_image">
                                                    <label for="widget_image"><span> تحميل شعار <i class="fa fa-cloud-upload" aria-hidden="true"></i>  </span></label>
                                                    <small style="display:none;color:red" id="upload_error">File Size is too large. Please upload the image of less than 10MB.</small>    
                                                </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="acc-inner-wrap">
                                                        <h2>لون العمل</h2>
                                                        <p>تستخدم في الأزرار والروابط والمزيد لتسليط الضوء والتأكيد.</p>
                                                        <input type="color" name="widget_color"  id="widget_color" value="<?= (isset($style_data) AND !empty($style_data[0]['widget_color'])) ? $style_data[0]['widget_color'] :'#20ce88'?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="acc-inner-wrap">
                                                        <h2>تحية</h2>
                                                        <p>قل مرحبًا لعملائك عند فتح رسول.</p>
                                                        <input type="color" name="favcolor" id="favcolor" value="#20ce88">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="widget_logo" id="widget_logo" value="<?= (isset($style_data) AND !empty($style_data[0]['widget_image'])) ? $style_data[0]['widget_image'] : ''?>">
                                            </form>
                                            <div class="btn-grp">
                                                <a class="btn btn-default btn-lg cancle" id="cancel" data-value="4">أغلق</a>
                                            </div>
                                        </div>
                                    </li>
                                    <?php endif;?>
                                    <li id="ac-5" class="accordian-li">
                                        <i class="arrow-dwn"></i>
                                        <div class="">
                                            <a href="javascript:void(0)" class="acc-inner">
                                                <img src="<?= IMG?>accordian-5.png" alt="icon">
                                                <div class="text">
                                                    <h2>تحكم في المحادثات الواردة والقاذفة </h2>
                                                    <h5>يمكنك التحكم في الأشخاص الذين يمكنهم إرسال رسائل إليك وأين يرون المشغِّل</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="accordiabn-meta accordiabn-meta-ac-5">
                                            <p>
                                                هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط. هذا هو مجرد نص زائف
                                                هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط. هذا هو مجرد نص زائف
                                                هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط.
                                            </p>
                                        </div>
                                    </li>
                                    <li id="ac-6" class="accordian-li">
                                        <i class="arrow-dwn"></i>
                                        <div class="">
                                            <a href="javascript:void(0)" class="acc-inner">
                                            <img src="<?= IMG?>accordian-6.png" alt="icon">
                                                <div class="text">
                                                    <h2>تأمين رسول ابق</h2>
                                                    <h5>الأمان وإعدادات المجال والتحقق من الهوية</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="accordiabn-meta  accordiabn-meta-ac-6">
                                            <p>
                                            هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط. هذا هو مجرد نص زائف
                                            هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط. هذا هو مجرد نص زائف
                                            هذا مجرد نص وهمي. هنا هو النص وهمية للرجوع اليها فقط.
                                            </p>
                                        </div>
                                    </li>
                                </ul> 
                                <a href="" class="">رسول فعل المزيد مع</a>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="admin-chat-widget">
                                <section class="chat-widget-wrap">
                                    <div class="chat-widget">
                                        <div class="msg-area">
                                            <div class="receiver">
                                                <div class="msg-bubble"> 
                                                    <p class="main-msg title">آدم من الدعم عبر الإنترنت</p>
                                                    <p class="main-msg">مرحبا هناك ، أنا دعم عبر الإنترنت هنا
                                                                كيف يمكنني مساعدتك ؟
                                                                هذا هو النص الوهمي هنا
                                                                مرحبا أنا نص وهمي ، مرحبا أنا نص وهمي ،
                                                    </p>
                                                    <p class="time">منذ 2 دقائق</p> 
                                                </div>
                                                <div class="user-avtar"><img src="<?= IMG;?>chat-user-avtar.png" class="img-responsive" alt="img"></div>
                                            </div>
                                            <div class="sender">
                                                <div class="user-avtar"><img src="<?= IMG;?>chat-user-avtar.png" class="img-responsive" alt="img"></div>
                                                <div class="msg-bubble">
                                                    <p class="main-msg">مرحبا أنا زائر هنا
                                                    </p>
                                                    <p class="time">منذ 2 دقائق</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="type-area">
                                            <a href="" class="send-btn" style="color: <?= (isset($style_data) && !empty($style_data))  ? $style_data[0]['widget_color'] : '#20ce88'?>" ><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
                                            <textarea class="form-control" rows="1" id="comment"></textarea>
                                            <div class="cntrl-grp">
                                                <a href="" class="emoji"><i class="fa fa-smile-o" aria-hidden="true"></i></a>
                                                <a href="" class="f-input"><i class="fa fa-paperclip" aria-hidden="true"></i></a>
                                                <a href="" class="gif">GIF</a>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <div class="chat-icon">
                                    <a href="#" id="chat_logo" class="login" title="Login" style="background-color: <?= (isset($style_data) && !empty($style_data))  ? $style_data[0]['widget_color'] : '#20ce88'?>" >
                                        <?php if (isset($style_data) && !empty($style_data[0]['widget_image'])):?>
                                            <img src="<?= BASE_URL.'uploads/logo/'.$style_data[0]['widget_image'];?>" id="logo_image" height="40px" width="40px" />
                                        <?php else:?>
                                            <img src="<?= IMG.'chat-logo.png';?>" id="logo_image" height="40px" width="40px" />
                                        <?php endif;?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
</section>