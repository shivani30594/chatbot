<section id="main-content" class="pt80">
    <section class="wrapper">
        <div class="">
            <div class="messaging">
                <div class="admin-header">
                    <h1 class="user-h1">  الاعدادات <i class="fa fa-cog" aria-hidden="true"></i> </h1>
                </div>
                <div class="inbox-msg">
                    <div class="setting-left-side">
                        <form action="/action_page.php">
                            <div class="form-group">
                                <label for="email">وحدة زمنية</label>
                                <select class="fixture_timezone"></select>
                            </div>
                            <button class="btn keep-up-btn" type="submit">حفظ</button>
                        </form>
                    </div>
                    <div class="inbox-people">
                        <div class="user-btn-mobile">               
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <div class="main-chat-list">    
                            <div class="setting-right-side">                                
                                <div class="dropdown">   
                                    <button class="btn <?=$this->uri->segment(1) == 'general_settings' ? 'profile-menu-active' : ''?>" type="button" id="drop-btn" >مساحة العمل ، الافرقة   
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>   
                                        <span id="btn_status" style="display:none">on</span>   
                                    </button>    
                                    <ul class="menu">  
                                        <li><a href="<?php echo BASE_URL?>general_settings" class="<?=$this->uri->segment(1) == 'general_settings' ? 'active' : ''?>"> الاعادادات العامة </a></li>  
                                        <li><a href="<?php echo BASE_URL?>teammates" class="<?=$this->uri->segment(1) == 'teammates' ? 'active' : ''?>""> اعضاء الفريق </a></li>         
                                    </ul>                                                                                                                                                                                                                                                                                                                                                                                                                                  
                                </div>              
                                <a href="<?php echo BASE_URL?>install" class="<?=$this->uri->segment(1) == 'install' ? 'profile-menu-active' : ''?>"> <h3>التثبيت</h3> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>