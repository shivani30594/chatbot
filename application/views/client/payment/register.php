<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('components/header');?>
<body>
    <?php if($this->session->flashdata('error')):?>
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert"> <i class="fa fa-times" aria-hidden="true"></i> </button>
            <p><strong>Warning!</strong> <?php echo $this->session->flashdata('error') ?></p>
        </div>
    <?php endif;?>
    <div class="navbar-flex" id="navi">
        <div class="navbar-header navbar-right">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand navbar-brand-register-acc" href="#">
                <p> ابدأ مع سوالف </p>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="myNavbar-ul">
                <li><a href="<?php echo BASE_URL?>register" class="current">١- اختر باقتك</a></li>
                <li><a href="<?php echo BASE_URL?>register_2">٢- أنشأ حسابك</a></li>
                <li><a href="<?php echo BASE_URL?>register_3">٣- أبدأ النسخة التجرديبية</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <section class="step-outer">
            <div id="step-1" class= "step-wrap step-one">
                <div class="step-title">
                    <h2>أختر باقة. أكبر اسرع مع سوالف</h2>
                    <p>نسخة مجانية لمدة ١٤ يوم</p>
                </div>
                <div class="step-title">
                    <h2>طريقة أمثل لتحويل المهتمين الى عملاء</h2>
                </div>
                <div class="plan-block">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="plan-wrap">
                                <div class="paln-intro-wrap">
                                    <div class="paln-intro">
                                        <img src="<?php echo IMG?>logo-symbol.png" class="img-responsive" alt="image">
                                    </div>    
                                    <div class="plan-name"> 
                                        <div class="name">علاوة</div>
                                        <div class="price">
                                            <!-- <span>من عند</span>
                                            <p>$ 202</p>
                                            <span>/ شهر</span> -->
                                        </div>
                                    </div>   
                                    <h5 class="meta-infor">مجموعة الأدوات الكاملة لنمو الأعمال</h5> 
                                    <a href="<?php echo BASE_URL.'create/3'?>" class="plan-wrap-a">دردش معنا</a>  
                                </div>
                                <div class="bottom-wrap">
                                    <ul>
                                        <li class="title">كل من طليعة زائد:</li>
                                        <li>جذب المشاركة والعمل باستخدام السير المخصص</li>
                                        <li>الحصول على تقارير حول أداء الفريق والعائدات</li>
                                        <li>جذب المشاركة والعمل باستخدام السير المخصص</li>
                                        <li>قريبًا: يمكنك تحويل العملاء المستهدفين إلى حملات تسويقية مستندة إلى الحساب</li>
                                        <li>قريبًا: يمكنك إدارة قدرة الفريق على نحو استراتيجي ومراقبة نطاقات SLA</li>
                                    </ul>  
                                </div>
                            </div>
                            <div class="hide-show-feature">
                                <a href="javascript:void(0)" class="hide-show-feature-a"> إخفاء ميزات إضافية </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="plan-wrap pro-plan">
                                <h3 class="most-used">الحزمة الأكثر استخداما</h3>
                                <div class="paln-intro-wrap">
                                    <div class="paln-intro ">
                                        <img src="<?php echo IMG?>logo-symbol.png" class="img-responsive" alt="image">
                                    </div>    
                                    <div class="plan-name"> 
                                        <div class="name">طليعة</div>
                                        <div class="price">
                                            <span>من عند</span>
                                            <p>$ 202</p>
                                            <span>/ شهر</span>
                                        </div>
                                    </div>   
                                    <h5 class="meta-infor">أتمتة وتحسين سير العمل الخاص بك</h5>
                                    <a href="<?php echo BASE_URL.'create/2'?>" class="plan-wrap-a">  دردش معنا <i class="fa fa-arrow-left" aria-hidden="true"></i> </a>  
                                </div>
                                <div class="bottom-wrap">
                                    <ul>
                                        <li class="title">كل من أساسى زائد:</li>
                                        <li>تخصيص سير العمل بالقواعد والأتمتة</li>
                                        <li>قم بإنشاء حملات ذكية متعددة اللمس</li>
                                        <li>السيطرة على من يرى رسول ومتى</li>
                                        <li>الاندماج مع قوة المبيعات و Zendesk و جيثب والمزيد</li>
                                    </ul>  
                                </div>
                            </div>
                            <div class="hide-show-feature">
                                <a href="javascript:void(0)" class="hide-show-feature-a"> إخفاء ميزات إضافية </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="plan-wrap">
                                <div class="paln-intro-wrap">
                                    <div class="paln-intro">
                                        <img src="<?php echo IMG?>logo-symbol.png" class="img-responsive" alt="image">
                                    </div>    
                                    <div class="plan-name"> 
                                        <div class="name">أساسى</div>
                                        <div class="price">
                                            <span>من عند</span>
                                            <p>$ 136</p>
                                            <span>/ شهر</span>
                                        </div>
                                    </div>   
                                    <h5 class="meta-infor">اعمل كفريق لإدارة المحادثات</h5> 
                                    <a href="<?php echo BASE_URL.'create/1'?>" class="plan-wrap-a">دردش معنا <i class="fa fa-arrow-left" aria-hidden="true"></i> </a>  
                                </div>
                                <div class="bottom-wrap">
                                    <ul>
                                        <li class="title"></li>
                                        <li>إدارة المحادثات من قنوات متعددة</li>
                                        <li>إرسال رسائل مستهدفة على موقع الويب الخاص بك أو في المنتج</li>
                                    </ul>  
                                </div>
                        </div>
                        <div class="hide-show-feature">
                            <a href="javascript:void(0)" class="hide-show-feature-a"> إخفاء ميزات إضافية </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-wrap">
                <div class="features">
                    <div class="f-title"><h2 class="">إدارة حوارات الدخول</h2></div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <ul>
                            <li>دردشة مباشرة</li>
                            <li>تطبيقات الموبايل</li>
                            <li>البريد الإلكتروني</li>
                            <li class="disable" >رؤية رسول</li>
                            <li class="disable" >تكامل تويتر</li>
                            <li class="disable" >التكامل الفيسبوك</li>
                            <li class="disable" >إزالة العلامة التجارية swalf</li>
                            <li class="disable" >المتقدمة البريد الإلكتروني وتخصيص البريد الإلكتروني</li>
                        </ul> 
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                        <ul>
                            <li>دردشة مباشرة</li>
                            <li>تطبيقات الموبايل</li>
                            <li>البريد الإلكتروني</li>
                            <li class="" >رؤية رسول</li>
                            <li class="" >تكامل تويتر</li>
                            <li class="" >التكامل الفيسبوك</li>
                            <li class="disable" >إزالة العلامة التجارية swalf</li>
                            <li class="disable" >المتقدمة البريد الإلكتروني وتخصيص البريد الإلكتروني</li>
                        </ul> 
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <ul>
                            <li>دردشة مباشرة</li>
                            <li>تطبيقات الموبايل</li>
                            <li>البريد الإلكتروني</li>
                            <li class="" >رؤية رسول</li>
                            <li class="" >تكامل تويتر</li>
                            <li class="" >التكامل الفيسبوك</li>
                            <li class="" >إزالة العلامة التجارية swalf</li>
                            <li class="" >المتقدمة البريد الإلكتروني وتخصيص البريد الإلكتروني</li>
                        </ul> 
                    </div>
                </div>
                <div class="features features-2">
                        <div class="f-title"><h2 class="">عمليات التشغيل التلقائي</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li> مجمع البريد الإلكتروني</li>
                                <li> ملفات العملاء الرائدين والعملاء</li>
                                <li> توفر فريق ووضع بعيدا</li>
                                <li class="" > البوتات المهمة </li>
                                <li class="disable" > جدولة الاجتماع الآلي </li>
                                <li class="disable" > مؤهل رائد مؤهل </li>
                                <li class="disable" > البوتات المخصصة </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <ul>
                                <li> مجمع البريد الإلكتروني</li>
                                <li> ملفات العملاء الرائدين والعملاء</li>
                                <li> توفر فريق ووضع بعيدا</li>
                                <li class="" > البوتات المهمة </li>
                                <li class="" > جدولة الاجتماع الآلي </li>
                                <li class="" > مؤهل رائد مؤهل </li>
                                <li class="disable" > البوتات المخصصة </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li> مجمع البريد الإلكتروني</li>
                                <li> ملفات العملاء الرائدين والعملاء</li>
                                <li> توفر فريق ووضع بعيدا</li>
                                <li class="" > البوتات المهمة </li>
                                <li class="" > جدولة الاجتماع الآلي </li>
                                <li class="" > مؤهل رائد مؤهل </li>
                                <li class="diable" > البوتات المخصصة </li>
                            </ul> 
                        </div>
                </div>
                <div class="features features-3">
                        <div class="f-title"><h2 class="">التعاون كفريق</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>صندوق الوارد في الفريق</li>
                                <li> ملاحظات و @ الإشارة</li>
                                <li> ساعات العمل </li>
                                <li class="" > تعيين وتأجيل المحادثات </li>
                                <li class="" > الردود المحفوظة </li>
                                <li class="disable" > قواعد الواجب </li>
                                <li class="disable" > أذونات </li>
                                <li class="disable" > سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <ul>
                                <li>صندوق الوارد في الفريق</li>
                                <li> ملاحظات و @ الإشارة</li>
                                <li> ساعات العمل </li>
                                <li class="" > تعيين وتأجيل المحادثات </li>
                                <li class="" > الردود المحفوظة </li>
                                <li class="" > قواعد الواجب </li>
                                <li class="" > أذونات </li>
                                <li class="disable" > سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>صندوق الوارد في الفريق</li>
                                <li> ملاحظات و @ الإشارة</li>
                                <li> ساعات العمل </li>
                                <li class="" > تعيين وتأجيل المحادثات </li>
                                <li class="" > الردود المحفوظة </li>
                                <li class="" > قواعد الواجب </li>
                                <li class="" > أذونات </li>
                                <li class="" > سجلات نشاط الفريق </li>
                            </ul> 
                        </div>
                </div>
                <div class="features features-4">
                        <div class="f-title"><h2 class="">إرسال رسالة إعلامية</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>رسائل تلقائية و يدوية</li>
                                <li>استهداف الجمهور</li>
                                <li> مشغلات الرسالة وجدولة</li>
                                <li class="" >الحملات الذكية</li>
                                <li class="disable" > </li>
                                <li class="disable" > قوالب البريد الإلكتروني المخصصة</li>
                                <li class="disable" > نطاقات البريد الإلكتروني المخصصة </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <ul>
                                <li>رسائل تلقائية و يدوية</li>
                                <li>استهداف الجمهور</li>
                                <li> مشغلات الرسالة وجدولة</li>
                                <li class="" >الحملات الذكية</li>
                                <li class="" > </li>
                                <li class="" > قوالب البريد الإلكتروني المخصصة</li>
                                <li class="" > نطاقات البريد الإلكتروني المخصصة </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>رسائل تلقائية و يدوية</li>
                                <li>استهداف الجمهور</li>
                                <li> مشغلات الرسالة وجدولة</li>
                                <li class="" >الحملات الذكية</li>
                                <li class="" > </li>
                                <li class="" > قوالب البريد الإلكتروني المخصصة</li>
                                <li class="" > نطاقات البريد الإلكتروني المخصصة </li>
                            </ul> 
                        </div>
                </div>
                <div class="features features-5">
                        <div class="f-title"><h2 class="">تقرير عن الأداء</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>لوحة معلومات ملخصة</li>
                                <li>يؤدي لوحة القيادة</li>
                                <li> لوحة تحكم أداء الفريق</li>
                                <li class="" >تصدير CSV </li>
                                <li class="" > تصدير المحادثة الكاملة</li>
                                <li class="disable" > استبيانات الرضا </li>
                                <li class="disable" > تقارير الإيرادات </li>
                                <li class="disable" > تقارير أداء فريق المبيعات </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <ul>
                                <li>لوحة معلومات ملخصة</li>
                                <li>يؤدي لوحة القيادة</li>
                                <li> لوحة تحكم أداء الفريق</li>
                                <li class="" >تصدير CSV </li>
                                <li class="" > تصدير المحادثة الكاملة</li>
                                <li class="" > استبيانات الرضا </li>
                                <li class="disable" > تقارير الإيرادات </li>
                                <li class="disable" > تقارير أداء فريق المبيعات </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>لوحة معلومات ملخصة</li>
                                <li>يؤدي لوحة القيادة</li>
                                <li> لوحة تحكم أداء الفريق</li>
                                <li class="" >تصدير CSV </li>
                                <li class="" > تصدير المحادثة الكاملة</li>
                                <li class="" > استبيانات الرضا </li>
                                <li class="" > تقارير الإيرادات </li>
                                <li class="disable" > تقارير أداء فريق المبيعات </li>
                            </ul> 
                        </div>
                </div>
                <div class="features features-6">
                        <div class="f-title"><h2 class="">التطبيقات والتكاملات</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="disable" > Zendesk </li>
                                <li class="disable" > جيثب </li>
                                <li class="disable" > قوة المبيعات الأساسية </li>
                                <li class="disable" > Marketo </li>
                                <li class="disable" > Clearbit </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="" > Zendesk </li>
                                <li class="" > جيثب </li>
                                <li class="" > قوة المبيعات الأساسية </li>
                                <li class="disable" > Marketo </li>
                                <li class="disable" > Clearbit </li>
                            </ul> 
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>تثاقل</li>
                                <li>أكثر من 100 تطبيق وتكامل</li>
                                <li>إطار المراسلة</li>
                                <li class="" >APIS </li>
                                <li class="" > Zendesk </li>
                                <li class="" > جيثب </li>
                                <li class="" > قوة المبيعات الأساسية </li>
                                <li class="" > Marketo </li>
                                <li class="" > Clearbit </li>
                            </ul> 
                        </div>
                </div>
                <div class="features features-7">
                        <div class="f-title"><h2 class="">الدعم والخدمات</h2></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="disable" > دعم الدردشة</li>
                                <li class="disable" > فريق نجاح شخصي </li>
                                <li class="disable" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 
                            <a href="<?php echo BASE_URL.'create/3'?>" class="plan-wrap-a">دردش معنا</a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 middle-border pro-plan">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="" > دعم الدردشة</li>
                                <li class="disable" > فريق نجاح شخصي </li>
                                <li class="disable" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 
                            <!--  -->
                            <a href="<?php echo BASE_URL.'create/2'?>" class="plan-wrap-a">  دردش معنا <i class="fa fa-arrow-left" aria-hidden="true"></i> </a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul>
                                <li>قاعدة المعرفة</li>
                                <li>تدريب ندوات عبر الإنترنت</li>
                                <li>دعم البريد الإلكتروني</li>
                                <li class="" > دعم الدردشة</li>
                                <li class="" > فريق نجاح شخصي </li>
                                <li class="" > دعم على مدار الساعة طوال أيام الأسبوع </li>
                            </ul> 

                            <a href="<?php echo BASE_URL.'create/1'?>" class="plan-wrap-a">دردش معنا <i class="fa fa-arrow-left" aria-hidden="true"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="chat-icon">
        <a href="">سِ</a>
    </div>
</body>
<?php $this->load->view('components/script')?>
<script type="text/javascript">
    $(document).ready(function() {
        // to hide and show the feature wrap hfkhjhhj hkjhkjh hjkhjk khkjhjh hjhjkh hhkh hhkhjh hkhkh hkhkhkjh hkhjhjh khhh
        $('.hide-show-feature-a').click(function() {
            $('.features-wrap').slideToggle("5000");
        });
});
// habe gebraucht - werde brauchen
// 
</script> 
</html>