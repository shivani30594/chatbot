<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('components/header');?>
<body>
    <div class="navbar-flex" id="navi">
        <div class="navbar-header navbar-right">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand navbar-brand-register-acc" href="#">
                <p> ابدأ مع سوالف </p>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="myNavbar-ul">
                <li><a href="<?php echo BASE_URL?>register">١- اختر باقتك</a></li>
                <li><a href="<?php echo BASE_URL?>register_2" class="current">٢- أنشأ حسابك</a></li>
                <li><a href="<?php echo BASE_URL?>register_3">٣- أبدأ النسخة التجرديبية</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <section class="step-outer">
            <div id="step-2" class= "step-wrap step-two">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="login-form">
                            <div class="step-title">
                                <h2>أنشئ حسابك</h2>
                            </div>
                            <form id="register_2" action="<?php echo BASE_URL?>save_details" method="post">
                                <input type="hidden" name="plan_id" id="plan_id" value="<?php echo isset($plan_id) ? $plan_id : '0'?>">
                                <fieldset class=""> 
                                    <div class="form-group">
                                        <label class="label"> اسم الشركة </label>
                                        <input type="text" class="form-control" placeholder="اسم التطبيق / الشركة" name="company_name" id="company_name">
                                    </div>
                                    <div class="form-group">
                                        <label class="label">حجم الشركة</label>
                                        <select class="form-control" name="company_size" id="company_size">
                                            <option value=''>حدد حجم الشركة</option>
                                            <option value="20">20</option>
                                            <option value="70">70</option>
                                            <option value="200">200</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="label"> اسم المستخدم </label>
                                        <input type="email" class="form-control" placeholder="ادخل اسم المستخدم" name="email_id" id="email_id">
                                        <label id="email_id_error" class="error" style="display:none">Email-id already exists. Try with another! </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="label"> كلمه السر </label>
                                        <input type="password" class="form-control" placeholder="أدخل كلمة المرور" name="password" id="password">
                                    </div>
                                </fieldset>
                                <div class="celarfix">
                                    <button type="submit" class="btn btn-primary">تسجيل</button>
                                </div>
                                <p class="notes">
                                    بالنقر فوق "تسجيل" ، فإنك توافق على  <a href="<?php echo BASE_URL?>terms"> شروط الخدمة </a>و  <a href="<?php echo BASE_URL?>privacy"> سياسة الخصوصية </a> الخاصة بـ Swalfna
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="chat-icon">
        <a href="">سِ</a>
    </div>
</body>
<?php $this->load->view('components/script');?>
<?php if($script):?>
    <?php $this->load->view($script);?>
<?php endif;?>
</html>