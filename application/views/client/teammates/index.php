<section id="main-content" class="pt80">
    <section class="wrapper">
        <div class="">
            <div class="messaging">
                <div class="admin-header">
                    <h1 class="user-h1">  الاعدادات <i class="fa fa-cog" aria-hidden="true"></i> </h1>
                </div>
                <div class="inbox-msg">
                    <div class="setting-left-side team-mate-wrraper">
                        <div class="team-mate-wrraper-inner">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#menu1">اضافات مرسلة</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link " data-toggle="pill" href="#home">اعضاء الفر</a>
                                </li>
                            </ul>
                            <button class="btn btn-success"  data-toggle="modal" data-target="#invitaion-modal"> اضف عضو جديد <i class="fa fa-plus" aria-hidden="true"></i> </button>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade" id="menu1">
                                <table class="table team-mate-table">
                                    <thead>
                                        <tr>
                                            <td style="width: 50%;">اسم</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(count($invited_teammates) > 0):?>
                                        <?php foreach ($invited_teammates as $invited_teammate) :?>
                                        <tr>
                                            <td>
                                                <div class="team-mate-name">
                                                    <div class="media">
                                                        <div class="media-right">
                                                            <img src="<?= (isset($invited_teammate) && !empty($invited_teammate['chatbot_profile'])) ? PROFILE_IMG.$invited_teammate['chatbot_profile'] : IMG."chat-user-avtar.png"?>" class="media-object">
                                                            <div class="online"></div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h5><b><?= (isset($invited_teammate) && !empty($invited_teammate['name'])) ? $invited_teammate['name'] : 'Stranger'?></b></h5>
                                                            <p><?= (isset($invited_teammate) && !empty($invited_teammate['chatbot_email'])) ? $invited_teammate['chatbot_email'] : 'xyz@gmail.com'?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                        <?php else: ?>
                                        <tr>
                                            <td>
                                                There is no in-active invitation remaining..
                                            </td>
                                        </tr>
                                        <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane active" id="home">
                                <div class="table-responsive">
                                    <table class="table team-mate-table">
                                        <thead classs="text-center">
                                            <tr>
                                                <td style="width: 25%;">اسم</td>
                                                <td style="width: 10%;">الحالة</td>
                                                <td style="width: 5%;">عمل</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="team-mate-name">
                                                        <div class="media">
                                                            <div class="media-right">
                                                                <img src="<?= (isset($client_info) && !empty($client_info[0]['chatbot_profile'])) ? PROFILE_IMG.$client_info[0]['chatbot_profile'] : IMG."chat-user-avtar.png"?>" class="media-object">
                                                                <div class="online"></div>
                                                            </div>
                                                            <div class="media-body">
                                                                <h5><b><?= (isset($client_info) && !empty($client_info[0]['name'])) ? $client_info[0]['name'] : 'Stranger'?></b></h5>
                                                                <p><?= (isset($client_info) && !empty($client_info[0]['chatbot_email'])) ? $client_info[0]['chatbot_email'] : 'xyz@gmail.com'?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="name" required>
                                                        <option value="away" selected>غير متواجد</option>
                                                        <option value="active">متواجد</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <a href="#" style="display:none">Edit</a>
                                                    <div id="confirmation-warpper" style="display:none">
                                                        <a type="button" id='cancel_button'>الغاء</a>
                                                        <span> | </span> 
                                                        <a type="button" id='confirm_button'>حذف</a>
                                                        
                                                    </div>
                                                    <div id="delete-wrapper">
                                                        <a type="button" id='delete_button'>حذف</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php if(count($teammates) > 0):?>
                                            <?php foreach ($teammates as $teammate) :?>
                                            <tr>
                                                <td>
                                                    <div class="team-mate-name">
                                                        <div class="media">
                                                            <div class="media-right">
                                                                <img src="<?= (isset($teammate) && !empty($teammate['chatbot_profile'])) ? PROFILE_IMG.$teammate['chatbot_profile'] : IMG."chat-user-avtar.png"?>" class="media-object">
                                                                <div class="online"></div>
                                                            </div>
                                                            <div class="media-body">
                                                                <h5><b><?= (isset($teammate) && !empty($teammate['name'])) ? $teammate['name'] : 'Stranger'?></b></h5>
                                                                <p><?= (isset($teammate) && !empty($teammate['chatbot_email'])) ? $teammate['chatbot_email'] : 'xyz@gmail.com'?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="name" required>
                                                        <option value="away" selected>غير متواجد</option>
                                                        <option value="active">متواجد</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <a href="#" style="display:none">Edit</a>
                                                    <div id="confirmation-warpper" class="confirmation-warpper-<?=$teammate['id']?>" style="display:none">
                                                        <a type="button" id='cancel_button' data-teammateid="<?= $teammate['id']?>">الغاء</a>
                                                        <span> | </span> 
                                                        <a type="button" id='confirm_button' data-teammateid="<?= $teammate['id']?>">حذف</a>
                                                        
                                                    </div>
                                                    <div id="delete-wrapper" class="delete-warpper-<?=$teammate['id']?>">
                                                        <a type="button" id='delete_button' data-teammateid="<?= $teammate['id']?>">حذف</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="inbox-people">
                        <div class="user-btn-mobile">               
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <div class="main-chat-list">    
                            <div class="setting-right-side">                                
                                <div class="dropdown">   
                                    <button class="btn <?=($this->uri->segment(1) == 'general_settings' OR $this->uri->segment(1) == 'teammates') ? 'profile-menu-active' : ''?>" type="button" id="drop-btn" >مساحة العمل ، الافرقة   
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>   
                                        <span id="btn_status" style="display:none">on</span>   
                                    </button>    
                                    <ul class="menu">  
                                        <li><a href="<?php echo BASE_URL?>general_settings" class="<?=$this->uri->segment(1) == 'general_settings' ? 'active' : ''?>"> الاعادادات العامة </a></li>  
                                        <li><a href="<?php echo BASE_URL?>teammates" class="<?=$this->uri->segment(1) == 'teammates' ? 'active' : ''?>""> اعضاء الفريق </a></li>         
                                    </ul>
                                </div>              
                                <a href="<?php echo BASE_URL?>install" class="<?=$this->uri->segment(1) == 'install' ? 'profile-menu-active' : ''?>"> <h3>التثبيت</h3> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<div class="modal fade" id="invitaion-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">اضافة عضو فريق جديد</h4>
            </div>
            <form action="<?= BASE_URL ?>invite_member" method="post" id="invitation_form">
                <div class="modal-body">
                    <label for="">اضافة عبر البريد</label>
                    <input type="email" name="teammate_email" id="teammate_email" class="form-control" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">قريب</button>
                    <button type="submit" class="btn btn-primary">إرسال</button>
                </div>
            </form>
        </div>
    </div>
</div>