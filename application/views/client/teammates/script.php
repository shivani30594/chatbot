<script>

$(document).ready(function() {
    $(document).on('click',"#drop-btn", function()
    {
        $("#btn_status").text() == 'off' ? $(this).addClass("open") : $(this).removeClass("open")  ;
        $("#btn_status").text() == "off" ? $("#btn_status").text("on") : $("#btn_status").text("off");
        $(".menu").slideToggle();
    });
    $('.fixture_timezone').timezones();
    $(document).on('change',"#teammate_email", function(e)
    {
        jQuery.ajax({
            url : '<?php echo BASE_URL?>check_teammate_email',
            method: 'post',
            dataType: 'json',
            data: {email_id: e.target.value},
            success: function(response){
                if (response.status == 'found')
                {
                    $("#teammate_email").val('');
                }
            }
        });
    });
    $('#invitation_form').validate({
        rules: {
            teammate_email : { 
                required :true,
            },
        },
        messages: {
            teammate_email : {
                required : 'معرف البريد الإلكتروني موجود بالفعل',
            },
        }
    });
    $(document).on('click',"#delete_button", function(e)
    {
        var t_id = $(this).data("teammateid");
        $(".delete-warpper-"+t_id).hide();
        $(".confirmation-warpper-"+t_id).show();
    });
    $(document).on('click',"#cancel_button", function(e)
    {
        var t_id =  $(this).data("teammateid");
        $(".delete-warpper-"+t_id).show();
        $(".confirmation-warpper-"+t_id).hide();
    });
    $(document).on('click',"#confirm_button", function(e)
    {
        var t_id =  $(this).data("teammateid");
        $(".delete-warpper-"+t_id).show();
        $(".confirmation-warpper-"+t_id).hide();
       // call the ajax function
       $(this).closest('tr').remove();
       jQuery.ajax({
            url : '<?php echo BASE_URL?>delete_teammate',
            method: 'post',
            dataType: 'json',
            data: {teammate_id: t_id},
            success: function(response){
            }
        });
    });
});

</script>