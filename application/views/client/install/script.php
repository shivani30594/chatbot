
<script>
$(document).ready(function() {
    $(document).on('click',"#drop-btn", function()
    {
        $("#btn_status").text() == 'off' ? $(this).addClass("open") : $(this).removeClass("open")  ;
        $("#btn_status").text() == "off" ? $("#btn_status").text("on") : $("#btn_status").text("off");
        $(".menu").slideToggle();
    });

    // $(document).on('click', "#copy_btn", function()
    // {
    //     var input = document.getElementById("content");
    //     var isiOSDevice = navigator.userAgent.match(/ipad|iphone/i);
    //     target = document.getElementById("content");
    //     if (isiOSDevice) {
    //         var editable = input.contentEditable;  
    //         var readOnly = input.readOnly;             
    //         input.contentEditable = true;
    //         input.readOnly = false;                                         
    //         var range = document.createRange();
    //         range.selectNodeContents(input);
    //         var selection = window.getSelection();
    //         selection.removeAllRanges();
    //         selection.addRange(range);
    //         input.setSelectionRange(0, 999999);
    //         input.contentEditable = editable;
    //         input.readOnly = readOnly;
    //         document.execCommand('copy');              
    //     } else {
    //         var $temp = $("<textarea>");
    //         $("body").append($temp);
    //         $temp.val($(this).text()).select();
    //         document.execCommand("copy");
    //         $temp.remove();
    //         // input.select();
    //     }       
    //     $("#copy_btn").text("تم النسخ");
    //     setTimeout(
    //       	function() 
    //       	{
    //       		$("#copy_btn").text('نسخ إلى الحافظة');
    //       	}, 2000);               
    // })

   

});

function copyToClipboard(element) 
{
    var input = document.getElementById("content");
    var isiOSDevice = navigator.userAgent.match(/ipad|iphone/i);
    if (isiOSDevice) {
            var editable = input.contentEditable;  
            var readOnly = input.readOnly;             
            input.contentEditable = true;
            input.readOnly = false;                                         
            var range = document.createRange();
            range.selectNodeContents(input);
            var selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
            input.setSelectionRange(0, 999999);
            input.contentEditable = editable;
            input.readOnly = readOnly;
            document.execCommand("copy");
        } else {
            var $temp = $("<textarea>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }       
    $("#copy_btn").text("تم النسخ");
    setTimeout(
        function() 
        {
            $("#copy_btn").text('نسخ إلى الحافظة');
        }, 2000);
    }
</script>
