<section id="main-content" class="pt80">
    <section class="wrapper">
        <div class="">
            <div class="messaging">
                <div class="admin-header">
                    <h1 class="user-h1">  الاعدادات <i class="fa fa-cog" aria-hidden="true"></i> </h1>
                </div>
                <div class="inbox-msg">
                    <div class="setting-left-side">
                        <div class="install-page-wrapper">
                            <p>
                                يتيح لك تثبيت محدث سوالف على موقع الويب الخاص بك و مزامنة بيانات المستخدم المباشر مع محدث سوالف وتمكينك من التحدث إلى عملائك
                                .وإرسال رسائل داخل التطبيق
                            </p>
                            <h3><strong> تثبيت المحدث لمستخدمين الموقع الالكتروني</strong></h3>
                            <h4>انسخ هذا الرمز والصقه قبل علامة &lt;body/&gt; في كل صفحة تريد ظهور محدث سوالف لزائري الموقع </h4>
                            <div class="code-viewer">
                                <!-- <input type="text" name="content" id="content" value="<?php echo isset($code_snippet) ? $code_snippet : ''?>  "> -->
                                <textarea rows="10" cols="50" translate="no" id="content" name="content" readonly>                                                                                                                                                                                                                                                     
                                    <?php echo isset($code_snippet) ? $code_snippet : ''?>                                                                                                                                                                                                                                                                                  
                                </textarea>                            
                                <button class="btn keep-up-btn" id="copy_btn" type="button" onclick="copyToClipboard('#content')">نسخ إلى الحافظة</button>                                  
                            </div>
                        </div>  
                    </div>
                    <div class="inbox-people">                   
                        <div class="user-btn-mobile">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>                                                                                                                                                                                                                                        
                        <div class="main-chat-list">
                            <div class="setting-right-side">
                                <div class="dropdown">
                                    <button class="btn <?=($this->uri->segment(1) == 'general_settings' OR $this->uri->segment(1) == 'teammates') ? 'profile-menu-active' : ''?>" type="button" id="drop-btn">مساحة العمل ، الافرقة
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        <span id="btn_status" style="display:none">off</span>
                                    </button>
                                    <ul class="menu" style="display:none">
                                        <li><a href="<?php echo BASE_URL?>general_settings" class="<?=$this->uri->segment(1) == 'general_settings' ? 'active' : ''?>"> الاعادادات العامة </a></li>
                                        <li><a href="<?php echo BASE_URL?>teammates" class="<?=$this->uri->segment(1) == 'teammates' ? 'active' : ''?>"> اعضاء الفريق </a></li>                                   
                                    </ul>                                                    
                                </div>
                                <a href="<?php echo BASE_URL?>install" class="<?=$this->uri->segment(1) == 'install' ? 'profile-menu-active' : ''?>"> <h3>التثبيت</h3> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </section>
</section>        
                                        