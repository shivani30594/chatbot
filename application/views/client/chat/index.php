<div id="myDiv">
    <div class="loader" id="loading-image"></div>
</div>
<section id="main-content">
	<section class="wrapper chat-wrapper">
            <div class="messaging">
                <div class="admin-header">
                    <h1 class="user-h1"> أنت</h1>
                </div>
                <div class="inbox-msg">
                    <div class="mesgs">
                        <div class="mesgs-header"></div>
                        <div class="mesgs-inner-wrap">
                            <div class="msg_history" id="msg_scroll">
                                <div class="msg-width-wrap">
                                    <div class="dwld-prvious-msg text-center">
                                        <a id="previous_msg_btn">تحميل الرسائل السابقة</a>
                                        <p>14 مساءً 12 سبتمبر 2018</p>
                                    </div>
                                    <!-- <div id='set_div' style="display:none"></div> -->
                                    <div id="messages">
                                    </div> 
                                </div>
                            </div>
                            <form id="submitForm" method="POST"> 
                                <div class="type-area">
                                    <button class="send-btn" href=""  >
                                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                    </button>
                                    <p class="lead">
                                        <textarea id="message" class="form-control" rows="1" data-emojiable="true" required></textarea>
                                    </p>
                                    <div class="cntrl-grp">
                                        <div class="element">
                                            <i id="image_span" class="fa fa-paperclip" aria-hidden="true"></i>
                                            <input type="file" name="image_file" id="image_file" accept="image/*"  >
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="inbox-people">
                        <div class="user-btn-mobile">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </div>
                        <div class="headind_srch">
                            <div class="recent_heading">
                                <img src="<?php echo IMG?>inbox.png" alt="icon">
                                <select>
                                    <option value="">0</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                </select>
                                <select>
                                    <option value=""> مستخدم وهمية</option>
                                    <option value="">1 مستخدم وهمية </option>
                                    <option value="">2 مستخدم وهمية </option>
                                    <option value="">3 مستخدم وهمية </option>
                                </select>
                            </div>
                        </div>
                        <div class="main-chat-list">
                            <div class="user-fillter">
                                <h3>المتعاملين </h3>
                                <select name="chat-filter" id="chat-filter" class="chat-filter">
                                    <option value="all" selected>كل المتعاملين </option>
                                    <option value="online">التواجدين </option>
                                    <option value="new">الجدد </option>
                                </select>
                            </div>
                            <div class="inbox_chat">
                            </div>
                            <div class="show-more" id="show-more" style="display:none">
                                <p id="page_no" style="display:none">1</p>
                            </div>
                            <div class="load-more" lastID="1" style="display: none;">
                                <img src="<?php echo IMG?>loading.gif"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</section>
