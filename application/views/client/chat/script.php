<script>
jQuery(document).ready(function($) {

    var screeHt = $('body').height();
    var headerHt = $('header').height();
    var finalHt = screeHt - headerHt - 61;
    $('.mesgs').height(finalHt);

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    //start - Initial declaration
    var socket = io.connect("http://18.217.216.88:3000",{transports: ['websocket','flashsocket','polling'] });
    var uploader = new SocketIOFileClient(socket);
    socket.emit('privatechatroom', {"email":'<?php echo strtolower($this->session->userdata('email'))?>'});
    var type = $("#chat-filter").val();
    var page_no = type == 'all' ? $("#page_no").html() : '';
    $.ajax({
        url: '<?php echo BASE_URL?>fetch_record',           
        type: 'POST',   
        data : {type : type,page_no:page_no},   
        beforeSend: function() {
            $("body").addClass("loading");
            $("#loading-image").show();
        },    
        success: function(response)
        {
            chat_filter_content(response); // set chat-filter content based on response
            $("#loading-image").hide();
            $("body").removeClass("loading");
        }
    })
    //end - initial declaration

    // Click on attachment button
    $("#image_span").click(function () {
        $("input[name=image_file]").trigger('click');
    });

    // Change on attachment image file
    $(document).on("change","#image_file", function(event)
    {
        var fileEl = document.getElementById('image_file');
        var uploadIds = uploader.upload(fileEl);
    });

    // Change on attachment image file
    var typing = false;
    var timeout = undefined;

    function timeoutFunction(){
        typing = false;
        var id =  $(".active_chat").attr("id");
        socket.emit("noLongerTypingMessage",{visitor_id : id.toString(),client_id :'<?php echo strtolower($this->session->userdata("user_id"))?>',own_email:'<?php echo strtolower($this->session->userdata("email"))?>'});
    }

    document.getElementById('message').addEventListener('keydown', function (e){
        var key = e.which;
        if(key != 13)  // the enter key code
        {
            if(typing == false) {
                typing = true
                var id =  $(".active_chat").attr("id");
                socket.emit("typingMessage",{visitor_id : id.toString(),client_id :'<?php echo strtolower($this->session->userdata("user_id"))?>',own_email:'<?php echo strtolower($this->session->userdata("email"))?>'});
                timeout = setTimeout(timeoutFunction, 5000);
            } else {
                clearTimeout(timeout);
                timeout = setTimeout(timeoutFunction, 5000);
            }
        }
    }, false);


    $(".inbox_chat").scroll(function(e)
    {
        var lastID = $('.load-more').attr('lastID');
        if  (($(window).scrollTop() == $(document).height() - $(window).height()) && (lastID != 0))
        {
            var page_no = $("#page_no").html();
            $("#page_no").html(parseInt(page_no) + 1);
            var filter_type = $("#chat-filter").val();
            var page_no = filter_type == 'all' ? $("#page_no").html() : '';
            $.ajax({
                url: '<?php echo BASE_URL?>fetch_record',           
                type: 'POST',  
                data : {type : filter_type, page_no: page_no},   
                beforeSend: function() {
                    $('.load-more').show();
                },      
                success: function(response)
                {
                    $('.load-more').hide();
                    if(response.status == 'no') {
                        $('.load-more').attr('lastID',0);
                    }
                    else {
                        chat_filter_content(response); // set chat-filter content based on response
                        $('.inbox_chat').animate({scrollTop: $('.inbox_chat')[0].scrollHeight}, 1000);
                    }
                    $("body").removeClass("loading");
                }
            });
        }
    });   
    
    // on click of chat listing
    $(document).on("click", ".chat_list", function()
    {
        socket.emit("leave room");
        localStorage.clear();
        $('.msg_history').animate({scrollTop: $('.msg_history')[0].scrollHeight}, 1000);
        var id =  $(".active_chat").attr("id");
        $("#"+id).removeClass("active_chat");
        var id = $(this).attr("id");
        $("#p_"+id).css("font-weight","100");
        var chat_id = $(this).attr('data-chat-id');
        $("#"+id).addClass("active_chat");
        socket.emit('client join', {visitor_id : id.toString(), email:id.toString(), client_id :'<?php echo strtolower($this->session->userdata("user_id"))?>',own_email:'<?php echo strtolower($this->session->userdata("email"))?>' });
	    socket.emit('read_chat',{"sender_id":'<?php echo strtolower($this->session->userdata("user_id"))?>', "receiver_id": id});
        socket.emit('update record', {"chat_id" : chat_id,"active_id":id});
        // clear the notification from the notification as we open the chat. So, we can consider it as marked notification and so that we can remove it from the notification block
        clear_individual_notification(id);

   }); 

    // click on previous button chat
    $(document).on('click','#previous_msg_btn', function(){
        if (localStorage.getItem("_gid") == 0)
        {
            alert("You are already reached o end of the message conversation");
            return false;
        }
        var id = $(".active_chat").attr("id");
        if ( localStorage.getItem("_gid") == undefined ) { console.log("undefined") }
        else { 
            var new_gid = parseInt(localStorage.getItem("_gid"))-1;
            localStorage.setItem("_gid", new_gid);
            $(".group_"+new_gid).show(); 
            $('.msg_history').animate({scrollBottom: $('.msg_history')[0].scrollHeight}, 1000);
        }
    });

    // change ick on chat-filter-option
    $(document).on("change","#chat-filter", function()
    {
        var filter_type = $("#chat-filter").val();
        filter_type == 'all' ? $("#show-more").show() : $("#show-more").hide() ;
        var page_no = filter_type == 'all' ? 1 : '';
        filter_type == 'all' ? $("#page_no").text("1") : '';
        $(".inbox_chat").html("");
        // $(".msg_history").html("");
        $.ajax({
            url: '<?= BASE_URL?>fetch_record',           
            type: 'POST',  
            data : {type : filter_type, page_no: page_no},  
            beforeSend: function() {
                $("#loading-image").show();
                $("body").addClass("loading");
            },       
            success: function(response)
            {
                chat_filter_content(response); // set chat-filter content based on response
                $("#loading-image").hide();
                $("body").removeClass("loading");
            }
        })
    });

    // click on the send button will send the message
    $('form').submit(function () 
    {   
        var receiver_id = $(".active_chat").attr("id");
        var message = $('#message').val();
        if (message.trim() != '')
        {
            message = message.replace(/</g, "&lt;").replace(/>/g, "&gt;");
            socket.emit('chat message',{ type :'client', message : message.trim(), attachment:'', date: Date.now(), sender_id : '<?php echo strtolower($this->session->userdata("user_id"))?>', receiver_id : receiver_id, status:"read"});
            $('.msg_history').animate({scrollTop: $('.msg_history')[0].scrollHeight}, 20000);
			$('#message').val('');
            return false;
        }
        return false;
    });

    // When press the enter key, send the message
    $('#message').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            var receiver_id = $(".active_chat").attr("id");
            var message = $('#message').val();
            if (message.trim() != '')
            {
                message = message.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                socket.emit('chat message',{ type :'client', message : message.trim(), attachment:'', date: Date.now(), sender_id : '<?php echo strtolower($this->session->userdata("user_id"))?>', receiver_id : receiver_id, status:"read"});
                $('.msg_history').animate({scrollTop: $('.msg_history')[0].scrollHeight}, 1000);
                $('#message').val('');
                return false;
            }
            return false;
        }
    });

    // checkwidth
    function checkWidth() 
    {
        var windowsize = $window.width();
        if (windowsize < 768) {
            $("#sidebar").addClass("side-bar-mobile");
            $("#main-content").addClass("main-content-mobile");
        }
    }

    // Set display chat-list based on the filter
    function chat_filter_content(response)
    {
        if (response.data != undefined && response.data.length > 0)
        {
            $(".chat_list").removeClass("active_chat");
            socket.emit('display contacts',{"sender_id":'<?php echo strtolower($this->session->userdata("user_id"))?>',"data":response.data});
        }
    }
    
    // on load function display chat of activated chat
    socket.on('display contacts', function(data)
    {
        console.log("data",data)
        var getting_id = getParameterByName('id');
        var html = '';
        // var mergedList = _.map(data.data, function(item){
        //     return _.extend(item, _.findWhere(data.records.data, { chat_id: item.chat_id }));
        // });
           var mergedList = _.map(data.records.data, function(item){
            return _.extend(item, _.findWhere(data.data, { chat_id: item.chat_id }));
        });
        console.log("merger list", mergedList);
        for(i=0;i<mergedList.length;i++)
        {
            var class_name = (getting_id != null)  ? (i == 0 ? 'active_chat' : '') : '';
            if (getting_id == null || getting_id == undefined)
            {
                if (i == 0)
                    var class_name ='active_chat';
                else
                    var class_name = '';
            }
            else
            {
                var class_name = '';
            }
            if (mergedList[i].message == 'attachment')
                var msg = '<i class="fa fa-camera" aria-hidden="true"></i> Photo';
            else
                var msg = mergedList[i].message;
            if (mergedList[i].full_name != '')
                name = mergedList[i].full_name;
            else if (mergedList[i].email != '')
                name = mergedList[i].email;
            else if (mergedList[i].phone_number != null)
                name = mergedList[i].phone_number;
            else 
                name = mergedList[i].ip;
            html += chat_contact_info(mergedList[i].notifications_count,class_name, mergedList[i].unique_id, mergedList[i].email, name, mergedList[i].send_date, msg,mergedList[i].msg_status, mergedList[i].chat_id, mergedList[i].status,'',mergedList[i].notifications_count)
         
        }
        var filter_type = $("#chat-filter").val();
        $(".inbox_chat").append(html);
        if (getting_id != null)
        {
            $("#"+getting_id).addClass("active_chat");
            socket.emit('read_chat',{"sender_id":'<?php echo strtolower($this->session->userdata("user_id"))?>', "receiver_id": getting_id});
            socket.emit('client join', {visitor_id : getting_id.toString(), email:(getting_id).toString(), client_id :'<?php echo strtolower($this->session->userdata("user_id"))?>',own_email:'<?php echo strtolower($this->session->userdata("email"))?>' });
            // console.log("getting_id", getting_id);
            clear_individual_notification(getting_id);
        }
        else{
            var id = $(".active_chat").attr("id");
            socket.emit('read_chat',{"sender_id":'<?php echo strtolower($this->session->userdata("user_id"))?>', "receiver_id": id});
            socket.emit('client join', {visitor_id : id.toString(), email:(id).toString(), client_id :'<?php echo strtolower($this->session->userdata("user_id"))?>',own_email:'<?php echo strtolower($this->session->userdata("email"));?>' });
            clear_individual_notification(id);
            // console.log("id", id);

        }
    });

    // update record
    socket.on('update record', function(data) {
        $("#p_"+data.active_id).css("font-weight","100");
    });

    // refelct visitor info
    socket.on('reflect visitor info', function(data) {
        console.log("dataaa", data)
        $("#info_"+data.visitor_id).text(data.modify_info);
    });
    // error 
    socket.on('error', function()
    {
        console.log("Sorry, there seems to be an issue with the connection!");
    });

    // if connection error
    socket.on('connect_error', function(err)
    {
        console.log("connect failed"+err);
    });

    // IF connect successfully
    socket.on('connect', function ()
    {
        console.log("connected");
    });

    // Image attachment upload succssfully
    uploader.on('complete', function(fileInfo) {
        var id = $(".active_chat").attr("id");
		socket.emit('chat message',{ type :'client', message : '', attachment:fileInfo.name, date: Date.now(), sender_id : '<?php echo strtolower($this->session->userdata("user_id"))?>', receiver_id : id,status:'read'});
        socket.emit('receive file',{file_name : fileInfo.name, sender_id : '<?php echo strtolower($this->session->userdata("user_id"))?>', receiver_id : id , type:'client' }); // sender means client over here
	});

    // If image attachment have error
	uploader.on('error', function(err) {
		alert(err);
	});

    // IF image attachment abort the transaction
	uploader.on('abort', function(fileInfo) {
		alert("Aborted: Please try again");
	});

    // if new visitor added successfully
    socket.on('new visitor added', function(data){
        var id = $(".active_chat").attr("id");
        if ((id) == undefined)
        {
            var chat_id = $('#'+id).attr('data-chat-id');
            var class_name = 'active_chat';
            var status = 'read';
            socket.emit('update record', {"chat_id" : chat_id});
            socket.emit('client join', {visitor_id : data.unique_id.toString(),email:(data.unique_id).toString(), client_id :'<?php echo strtolower($this->session->userdata("user_id"))?>',own_email:'<?php echo strtolower($this->session->userdata("email"))?>' });
        }
        else{
            var class_name = '';
            var status = 'unread';
            // var style = "style='font-weight:bold'";
        }
        var date = new Date();
        var timestamp = date.getTime();
        var html = chat_contact_info('0',class_name, data.unique_id, data.visitor_email, data.ip_value, timestamp,'',status,'');
        
        $(".inbox_chat").append(html)
    });
    // 
    // If we receive the file from visitor and we want to display it or render it
    socket.on('receive file', function(data)
    {
        var socket_id = socket.id;
        id = Math.floor(Math.random() * (1000000 - 1000 + 1)) + 1000;
        var imga = document.createElement("img");
        imga.setAttribute("width", "100px");
        imga.setAttribute("height", "100px");
        var div = $('#messages').children().last();
        if (div.length > 0)
        {
            var main_class =  div[0] ;
            class_str = $(main_class).attr("class");
            class_name = class_str.substr(0,class_str.indexOf(' '));
        }
        else
        {
            class_name = '';
        }
        if (data.sender_id == $(".active_chat").attr("id") && data.comes_from == 'visitor')
        {
            if (class_name == 'incoming_msg')
            {
                var html = receiver_change_html('',Date.now(), true, id);
                var new_main_class = $(main_class).children().first();
                $(new_main_class).append(html);
            }
            else{
                var html = receiver_html('',Date.now(), true, id);
                $("#messages").append(html);
            }
            $("#p_"+data.sender_id).html('<i class="fa fa-camera" aria-hidden="true"></i> Photo'); // for last message update
        }
        else if (data.sender_id != $(".active_chat").attr("id") && data.comes_from == 'visitor')
        {
            $("#"+data.sender_id).children().addClass("online");
            $("#"+data.sender_id).attr("data-chat-id",data.chat_id);
            $("#p_"+data.sender_id).html('<i class="fa fa-camera" aria-hidden="true"></i> Photo'); // for last message update
            // notification code
            // append_notification(data.sender_id);
        }
        else if (data.comes_from == 'client' && data.receiver_id == $(".active_chat").attr("id") )
        {
            if (class_name == 'outgoing_msg')
            {
                var html = sender_change_html('',Date.now(), true, id);
                var new_main_class = $(main_class).children().last();
                $(new_main_class).append(html);
            }
            else
            {
                var html = sender_html('',Date.now(), true, id);
                $("#messages").append(html);
            }
            $("#p_"+data.receiver_id).html('<i class="fa fa-camera" aria-hidden="true"></i> Photo'); // for last messgae update
        }
        
        $('.msg_history').animate({scrollTop: $('.msg_history')[0].scrollHeight}, 1000);
        $("#"+id).html(imga);
        imga.setAttribute("src","data:image/png;base64,"+data.data);
    })

    // set the send message as html
    socket.on('chat message', function (data) 
    {
        var socket_id = socket.id;
        id = Math.floor(Math.random() * (1000000 - 1000 + 1)) + 1000;
        var div = $('#messages').children().last();
        if (div.length > 0)
        {
            var main_class =  div[0] ;
            class_str = $(main_class).attr("class");
            class_name = class_str.substr(0,class_str.indexOf(' '));
        }
        else
        {
            class_name = '';
        }
        if (data.sender_id == $(".active_chat").attr("id") && data.comes_from == 'visitor')
        {
            if (data.message == 'attachment')
            {
                var imga = document.createElement("img");
                imga.setAttribute("width", "100px");
                imga.setAttribute("height", "100px");
                // function receiver_html(msg, date, attachment = false, id = '',i, chat_id = '')
                if (class_name == 'incoming_msg')
                {
                    html = receiver_change_html(data.message, Date.now(), true,id);
                    var new_main_class = $(main_class).children().first();
                    $(new_main_class).append(html);
                    $("#"+id).html(imga);
                    imga.setAttribute("src","<?php echo UPLOAD;?>"+result[i].attachment);
                }
                else
                {
                    html = receiver_html(data.message, Date.now(), true, id); // sender _chat
                    $("#"+id).html(imga);
                    imga.setAttribute("src","<?php echo UPLOAD;?>"+result[i].attachment);
                    $("#messages").append(html);
                }
                $('#messages').animate({scrollTop: $('#messages')[0].scrollHeight}, 1000);
                $("#p_"+data.sender_id).html(data.message);
                var active_chat_id = $(".active_chat").attr("id");
                $("#"+active_chat_id).attr("data-chat-id",data.chat_id);
                $("#"+active_chat_id).detach().prependTo('.inbox_chat'); // this if for when new message arrived just make it at top position
                socket.emit('update record', {"chat_id" : data.chat_id});
            }
            else if(data.message != ''){

                if (class_name == 'incoming_msg')
                {
                    html = receiver_change_html(data.message, Date.now(), false,id);
                    var new_main_class = $(main_class).children().first();
                    $(new_main_class).append(html);
                }
                else
                {
                    html = receiver_html(data.message, Date.now(), false, id); // sender _chat
                    $("#messages").append(html);
                }
                $('.msg_history').animate({scrollTop: $('.msg_history')[0].scrollHeight}, 1000);
                $("#p_"+data.sender_id).html(data.message);
                var active_chat_id = $(".active_chat").attr("id");
                $("#"+active_chat_id).attr("data-chat-id",data.chat_id);
                // $("#"+active_chat_id).prepend("#messages");
                $("#"+active_chat_id).detach().prependTo('.inbox_chat'); // this if for when new message arrived just make it at top position
                socket.emit('update record', {"chat_id" : data.chat_id});
                message = data.message.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
                $("#p_"+id).text(String(message));
            }
        }
        else if (data.sender_id != $(".active_chat").attr("id") && data.comes_from == 'visitor')
        {
            $("#"+data.sender_id).children().addClass("online");
            $("#"+data.sender_id).attr("data-chat-id",data.chat_id);
            $("#p_"+data.sender_id).html(data.message);
            $("#p_"+data.sender_id).css("font-weight","bold");
            // notification code
            $("#"+data.sender_id).detach().prependTo('.inbox_chat'); // this if for when new message arrived just make it at top position
            $("#chat_notification_"+data.sender_id).html(parseInt($("#chat_notification_"+data.sender_id).html()) + 1);
            console.log($("#chat_notification_"+data.sender_id).html(),"notification count");
            $("#chat_notification_"+data.sender_id).css("display","inline-block"); 
            append_notification(data.sender_id, data.message, data.chat_id, data.file_name);
        }
        else if (data.comes_from == 'client' && data.receiver_id == $(".active_chat").attr("id"))
        {
            if (data.message == 'attachment')
            {
                var imga = document.createElement("img");
                imga.setAttribute("width", "100px");
                imga.setAttribute("height", "100px");
                if (class_name == 'outgoing_msg')
                {
                    html = sender_change_html(data.message, Date.now(), true,id);
                    var new_main_class = $(main_class).children().last();
                    $(new_main_class).append(html);
                    $("#"+id).html(imga);
                    imga.setAttribute("src","<?php echo UPLOAD;?>"+result[i].attachment);
                }
                else
                {
                    html = sender_html(data.message, Date.now(), true, id); // sender _chat
                    $("#"+id).html(imga);
                    imga.setAttribute("src","<?php echo UPLOAD;?>"+result[i].attachment);
                    $("#messages").append(html);
                }
                $('.msg_history').animate({scrollTop: $('.msg_history')[0].scrollHeight}, 1000);
                $("#p_"+data.receiver_id).html(data.message); // update last message into http://prntscr.com/mkidxn
                var active_chat_id = $(".active_chat").attr("id");
                $("#"+active_chat_id).attr("data-chat-id",data.chat_id);
                socket.emit('update record', {"chat_id" : data.chat_id});
            }
            else if(data.message != '')
            {
                if (class_name == 'outgoing_msg')
                {
                    html = sender_change_html(data.message, Date.now(), false,id);
                    var new_main_class = $(main_class).children().last();
                    $(new_main_class).append(html);
                }
                else
                {
                    html = sender_html(data.message, Date.now(), false, id); // sender _chat
                    $("#messages").append(html);
                }
                $('.msg_history').animate({scrollTop: $('.msg_history')[0].scrollHeight}, 1000);
                $("#p_"+data.receiver_id).html(data.message); // update last message into http://prntscr.com/mkidxn
                message = data.message.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
                $("#p_"+id).text(String(message));
                var active_chat_id = $(".active_chat").attr("id");
                $("#"+active_chat_id).attr("data-chat-id",data.chat_id);
                socket.emit('update record', {"chat_id" : data.chat_id});
            }
        }
        $.ajax({
            url: '<?php echo BASE_URL?>update_last_seen',           
            type: 'POST',          
            data: {chat_id : data.chat_id, visitor_id : data.receiver_id},          
            success: function(response){}
        })
    });
    
    function test()
    {
        // console.log("testst", $(".msg_history").height())
        // console.log($("#messages").height())
        alert('fgsdfg');
        var height =  $("#messages").height();
        $(".msg-width-wrap").animate({ scrollTop: height }, 1000, function(){
            console.log("animation called")
        });
        // $(".msg_history").animate({ scrollTop: height }, 1000, function(){
        //     console.log("animation called")
        // });
        return false;
    }

    // read the conversation
    socket.on('read messages', function(data)
    {
        $('.msg_history').animate({scrollTop: $('.msg_history')[0].scrollHeight}, 1000);
        var receiver_id = data.receiver_id;
        document.getElementById("messages").innerHTML = "";
        if (data.Count > 0)
        {
            var result = (data.Items).sort(function(a, b) {
                return parseFloat(a.send_date) - parseFloat(b.send_date);  
            });
            localStorage.setItem("_gid", Math.ceil(data.Count / 10));
            for (i=0;i<result.length;i++)
            {
                ////////////// check for last msg updation
                var div = $('#messages').children().last();
                if (div.length > 0)
                {
                    var main_class =  div[0] ;
                    class_str = $(main_class).attr("class");
                    class_name = class_str.substr(0,class_str.indexOf(' '));
                }
                else
                {
                    class_name = '';
                }
                id = Math.floor(Math.random() * (1000000 - 1000 + 1)) + 1000;
                if (result[i].receiver_id == receiver_id)
                {
                    if (result[i].message == 'attachment' )
                    {
                        var imga = document.createElement("img");
                        imga.setAttribute("width", "100px");
                        imga.setAttribute("height", "100px");
                        if (class_name == 'outgoing_msg')
                        {
                            html = sender_change_html(result[i].message,result[i].send_date, true,id);
                            var new_main_class = $(main_class).children().last();
                            $(new_main_class).append(html);
                            $("#"+id).html(imga);
                            imga.setAttribute("src","<?php echo UPLOAD;?>"+result[i].attachment);
                        }
                        else
                        {
                            sndr_html = sender_html(result[i].message,result[i].send_date,true,id, i); // sender_chat . here we used i for the previous chat load functionaity
                            $("#messages").append(sndr_html);
                            $("#"+id).html(imga);
                            imga.setAttribute("src","<?php echo UPLOAD;?>"+result[i].attachment);
                        }
                    }
                    else{   

                        if (class_name == 'outgoing_msg')
                        {
                            html = sender_change_html(result[i].message,result[i].send_date, false,id);
                            var new_main_class = $(main_class).children().last();
                            $(new_main_class).append(html);
                        }
                        else
                        {
                            sndr_html = sender_html(result[i].message,result[i].send_date,false,id,i); // sender_chat
                            $("#messages").append(sndr_html);
                        }
                        message = result[i].message.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
                        $("#p_"+id).text(message);
                    }
                }
                else{
                    if (result[i].message == 'attachment')
                    {
                        var imga = document.createElement("img");
                        imga.setAttribute("width", "100px");
                        imga.setAttribute("height", "100px");
                        if (class_name == 'incoming_msg')
                        {
                            html = receiver_change_html(result[i].message,result[i].send_date, true,id);
                            var new_main_class = $(main_class).children().first(); 
                            $(new_main_class).append(html);
                            $("#"+id).html(imga);
                            imga.setAttribute("src","<?php echo UPLOAD;?>"+result[i].attachment);
                        }
                        else{
                            rcvr_html = receiver_html(result[i].message,result[i].send_date,true,id,i); // receiver_Chat
                            $("#messages").append(rcvr_html);
                            $("#"+id).html(imga);
                            imga.setAttribute("src","<?php echo UPLOAD;?>"+result[i].attachment);
                        }
                    }
                    else{
                        if (class_name == 'incoming_msg')
                        {
                            html = receiver_change_html(result[i].message,result[i].send_date, false,id);
                            var new_main_class = $(main_class).children().first(); 
                            $(new_main_class).append(html);
                        }
                        else
                        {
                            rcvr_html = receiver_html(result[i].message, result[i].send_date,false,id,i); //receiver_Chat
                            $("#messages").append(rcvr_html);
                        }
                        message = result[i].message.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
                        $("#p_"+id).text(message);
                    }
                }
            }
        }
    });
    
    // unique function to calculate the time
    function Unix_timestamp(timestamp)
    {
        var dt = new Date(timestamp);
        var hr = dt.getHours();
        var m = "0" + dt.getMinutes();
        if (hr > 12)
        {
            var am_pm = 'PM';
            hr = hr-12;
        }
        else
        {
            var am_pm = 'AM';
        }
        return hr+ ':' + m.substr(-2)+' '+ am_pm ;  
    }

    // set sender's html
    function sender_html(msg, date, attachment = false, id = '',i, chat_id)
    {
        html_gen = "<div class='outgoing_msg group_"+Math.round(i / 10)+"'>\
                        <div class='incoming_msg_img'> <img src='http://18.217.216.88/Chatbot/assets/img/chat-user-avtar.png' alt='user' class='mCS_img_loaded'> </div>\
                        <div class='sent-msg-wrap'>\
                            <div class='sent_msg'>\
                                <div class='sent-inner-msg-wrap'>";
        html_gen += attachment == true ? " <div id='"+id+"'></div>" : "<p id='p_"+id+"'></p>";
        html_gen += "<span class='time_date'> "+Unix_timestamp(date)+" </span>\
                                </div>\
                            </div>\
                        </div>\
                    </div>";
        return html_gen;                           
    }
   
    // set receiver's html 
    function receiver_html(msg, date, attachment = false, id = '',i)
    {
        html_gen = "<div class='incoming_msg group_"+Math.round(i / 10)+"'>\
                    <div class='rcv-msg-wrap'>\
                        <div class='received_msg'>\
                            <div class='received_withd_msg'>";
        html_gen += attachment == true ? " <div id='"+id+"'></div>" : "<p id='p_"+id+"'></p>";
        html_gen += "<span class='time_date'>  "+Unix_timestamp(date)+" </span>\
                            </div>\
                        </div>\
                    </div>\
                    <div class='incoming_msg_img'> <img src='http://18.217.216.88/Chatbot/assets/img/chat-user-avtar.png' alt='user' class='mCS_img_loaded'> </div>\
                </div>";
        return html_gen;
    }

    // set chat listing content
    function chat_contact_info(notiffication_count,class_name,unique_id,email,full_name,send_date, msg, status = 'read', chat_id='', visiblity_status = '0')
    {
        var class_namee = status == 'read' ? '' : "style = 'font-weight:bolder'";
        var visibility_class = visiblity_status == '1' ? 'online' : '';
        var notification_status = notiffication_count > 0 ? "style = 'display:inline-block'" : '';
        return '<div class="chat_list '+class_name+'" id="'+unique_id+'" data-chat-id='+chat_id+'>\
                    <span id="span_'+unique_id+'" style="display:none">'+email+'</span>\
                    <div class="chat_people '+visibility_class+'">\
                        <div class="chat_img"> <img src="http://18.217.216.88/Chatbot/assets/img/chat-user-avtar.png" alt="user" class="mCS_img_loaded"> </div>\
                        <div class="chat_ib">\
                            <h5><p id="info_'+unique_id+'" class="contact_p">'+full_name+'</p><span '+notification_status+' class="chat_list_notification" id="chat_notification_'+unique_id+'">'+notiffication_count+'</span><span class="chat_date" >'+Unix_timestamp(send_date)+'</span></h5>\
                            <p class="chat-last-message" id="p_'+unique_id+'" '+class_namee+'>'+msg+'</p>\
                        </div>\
                    </div>\
                </div>';
    }

    // Set sender html 
    function sender_change_html(msg, date, attchment = false, id = '')
    {
        html_gen = '<div class="sent_msg">\
                        <div class="sent-inner-msg-wrap">';
        html_gen += attchment == true ? " <div id='"+id+"'></div>" : '<p>'+msg+'</p>';
        html_gen += '<span class="time_date">'+Unix_timestamp(date)+'</span> \
                        </div>\
                      </div>';
        return html_gen;
    }

    // set receiver html
    function receiver_change_html(msg, date, attchment = false, id = '') 
    {
        html_gen = '<div class="received_msg">\
                <div class="received_withd_msg">';
        html_gen += attchment == true ? " <div id='"+id+"'></div>" : '<p>'+msg+'</p>';
        html_gen += '<span class="time_date"> '+Unix_timestamp(date)+'</span>\
                </div>\
              </div>';
        return html_gen;
    }

    //append notification
    function append_notification(sender_id, msg = '', chat_id, file_name)
    {
        // play sound when notification will be arraive
		play_sound();
        // Set the message
        var msg = msg == '' ? file_name : msg;
        // set notification count
        $("#noti_count_"+sender_id).html(parseInt($("#noti_count_"+sender_id).html()) + 1);
        // update the main notification count
        $("#main_noti_count").html(parseInt($("#main_noti_count").html()) + 1);
        $("#main_noti_count").css("display","inline-block"); // hide the main notification count
        // Check if notification is already exist for the partivcualr visitor, we replace it with the newer message
        if ($("#noti_"+sender_id).length > 0)
        {
            $("#noti_msg_"+sender_id).html(msg);
        }
        else
        {
            var html = '';
            var new_sender_id = "noti_"+sender_id;
            html +='<div class="media" id='+new_sender_id+'>\
                        <div class="media-body">\
                            <h5 class="media-heading" id="noti_msg_'+sender_id+'">'+msg+'</h5>\
                            <div class="n-count"><span id="noti_count_'+sender_id+'">1</span></div>\
                        </div>\
                    </div>';
            $(".notification-block-inner").append(html);
        }
        // call Ajax which will track the notification:
        $.ajax({
            url: '<?php echo BASE_URL?>add_notification',           
            type: 'POST',          
            data: {chat_id : chat_id, visitor_id : sender_id},          
            success: function(response){
                    console.log(response);
            }
        });
    }    

    // play the sound if client select the sound notificarion on
    function play_sound()
    {
        var sound_status =	$(".volume").attr("id");
        if (sound_status == 'on')
        {
            $('#chatAudio')[0].play();
        }
    }

    // click on the individual Notification from the notification block, will redirect them into individual chat conversation 
    $(document).on('click',".media", function()
    {
        // $("#"+id).removeElement();
        $(".notification-block").hide();
        var active_id = $(this).attr("id");
        active_id = active_id.substr(5);
        var id =  $(".active_chat").attr("id");
        $("#"+id).removeClass("active_chat");
        $("div#"+active_id+".chat_list").addClass("active_chat");
        if (active_id != undefined)
        {
            socket.emit('read_chat',{"sender_id":'<?php echo strtolower($this->session->userdata("user_id"))?>', "receiver_id": active_id});
        }
        // clear the individual notification when we open the chat for specific visitor id
        clear_individual_notification(active_id);
    });

    // To remove the individual notification while chat is open automatically from the chat-list or while loading the pgae. If there is notification
    // for such visitor is there into the notificaytion block, we will remove it from the block as well as reset from the database
    function clear_individual_notification(active_id)
    {
        $.ajax({
            url: '<?php echo BASE_URL?>reset_notification',           
            type: 'POST',
            data: {visitor_id : active_id},      
            success: function(response){
                    console.log(response);
            }
        });
        if ($("#noti_count_"+active_id).html() != undefined)
        {
            $("#main_noti_count").html(parseInt($("#main_noti_count").html()) - parseInt($("#noti_count_"+active_id).html()));
            $("#noti_"+active_id).remove();
            if (parseInt($("#main_noti_count").html()) <= 0)
            {
                $(".notification-block-inner").html(''); // blank the whole block
                $(".notification-block").fadeOut(); // close the notification block
                $("#main_noti_count").css("display","none");  // hide the main notification count
            }
            $("#chat_notification_"+active_id).html(0); 
            $("#chat_notification_"+active_id).css("display","none"); 
        }
    }

    // To clear the all-notification from the notification block, this function is called. 
    // Which will set the main notification count to 0 and clear the notification block html
    $(document).on('click', "#clear-all", function()
    {
        $(".notification-block-inner").html(''); // blank the whole block
		$(".notification-block").fadeOut(); // close the notification block
        $("#main_noti_count").css("display","none"); // hide the main notification count
        $.ajax({
            url: '<?php echo BASE_URL?>clear_notification',           
            type: 'POST',          
            success: function(response){
                console.log(response);
            }
        });
    });

});

</script>

