<div id="sidebar" class="nav-collapse">
    <div class="leftside-navigation">
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="<?php echo BASE_URL?>" class="active">
                    <span>لوحة التحكم</span>
                </a>
            </li>
            <li>
                <a href="<?php echo BASE_URL?>">
                    <span>نتائج</span>
                </a>
            </li>
            <li>
                <a class="<?php echo $this->uri->segment(1) == 'chats' ? 'active-menu' : ''?>" href="<?php echo BASE_URL?>chats">
                    <span>دردشة</span>
                </a>
            </li>
            <li>
                <div class="notification-block" style="display:none">
                    <div class="notification-inner-block">
                        <div class="notification-block-header">
                            <a class="" id="clear-all">امسح الكل</a>
                            <h3>إخطارات</h3>
                            <div class="volume" id="<?= $this->data['user_details']->sound_status; ?>">
                            <?php  if ($this->data['user_details']->sound_status == 'on') : ?>
                                <a id="volumn_up"><i class="fa fa-volume-up" aria-hidden="true"></i></a>
                                <a id="volumn_off" style="display:none" ><i class="fa fa-volume-off " aria-hidden="true"></i></a>
                            <?php else :?>
                                <a id="volumn_up" style="display:none" ><i class="fa fa-volume-up" aria-hidden="true"></i></a>
                                <a id="volumn_off"><i class="fa fa-volume-off " aria-hidden="true"></i></a>
                            <?php endif;?>
                            </div>
                        </div>  
                        <div class="notification-block-inner">
                            <!-- Dynamic content for the notification will be display over here!! -->
                        </div>                                 
                    </div>
                </div>
                <a id="notification_link">
                    <span class="menu-a">اشعارات<span class="notification" id="main_noti_count" style="display:none"><?= $this->data['count_notification'];?></span></span>
                </a>
            </li>        
            <li>
                <a class="<?php echo $this->uri->segment(1) == 'client_settings' ? 'active-menu' : ''?>" href="<?php echo BASE_URL?>client_settings">
                    <span>حسابي</span>
                </a>                            
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle <?php echo ($this->uri->segment(1) == 'general_settings' || $this->uri->segment(1) == 'profile' || $this->uri->segment(1) == 'install' ) ? 'active-menu' : ''?>" data-toggle="dropdown" href="#">
                    <span>اعدادات</span>
                </a>
                <ul class="dropdown-menu" role="menu">                 
                    <li class="online-user">
                        <div class="active-user">
                            <div class="active-people online">
                                <div class="active-user-img"> 
                                    <img src="<?php echo (isset($client_secondary_info) && !empty($client_secondary_info[0]['chatbot_profile'])) ? PROFILE_IMG.$client_secondary_info[0]['chatbot_profile'] : IMG.'chat-user-avtar.png'?>" alt="user" class="mCS_img_loaded" height="40px" width="40px"> 
                                </div>
                                <div class="active-user-ib">
                                    <h5>مستخدم وهمية </h5>
                                    <p>نشيط</p>
                                </div>         
                            </div>
                        </div>
                    </li>
                    <li class="setting">
                        <a href="<?php echo BASE_URL?>general_settings"> 
                            <i class="fa fa-cog" aria-hidden="true"></i> الإعدادات  
                        </a>
                    </li>
                    <li>
                        <a href="<?= BASE_URL?>profile">  
                            <i class="fa fa-user" aria-hidden="true"></i> الحساب الخاص بك 
                    </li> 
                    <li class="billing">
                        <a href="<?php echo BASE_URL?>register">
                            <i class="fa fa-credit-card-alt" aria-hidden="true"></i> الفواتير  
                        </a>
                    </li>                                                   
                    <li class="t-n-p">
                        <a href="#">سياسات الشروط</a>
                    </li>
                    <li class="logout">
                        <a href="<?php echo BASE_URL?>logout"> 
                            <i class="fa fa-power-off" aria-hidden="true"></i> الخروج  
                        </a>
                    </li>         
                </ul> 
            </li>
        </ul>
    </div>          
</div> 