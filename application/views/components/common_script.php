<script src="https://cdn.socket.io/socket.io-1.2.0.js"></script>
<script src="https://cdn.jsdelivr.net/npm/socket.io-file-client@2.0.2/socket.io-file-client.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

    var screeHt = $('body').height();
    var headerHt = $('header').height();
    var finalHt = screeHt - headerHt - 61;
    $('.mesgs').height(finalHt);

    //start - Initial declaration
    var socket = io.connect("http://18.217.216.88:3000",{transports: ['websocket'] });
    var uploader = new SocketIOFileClient(socket);
    socket.emit('privatechatroom', {"email":'<?php echo strtolower($this->session->userdata('email'))?>'});

    // set the send message as html
    socket.on('chat message', function (data) 
    {
        var socket_id = socket.id;
        id = Math.floor(Math.random() * (1000000 - 1000 + 1)) + 1000;
        var div = $('#messages').children().last();
        if (div.length > 0)
        {
            var main_class =  div[0] ;
            class_str = $(main_class).attr("class");
            class_name = class_str.substr(0,class_str.indexOf(' '));
        }
        else
        {
            class_name = '';
        }
        if (data.sender_id != $(".active_chat").attr("id") && data.comes_from == 'visitor')
        {
            $("#"+data.sender_id).children().addClass("online");
            $("#"+data.sender_id).attr("data-chat-id",data.chat_id);
            $("#p_"+data.sender_id).html(data.message);
            $("#p_"+data.sender_id).css("font-weight","bold");
            // notification code
            append_notification(data.sender_id, data.message, data.chat_id, data.file_name);
        }
    });

    // apppend the notification if client is not talking with the client who sends us the msg
    function append_notification(sender_id, msg = '', chat_id, file_name)
    {
        // play sound when notification will be arraive
		play_sound();
        // Set the message
        var msg = msg == '' ? file_name : msg;
        // set notification count
        $("#noti_count_"+sender_id).html(parseInt($("#noti_count_"+sender_id).html()) + 1);
        // update the main notification count
        $("#main_noti_count").css("display","inline-block"); // hide the main notification count

        $("#main_noti_count").html(parseInt($("#main_noti_count").html()) + 1);
        // Check if notification is already exist for the partivcualr visitor, we replace it with the newer message
        if ($("#noti_"+sender_id).length > 0)
        {
            $("#noti_msg_"+sender_id).html(msg);
        }
        else
        {
            var html = '';
            var new_sender_id = "noti_"+sender_id;
            html +='<div class="media" id='+new_sender_id+'>\
                        <div class="media-body">\
                            <h5 class="media-heading" id="noti_msg_'+sender_id+'">'+msg+'</h5>\
                            <div class="n-count"><span id="noti_count_'+sender_id+'">1</span></div>\
                        </div>\
                    </div>';
            $(".notification-block-inner").append(html);
        }
        // call Ajax which will track the notification:
        $.ajax({
            url: '<?php echo BASE_URL?>add_notification',           
            type: 'POST',          
            data: {chat_id : chat_id, visitor_id : sender_id},          
            success: function(response){
                    console.log(response);
            }
        });
    }    

    // IF notification-sound is on, then we sound the notification-tone. OR else nothing can be done
    function play_sound()
    {
        var sound_status =	$(".volume").attr("id");
        if (sound_status == 'on')
        {
            $('#chatAudio')[0].play();
        }
    }

    // click on the individual notification from the notification-block, we will redirect them on indivial chat-conversartion and reset the notification count
    $(document).on('click',".media", function()
    {
        var active_id = $(this).attr("id");
        var active_id = active_id.substr(5);
        var segmet_url = '<?php echo $this->uri->segment("1");?>';
        if (segmet_url != 'chats')
        {
            window.location.href = '<?php echo BASE_URL?>chats?id='+active_id ;
        }
        $(".notification-block").hide();
        var id =  $(".active_chat").attr("id");
        $("#"+id).removeClass("active_chat");
        $("div#"+active_id+".chat_list").addClass("active_chat");
        if (active_id != undefined)
        {
            socket.emit('read_chat',{"sender_id":'<?php echo $this->session->userdata("user_id")?>', "receiver_id": active_id});
        }
        $.ajax({
            url: '<?php echo BASE_URL?>reset_notification',           
            type: 'POST',
            data: {visitor_id : active_id},      
            success: function(response){
                    console.log(response);
            }
        });
        $("#main_noti_count").html(parseInt($("#main_noti_count").html()) - parseInt($("#noti_count_"+active_id).html()));
        if (parseInt($("#main_noti_count").html() <= 0))
        {
            $(".notification-block-inner").html(''); // blank the every block
            $(".notification-block").fadeOut();
            $("#main_noti_count").css("display","none");
        }
    });

    //  click on the clear all notifications button., we call an ajax function which will reset all the notitification of this client to 0 and also update the total count as 0
    $(document).on('click', "#clear-all", function()
    {
        $(".notification-block-inner").html(''); // blank the every block
		$(".notification-block").fadeOut();
        $("#main_noti_count").css("display","none");
        $.ajax({
            url: '<?php echo BASE_URL?>clear_notification',           
            type: 'POST',          
            success: function(response){
                console.log(response);
            }
        });
    });

});

</script>