<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title?></title>
	<link href="<?php echo CSS?>bootstrap.min.css" rel="stylesheet"><!--bootstrap css-->
	<link href="<?php echo CSS?>font-awesome.min.css" rel="stylesheet"><!--font-awesome css-->
	<link rel="stylesheet" type="text/css" href="<?php echo CSS?>style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS?>responsive.css"/>
</head>