<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $title;?></title>

	<link href="<?php echo CSS?>bootstrap.min.css" rel="stylesheet"><!--bootstrap css-->
	<link href="<?php echo CSS?>font-awesome.min.css" rel="stylesheet"><!--font-awesome css-->
	
	<link rel="stylesheet" type="text/css" href="<?php echo CSS?>client_style.css?<?php echo date('l jS \of F Y h:i:s A');?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS?>client_responsive.css?<?php echo date('l jS \of F Y h:i:s A');?>"/>
	<link rel="stylesheet" href="<?php echo CSS?>jquery.mCustomScrollbar.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo CSS?>emoji.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	
</head>