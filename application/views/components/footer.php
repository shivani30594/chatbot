<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer-content">
                    <h2>الشركة</h2>
                    <ul>
                        <li><a href="<?php echo BASE_URL?>about">عن سوالف</a></li>
                        <li><a href="javascript:void(0)">عملائنا</a></li>
                        <li><a href="javascript:void(0)">وظائف</a></li>
                        <li><a href="<?php echo BASE_URL?>terms">شروط</a></li>
                        <li><a href="<?php echo BASE_URL?>privacy">سرية</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer-content">
                    <h2>مميزات</h2>
                    <ul>
                        <li><a href="javascript:void(0)">إشعارات مباشرة</a></li>
                        <li><a href="javascript:void(0)">برمجية محادة</a></li>
                        <li><a href="javascript:void(0)">برامج أبل و أندرويد</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer-content">
                    <h2>منتجات</h2>
                    <ul>
                        <li><a href="javascript:void(0)">سوالفنا</a></li>
                    </ul>
                    <ul class="footer-bootm-link">
                        <li><a href="<?php echo BASE_URL?>pricing">أسعارنا</a></li>
                        <li><a href="javascript:void(0)">سوالف للشركات البتدأة</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
