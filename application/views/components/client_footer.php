<script src="<?php echo SCRIPTS?>bootstrap.min.js"></script><!--bootstrap js-->
<script src="<?php echo SCRIPTS?>jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo SCRIPTS?>timezones.full.min.js"></script><!--bootstrap js-->
<script src="https://cdn.socket.io/socket.io-1.2.0.js"></script>
<script src="https://cdn.jsdelivr.net/npm/socket.io-file-client@2.0.2/socket.io-file-client.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="<?php echo SCRIPTS?>jquery.validate.js"></script><!--jquery js-->

<script>
	$(document).ready(function() {

		var socket = io.connect("http://18.217.216.88:3000",{transports: ['websocket'] });
		var uploader = new SocketIOFileClient(socket);
		socket.emit('privatechatroom', {"email":'<?php echo strtolower($this->session->userdata('email'));?>'}); 

		$(".sidebar-toggle-box").click(function(){
			$("#main-content").toggleClass("main-content-full");
			$(this).closest(".header").next(".side-bar-mobile").toggleClass("show-menu")
		});
	
		var $window = $(window);

		function checkWidth() {
			var windowsize = $window.width();
			if (windowsize < 768) {
				//if the window is greater than 440px wide then turn on jScrollPane..
				$("#sidebar").addClass("side-bar-mobile");
				$("#main-content").addClass("main-content-mobile");
				var screeHt = $('body').height();
    			var headerHt = $('header').height();
    			var finalHt = screeHt - headerHt - 61;
    			$('.inbox-people').height(finalHt);
			}
		}
		// by deafult opening the menu in mobile view
		$(".inbox-people").addClass("inbox_people-mobile");

		$(".user-btn-mobile").click(function(){ 
			
			$(".inbox-people").toggleClass("inbox_people-mobile");
		});
		
		// Execute on load
		checkWidth();
	
		// Bind event listener
		$(window).resize( function()
		{
			checkWidth();  
		});

		// set the notification tone to the body tag. So when required we can play the music from it.
		$('<audio id="chatAudio"><source src="<?= MP3?>notify.ogg" type="audio/ogg"><source src="<?= MP3?>notify.mp3" type="audio/mpeg"><source src="<?= MP3?>notify.wav" type="audio/wav"></audio>').appendTo('body');

		$(document).on("click", "#notification_link", function()
		{
			if (parseInt($("#main_noti_count").html()) > 0)
			{
				$(".notification-block").show();
			}
		})
		
		$(document).mouseup(function (e){
			var container = $(".notification-block");
			if (!container.is(e.target) && container.has(e.target).length === 0){
				container.fadeOut();
			}
		}); 

		$(document).on("click", "#volumn_off", function()
		{
			$(".volume").attr("id", 'on');
			set_sound_status('on');
			$("#volumn_up").show();
			$("#volumn_off").hide();
		})

		$(document).on("click", "#volumn_up", function()
		{
			$(".volume").attr("id", 'off');
			set_sound_status('off');
			$("#volumn_up").hide();
			$("#volumn_off").show();
		})

		function set_sound_status(status)
		{
			$.ajax({
				url: '<?php echo BASE_URL?>set_sound_status',           
				type: 'POST',          
				data: {sound_status : status},          
				success: function(response){
					// console.log(response);
				}
        	})
		}

		load_notification();
		function load_notification()
		{
			$.ajax({
				url: '<?php echo BASE_URL?>notification_details',           
				type: 'POST',          
				success: function(response){
					console.log(response)
					var segmet_url = '<?php echo $this->uri->segment("1");?>';
					if (segmet_url != 'chats')
					{
						socket.emit('display contacts',{"sender_id":'<?php echo $this->session->userdata("user_id")?>',"data":response.data});
					}
				}
			})
			
		}

		socket.on('display contacts', function(data)
		{
			var mergedList = _.map(data.data, function(item){
				return _.extend(item, _.findWhere(data.records.data, { chat_id: item.chat_id }));
			});
			for(i=0;i<mergedList.length;i++)
			{
				if ( mergedList[i].notifications_count > 0)
				{
					$("#main_noti_count").css("display","inline-block");
					var html = '';
					var new_sender_id = "noti_"+mergedList[i].unique_id;
					html +='<div class="media" id='+new_sender_id+'>\
						<div class="media-body">\
							<h5 class="media-heading" id="noti_msg_'+mergedList[i].unique_id+'">'+mergedList[i].message+'</h5>\
							<div class="n-count"><span class="total_notification_count" id="noti_count_'+mergedList[i].unique_id+'">'+mergedList[i].notifications_count+'</span></div>\
						</div>\
					</div>';
					$(".notification-block-inner").append(html);
				}
			}
		});

	});
</script>

