<?php
class MY_Controller extends CI_Controller 
{
	public $data = array();
		
		function __construct() 
		{
			parent::__construct();
			$this->data['errors'] = array();
			$this->load->model('user_m');
			$this->load->model('visitor_m');
			$this->load->model('secondary_m');
			if ($this->session->userdata('user_id')) {
				$this->data['user_details'] = $this->user_m->get($this->session->userdata('user_id'));
				$relation = array(
					"fields" => "*",
					'conditions' => "client_id =".$this->session->userdata('user_id')
				);
				$visitor_info = $this->visitor_m->get_relation("",$relation);
				$this->data['count_notification'] = array_sum(array_column($visitor_info, "notifications_count"));
				$relation = array(
					"fields" => "*",
					'conditions' => "client_id =".$this->session->userdata('user_id')
				);
				$this->data['client_secondary_info'] = $this->secondary_m->get_relation("",$relation);
				
			}
		}
}