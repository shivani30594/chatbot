<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('send_mail'))
{    
    function send_mail($to, $subject, $body) {
		$CI = & get_instance();
		$CI->load->library('My_PHPMailer');
		$mail = new PHPMailer();
		$mail->debug = 2;
		$mail->IsSMTP(); // we are going to use SMTP
		$mail->SMTPAuth = true; // enabled SMTP authentication
		$mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
		$mail->Host = "smtp.gmail.com";      // setting GMail as our SMTP server
		$mail->Port = 465;     // SMTP port to connect to GMail
		$mail->Username = EMAILID;  // user email address
		$mail->Password = PASSWORD;	    // password in GMail
		$mail->Transport = 'Smtp';
		$mail->SetFrom(EMAILID, 'Web Live Chat');  //Who is sending the email
		$mail->AddReplyTo(EMAILID, 'Web Live Chat');  //email address that receives the response
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $body;
		$mail->AddAddress($to);
		if (!$mail->send()) {
			return 0;
 		} else {
		    return 1;
		}
    }
}




