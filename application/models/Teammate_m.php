<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teammate_m extends My_Model {

    protected $_table_name     = 'webchat_teammates';
    protected $_primary_key    = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by       = 'id';
    protected $_timestamps     = TRUE;
 
}
