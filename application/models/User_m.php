<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends My_Model {

    protected $_table_name     = 'webchat_users';
    protected $_primary_key    = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by       = 'id';
    protected $_timestamps     = TRUE;
 
    public function u_loggedin() {
        return (bool) $this->session->userdata('u_loggedin');
    }

    public function login($email, $password)
    {
        $this->db->where('password', $password);
    	$this->db->where('email_id', $email);
        $user = $this->db->get('webchat_users')->row();
        if (count($user) )
        {
            if ($user->is_active == 'Yes') 
            {
                $data = array(
                    'user_id' => $user->id,
                    'u_loggedin' => TRUE,
                    'email' => $email,
                    'role' => $user->role
                );
    
                $this->session->set_userdata($data);
                return TRUE;
            }
            else
            {
                $this->session->set_flashdata("error","Your account is deactivated. Please contact to admin.");
            }
        }
        else{
            $this->session->set_flashdata("error","Password and username combination doesn't match.");
        }
        return false;
    }

    public function logout()
    {
        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('webchat_users', array("login_status" => 'offline'));
    }
}

