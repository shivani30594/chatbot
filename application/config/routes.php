<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['login'] = 'security/login';
$route['check_login'] = 'security/check_login';
$route['google_login'] = 'security/google_login';

$route['register'] = 'client/payment/index';
$route['logout'] = 'client/settings/logout';
$route['register_2'] = 'client/payment/register_2';
$route['register_3'] = 'client/payment/register_3';
$route['save_details'] = 'client/payment/save_details';
$route['create/(:num)'] = 'client/payment/create/$1';
$route['final_save'] = 'client/payment/final_save';
$route['client_settings'] = 'client/settings/index';
$route['chats'] = 'client/Chat/index';
$route['style_messenger'] = 'client/settings/style_messenger';
$route['general_settings'] = 'client/general_setting/index';    
$route['install_script'] = 'client/install/index';    
$route['set_sound_status'] = 'client/notifications/set_sound_status';    
$route['add_notification'] = 'client/notifications/add_notification';
$route['clear_notification'] = 'client/notifications/clear_notification';
$route['reset_notification'] = 'client/notifications/reset_notification';
$route['notification_details'] = 'client/notifications/notification_details'; 
$route['profile'] = 'client/profile/index'; 
$route['set_chatbot_email'] = 'client/profile/set_chatbot_email'; 
$route['set_chatbot_image'] = 'client/profile/set_chatbot_image'; 
$route['install'] = 'client/install/index'; 
$route['change_client_status'] = 'client/settings/change_client_status';
$route['change_contact_option'] = 'client/settings/change_contact_option';
$route['set_question_text'] = 'client/settings/set_question_text';
$route['teammates'] = 'client/teammates/index';
$route['invite_member'] = 'client/teammates/invite_member';
$route['verification/(:any)/(:num)'] = 'client/teammates/verification/$1/$2';
$route['check_teammate_email'] = 'client/teammates/check_email';
$route['delete_teammate'] = 'client/teammates/delete_teammate';

$route['t_chat'] = 'teammate/chat/index';
$route['t_profile'] = 'teammate/profile/index';
$route['t_set_teammate_info'] = 'teammate/profile/set_teammate_info';
$route['t_set_chatbot_image'] = 'teammate/profile/set_chatbot_image';

$route['terms'] = 'front/home/terms';
$route['privacy'] = 'front/home/privacy';
$route['pricing'] = 'front/home/pricing';
$route['about'] = 'front/home/about';
$route['chatbot'] = 'front/home/chatbot';


$route['fetch_record'] = 'visitor/visitor/fetch_record';
$route['update_last_seen'] = 'visitor/visitor/update_last_seen';
$route['check_status'] = 'visitor/visitor/check_status';
$route['get_questions'] = 'visitor/visitor/get_questions';
$route['add_visitor'] = 'visitor/visitor/add_visitor';
$route['set_visitor_info'] = 'visitor/visitor/set_visitor_info';

// $route['a_chgstatus/(:num)'] = 'admin/user/change_status/$1';

$route['default_controller'] = 'security';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
